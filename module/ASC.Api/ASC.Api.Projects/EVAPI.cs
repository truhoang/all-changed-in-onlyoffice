﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ASC.Api.Interfaces;
using ASC.Api.Attributes;

namespace ASC.Api.Projects
{
    class EVAPI : IApiEntryPoint
    {
        public string Name
        {
            get { return "EVAPI"; }
        }


        [Read("")]
        public string GetAllEV()
        {
            return "ev";
        }
    }
}
