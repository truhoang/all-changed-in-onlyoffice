delete from res_files where projectname = 'Addons' and modulename in ('Organizer','Todo','QuickLinks');
delete from res_files where projectname = 'Files' and modulename = 'Editor';
delete from res_files where projectname = 'Community' and modulename = 'Photo';
delete from res_files where projectname in ('Partners', 'Statistics', 'TeamLab Editors', 'Miscellaneous', 'Scanner','Tools','WebApps','Affiliate');
delete from res_data where fileid not in (select id from res_files);
delete from res_files where not exists(select fileid from res_data where fileid = res_files.id);