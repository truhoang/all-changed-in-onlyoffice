-- procedure for executing dynamic query
drop procedure if exists executeQuery;
DELIMITER //
CREATE PROCEDURE executeQuery
(IN MyQuery VARCHAR(21845))
BEGIN
	DECLARE LocalQuery VARCHAR(21845);
-- ------------------------------------------------------------------------
    set @LocalQuery = concat(MyQuery,'');
    PREPARE stmt FROM @LocalQuery;
	EXECUTE stmt;
    DEALLOCATE PREPARE stmt;
END //
DELIMITER ;


-- this function checks if a specific column exists in a specific table
drop procedure if exists ifColumnExists;
DELIMITER //
CREATE PROCEDURE ifColumnExists
(IN tableName CHAR(40),IN columnName CHAR(40), Out result bool)
BEGIN
if not exists(Select *
	from information_schema.columns
    where TABLE_SCHEMA=(SELECT DATABASE())
		and table_name=tableName
		and column_name=columnName) then
        set result=true;
        else set result=false;
end IF;
END //
DELIMITER ;

-- this procedure is used for adding a column to a table if it does not exist
drop procedure if exists addNewColumn;
DELIMITER //
CREATE PROCEDURE addNewColumn
(IN tableName CHAR(40), IN columnName CHAR(40), IN colDef CHAR(150))
BEGIN
	declare result bool;
-- ---------------------------------------------------------------------------
	call ifColumnExists(tableName, columnName,result);
	if result=true then
		call executeQuery(concat('alter table ', tableName,  ' add column ', columnName, ' ', colDef));
	end if;
END //
DELIMITER ;


drop procedure if exists addnewColumns;
DELIMITER //
CREATE PROCEDURE addnewColumns()
BEGIN
	declare tableName varchar(100);
-- --------------------------------------------------------------------------
	-- add new columns to existing table core_user     
	set @tableName='core_user';
    
	call addNewColumn(@tableName, 'windowsAcount', 'char(100)');
	
    -- add hour salary
    call addNewColumn(@tableName, 'hour_salary', 'int default 0');
	
    -- add hour salary column
    call addNewColumn(@tableName,'salary', 'int default 0');
	   
    -- add hour monthly_signed_time column
    call addNewColumn(@tableName,'monthly_signed_time', 'int default 0');
	
-- -----------------------------------------------------   
	-- add new columns to existing table projects_projects    
    set @tableName='projects_projects';
    
    call addNewColumn(@tableName,'cost', 'int default 0');
	
    call addNewColumn(@tableName,'price', 'int default 0');
	
    call addNewColumn(@tableName,'time_expected', 'int default 0');
    
    call addNewColumn(@tableName,'deadline', 'datetime');
    
-- ----------------------------------------------------- 
	-- add new columns to existing table projects_tasks
    set @tableName='projects_tasks';
    
    call addNewColumn(@tableName,'task_cost', 'int default 0');
    
    call addNewColumn(@tableName,'task_price', 'int default 0');
	
    call addNewColumn(@tableName,'task_time_expected', 'int default 0'); -- 'total time expected to successful accomplish the task';
	
    call addNewColumn(@tableName,'deadline', 'datetime');
	
-- ----------------------------------------------------- 
	-- add new columns to existing table projects_time_tracking    
    
    set @tableName='projects_time_tracking';
    
    call addNewColumn(@tableName,'modified_by', 'varchar(38)');
	
    call addNewColumn(@tableName,'datetime_last_modified', 'datetime');
	
    call addNewColumn(@tableName,'time_started', 'datetime');
	
END //
DELIMITER ;

call addnewColumns();

-- -------------------------------------------------------------------------------------------------------------

create table if not exists projects_signed_times
(
	id int not null AUTO_INCREMENT,
    title varchar(38),
    user_id varchar(38) not null,
	task_id int(11),
	project_id int(11),
    total_minutes int not null default 0,
    created_by varchar(38) not null,
	tenant_id int(11) not null,
    datetime_created datetime not null,
    description text,
    -- describes if signed time is on task or on project
    type_signed_time varchar(20) not null,
    last_modified_by varchar(38),
    last_modified_on datetime,
    primary key (`id`),
    key `user_id` (`user_id`),
    key `task_id` (`task_id`),
    key `project_id` (`project_id`),
    key `created_by` (`created_by`),
    key `tenant_id` (`tenant_id`)
);

-- -------------------------------------------------------------------------------------------------------------

-- procedures for creating history tables
drop procedure if exists CreateHistoryTable;
DELIMITER //
CREATE PROCEDURE CreateHistoryTable
(IN tableName CHAR(40))
BEGIN
    declare tableHistoryName char(100);
-- -----------------------------------------------------------------------------------------
	set @tableHistoryName = concat(tableName, '_history');
    call executeQuery(concat('drop table if exists  ', @tableHistoryName));-- drop existing history table
    call executeQuery(concat('create table if not exists ', @tableHistoryName, '(like ', tableName, ')'));-- create new history table
    call executeQuery(concat('alter table ', @tableHistoryName, ' add `', tableName, '_id` int(11) NOT NULL, add `action_type` enum("delete","update");'));-- Add new two columns 'object_Name_id' and 'action_type' 
END //
DELIMITER ;



-- region projects_time_tracking_history
call CreateHistoryTable('projects_time_tracking');

drop trigger if exists update_projects_time_tracking;
DELIMITER //
Create trigger update_projects_time_tracking after update on projects_time_tracking
for each row
begin
	insert into projects_time_tracking_history values (default, Old.note, OLD.date, OLD.hours, OLD.tenant_id, OLD.relative_task_id, OLD.person_id, OLD.project_id, OLD.create_on, Old.create_by, OLD.payment_status, OLD.status_changed, OLD.modified_by, OLD.datetime_last_modified, OLD.time_started, OLD.id, 'update');
end//
DELIMITER ;

drop trigger if exists delete_projects_time_tracking;
DELIMITER //
Create trigger delete_projects_time_tracking after delete on projects_time_tracking
for each row
begin
	insert into projects_time_tracking_history values (default, Old.note, OLD.date, OLD.hours, OLD.tenant_id, OLD.relative_task_id, OLD.person_id, OLD.project_id, OLD.create_on, Old.create_by, OLD.payment_status, OLD.status_changed, OLD.modified_by, OLD.datetime_last_modified, OLD.time_started, OLD.id, 'delete');
end//
DELIMITER ;
-- end region projects_time_tracking_history

-- -------------------------------------------------------------------------------------------------------------

-- region projects_projects_history
call CreateHistoryTable('projects_projects');

drop trigger if exists update_projects_projects;
DELIMITER //
Create trigger update_projects_projects after update on projects_projects
for each row
begin
	insert into projects_projects_history values (default, Old.status, OLD.status_changed, OLD.title, OLD.description, OLD.responsible_id, OLD.tenant_id, OLD.private, OLD.create_on, OLD.create_by, OLD.last_modified_on, OLD.last_modified_by, OLD.cost, OLD.price, OLD.time_expected, OLD.deadline, OLD.id, 'update');
end//
DELIMITER ;

drop trigger if exists delete_projects_projects;
DELIMITER //
Create trigger delete_projects_projects after delete on projects_projects
for each row
begin
	insert into projects_projects_history values (default, Old.status, OLD.status_changed, OLD.title, OLD.description, OLD.responsible_id, OLD.tenant_id, OLD.private, OLD.create_on, OLD.create_by, OLD.last_modified_on, OLD.last_modified_by, OLD.cost, OLD.price, OLD.time_expected, OLD.deadline, OLD.id, 'delete');
end//
DELIMITER ;
-- end region projects_projects_history

-- -------------------------------------------------------------------------------------------------------------

-- region projects_tasks_history
call CreateHistoryTable('projects_tasks');

drop trigger if exists update_projects_tasks;
DELIMITER //
Create trigger update_projects_tasks after update on projects_tasks
for each row
begin
	insert into projects_tasks_history values (default, Old.title, OLD.description, OLD.responsible_id, OLD.priority, OLD.status, OLD.status_changed, OLD.project_id,OLD.milestone_id, Old.tenant_id, OLD.sort_order, OLD.deadline, OLD.create_by, OLD.create_on, OLD.last_modified_by, OLD.last_modified_on, OLD.start_date, OLD.progress, OLD.task_cost, OLD.task_price, OLD.task_time_expected, OLD.id, 'update');
end//
DELIMITER ;

drop trigger if exists delete_projects_tasks;
DELIMITER //
Create trigger delete_projects_tasks after delete on projects_tasks
for each row
begin
	insert into projects_tasks_history values (default, Old.title, OLD.description, OLD.responsible_id, OLD.priority, OLD.status, OLD.status_changed, OLD.project_id,OLD.milestone_id, Old.tenant_id, OLD.sort_order, OLD.deadline, OLD.create_by, OLD.create_on, OLD.last_modified_by, OLD.last_modified_on, OLD.start_date, OLD.progress, OLD.task_cost, OLD.task_price, OLD.task_time_expected, OLD.id, 'delete');
end//
DELIMITER ;
-- end region projects_tasks_history

-- -------------------------------------------------------------------------------------------------------------

-- region projects_signed_times_history
call CreateHistoryTable('projects_signed_times');

drop trigger if exists update_projects_signed_times;
DELIMITER //
Create trigger update_projects_signed_times after update on projects_signed_times
for each row
begin
	insert into projects_signed_times_history values (default, Old.title, OLD.user_id, OLD.task_id, OLD.project_id, OLD.total_minutes, OLD.created_by, OLD.tenant_id, OLD.datetime_created, Old.description, OLD.type_signed_time, OLD.last_modified_by, OLD.last_modified_on, OLD.id, 'update');
end//
DELIMITER ;

drop trigger if exists delete_projects_signed_times;
DELIMITER //
Create trigger delete_projects_signed_times after delete on projects_signed_times
for each row
begin
	insert into projects_signed_times_history values (default, Old.title, OLD.user_id, OLD.task_id, OLD.project_id, OLD.total_minutes, OLD.created_by, OLD.tenant_id, OLD.datetime_created, Old.description, OLD.type_signed_time, OLD.last_modified_by, OLD.last_modified_on, OLD.id, 'delete');
end//
DELIMITER ;
-- end region projects_signed_times_history
