ALTER TABLE `account_links` CHANGE COLUMN `provider` `provider` CHAR(60) NULL DEFAULT NULL;

ALTER TABLE `audit_events` CHANGE COLUMN `page` `page` VARCHAR(300) NULL DEFAULT NULL;
ALTER TABLE `audit_events` DROP INDEX `tenant_id`;
ALTER TABLE `audit_events` DROP INDEX `date`;
ALTER TABLE `audit_events` ADD INDEX `date` (`tenant_id`, `date`);
    
CREATE TABLE IF NOT EXISTS `res_cultures` (
  `title` varchar(120) NOT NULL,
  `value` varchar(120) NOT NULL,
  `available` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`title`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `res_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fileid` int(11) NOT NULL,
  `title` varchar(120) NOT NULL,
  `cultureTitle` varchar(20) NOT NULL,
  `textValue` text,
  `description` text,
  `timeChanges` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `resourceType` varchar(20) DEFAULT NULL,
  `flag` int(11) NOT NULL DEFAULT '0',
  `link` varchar(120) DEFAULT NULL,
  `authorLogin` varchar(50) NOT NULL DEFAULT 'Console',
  PRIMARY KEY (`fileid`,`title`,`cultureTitle`),
  UNIQUE KEY `id` (`id`),
  KEY `dateIndex` (`timeChanges`),
  KEY `resources_FK2` (`cultureTitle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `res_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `projectName` varchar(50) NOT NULL,
  `moduleName` varchar(50) NOT NULL,
  `resName` varchar(50) NOT NULL,
  `isLock` tinyint(1) NOT NULL DEFAULT '0',
  `lastUpdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index1` (`resName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `backup_backup` (
  `id` char(38) NOT NULL,
  `tenant_id` int(11) NOT NULL,
  `is_scheduled` int(1) NOT NULL,
  `name` varchar(255) NOT NULL,
  `storage_type` int(11) NOT NULL,
  `storage_base_path` varchar(255) DEFAULT NULL,
  `storage_path` varchar(255) NOT NULL,
  `created_on` datetime NOT NULL,
  `expires_on` datetime NOT NULL DEFAULT '0001-01-01 00:00:00',
  PRIMARY KEY (`id`),
  KEY `tenant_id` (`tenant_id`),
  KEY `expires_on` (`expires_on`),
  KEY `is_scheduled` (`is_scheduled`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `backup_schedule` (
  `tenant_id` int(11) NOT NULL,
  `backup_mail` int(11) NOT NULL DEFAULT '0',
  `cron` varchar(255) NOT NULL,
  `backups_stored` int(11) NOT NULL,
  `storage_type` int(11) NOT NULL,
  `storage_base_path` varchar(255) DEFAULT NULL,
  `last_backup_time` datetime NOT NULL,
  PRIMARY KEY (`tenant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `crm_organisation_logo` CHANGE COLUMN `content` `content` MEDIUMTEXT NOT NULL;

ALTER TABLE `crm_tag` CHANGE COLUMN `title` `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL;

CREATE TABLE IF NOT EXISTS `crm_voip_calls` (
  `id` varchar(50) NOT NULL,
  `number_from` varchar(50) NOT NULL,
  `number_to` varchar(50) NOT NULL,
  `status` int(10) DEFAULT NULL,
  `answered_by` varchar(50) NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000',
  `dial_date` datetime DEFAULT NULL,
  `dial_duration` int(11) DEFAULT NULL,
  `record_url` text,
  `record_duration` int(11) DEFAULT NULL,
  `contact_id` int(10) DEFAULT NULL,
  `price` decimal(10,4) DEFAULT NULL,
  `tenant_id` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tenant_id` (`tenant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `crm_voip_calls_history` (
  `id` varchar(50) NOT NULL,
  `parent_call_id` varchar(50) NOT NULL,
  `answered_by` varchar(50) NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000',
  `queue_date` datetime DEFAULT NULL,
  `answer_date` datetime DEFAULT NULL,
  `end_dial_date` datetime DEFAULT NULL,
  `record_url` text,
  `record_duration` int(11) DEFAULT NULL,
  `price` decimal(10,4) DEFAULT NULL,
  `tenant_id` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tenant_id` (`tenant_id`,`parent_call_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `crm_voip_number` (
  `id` varchar(50) NOT NULL,
  `number` varchar(50) NOT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `settings` text,
  `tenant_id` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tenant_id` (`tenant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `files_security` ALTER `entry_id` DROP DEFAULT;
ALTER TABLE `files_security` CHANGE COLUMN `entry_id` `entry_id` VARCHAR(50) NOT NULL;
ALTER TABLE `files_security` ADD INDEX `tenant_id` (`tenant_id`, `entry_type`, `entry_id`, `owner`);

CREATE TABLE IF NOT EXISTS `files_thirdparty_app` (
  `user_id` varchar(38) NOT NULL,
  `app` varchar(50) NOT NULL,
  `token` text,
  `tenant_id` int(11) NOT NULL,
  `modified_on` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`,`app`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `jabber_archive` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jid` varchar(255) NOT NULL,
  `stamp` datetime NOT NULL,
  `message` mediumtext,
  PRIMARY KEY (`id`),
  KEY `jabber_archive_jid` (`jid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `jabber_archive_switch` (
  `id` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `jabber_offactivity` (
  `jid` varchar(255) NOT NULL,
  `logout` datetime DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`jid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `jabber_offmessage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jid` varchar(255) NOT NULL,
  `message` mediumtext,
  PRIMARY KEY (`id`),
  KEY `jabber_offmessage_jid` (`jid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `jabber_offpresence` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jid_to` varchar(255) NOT NULL,
  `jid_from` varchar(255) DEFAULT NULL,
  `type` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `jabber_offpresence_to` (`jid_to`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `jabber_private` (
  `jid` varchar(255) NOT NULL,
  `tag` varchar(255) NOT NULL,
  `namespace` varchar(255) NOT NULL,
  `element` text,
  PRIMARY KEY (`jid`,`tag`,`namespace`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `jabber_room` (
  `jid` varchar(255) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `subject` varchar(255) DEFAULT NULL,
  `instructions` varchar(255) DEFAULT NULL,
  `pwd` varchar(255) DEFAULT NULL,
  `pwdprotect` int(11) DEFAULT NULL,
  `visible` int(11) DEFAULT NULL,
  `members` text,
  `maxoccupant` int(11) DEFAULT NULL,
  `historycountonenter` int(11) DEFAULT NULL,
  `anonymous` int(11) DEFAULT NULL,
  `logging` int(11) DEFAULT NULL,
  `membersonly` int(11) DEFAULT NULL,
  `usernamesonly` int(11) DEFAULT NULL,
  `moderated` int(11) DEFAULT NULL,
  `persistent` int(11) DEFAULT NULL,
  `presencebroadcastedfrom` int(11) DEFAULT NULL,
  `canchangesubject` int(11) DEFAULT NULL,
  `caninvite` int(11) DEFAULT NULL,
  `canseememberlist` int(11) DEFAULT NULL,
  PRIMARY KEY (`jid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `jabber_room_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jid` varchar(255) NOT NULL,
  `stamp` datetime NOT NULL,
  `message` mediumtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `jabber_room_history_jid` (`jid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `jabber_roster` (
  `jid` varchar(255) NOT NULL,
  `item_jid` varchar(255) NOT NULL,
  `name` varchar(512) DEFAULT NULL,
  `subscription` int(11) NOT NULL DEFAULT '0',
  `ask` int(11) NOT NULL DEFAULT '0',
  `groups` text,
  PRIMARY KEY (`jid`,`item_jid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `jabber_vcard` (
  `jid` varchar(255) NOT NULL,
  `vcard` text NOT NULL,
  PRIMARY KEY (`jid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `login_events` ALTER `page` DROP DEFAULT;
ALTER TABLE `login_events` CHANGE COLUMN `page` `page` VARCHAR(600) NOT NULL;

ALTER TABLE `mail_alerts` ADD COLUMN `id_mailbox` INT NOT NULL DEFAULT '-1';
ALTER TABLE `mail_alerts` ADD COLUMN `type` INT NOT NULL DEFAULT '0';
ALTER TABLE `mail_alerts` CHANGE COLUMN `data` `data` MEDIUMTEXT NULL DEFAULT NULL;
ALTER TABLE `mail_alerts` DROP INDEX `USER`;
ALTER TABLE `mail_alerts` ADD INDEX `tenant_id_user_id_mailbox_type` (`tenant`, `id_user`, `id_mailbox`, `type`);

ALTER TABLE `mail_mailbox` CHANGE COLUMN `imap_folders` `imap_folders` MEDIUMTEXT NULL;

ALTER TABLE `projects_messages` ADD COLUMN `status` INT NOT NULL DEFAULT '0' AFTER `title`;

ALTER TABLE `projects_project_participant` DROP INDEX `tenant`;
ALTER TABLE `projects_project_participant` DROP PRIMARY KEY;
ALTER TABLE `projects_project_participant` ADD PRIMARY KEY (`tenant`, `project_id`, `participant_id`);

ALTER TABLE `projects_tasks` DROP INDEX `tenant_id`;
ALTER TABLE `projects_tasks` ADD INDEX `tenant_id` (`tenant_id`, `project_id`);
ALTER TABLE `projects_tasks` DROP INDEX `milestone_id`;
ALTER TABLE `projects_tasks` ADD INDEX `milestone_id` (`tenant_id`, `milestone_id`);

ALTER TABLE `sso_tokens` DROP INDEX `tokenId`;
ALTER TABLE `sso_tokens` ADD PRIMARY KEY (`tokenId`);

CREATE TABLE IF NOT EXISTS `tenants_buttons` (
  `tariff_id` int(10) NOT NULL,
  `partner_id` varchar(50) NOT NULL,
  `button_url` text NOT NULL,
  PRIMARY KEY (`tariff_id`,`partner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `tenants_iprestrictions` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `tenant` int(10) NOT NULL,
  `ip` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tenant` (`tenant`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `webstudio_index` (
  `index_name` varchar(50) NOT NULL,
  `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`index_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `mail_server_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tenant` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `id_domain` int(11) DEFAULT NULL,
  `id_mailbox` int(11) DEFAULT NULL,
  `is_mail_group` tinyint(4) DEFAULT '0',
  `is_alias` tinyint(4) DEFAULT '0',
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_tenant_index` (`id`,`tenant`),
  KEY `id_domain_fk_index` (`id_domain`),
  KEY `id_mailbox_fk_index` (`id_mailbox`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `mail_server_dns` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tenant` int(11) NOT NULL,
  `id_user` varchar(255) NOT NULL,
  `id_domain` int(11) NOT NULL DEFAULT '-1',
  `dkim_selector` varchar(63) NOT NULL DEFAULT 'dkim',
  `dkim_private_key` text,
  `dkim_public_key` text,
  `domain_check` text,
  `spf` text,
  `time_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `id_domain_tenant_id_user` (`id_domain`,`tenant`,`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `mail_server_domain` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tenant` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_tenant_index` (`id`,`tenant`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `mail_server_domain_x_cname` (
  `id_domain` int(11) NOT NULL,
  `cname` varchar(32) NOT NULL,
  `reference_url` varchar(256) NOT NULL,
  `verified` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`cname`),
  KEY `domain_id_x_mail_server_domain_id_fk_index` (`id_domain`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `mail_server_mail_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_tenant` int(11) NOT NULL,
  `id_address` int(11) NOT NULL,
  `date_created` datetime NOT NULL,
  `address` varchar(320) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `mail_server_address_fk_id` (`id_address`),
  KEY `mail_server_mail_group_tenant_seatch_indx` (`id`,`id_tenant`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `mail_server_mail_group_x_mail_server_address` (
  `id_address` int(11) NOT NULL,
  `id_mail_group` int(11) NOT NULL,
  PRIMARY KEY (`id_address`,`id_mail_group`),
  KEY `mail_server_mail_group_fk_id` (`id_mail_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `mail_server_server` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mx_record` varchar(128) DEFAULT NULL,
  `connection_string` varchar(256) NOT NULL,
  `server_type` int(11) NOT NULL,
  `smtp_settings_id` int(11) NOT NULL,
  `imap_settings_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `mail_mailbox_smtp_server_fk_id` (`smtp_settings_id`),
  KEY `mail_mailbox_imap_server_fk_id` (`imap_settings_id`),
  KEY `mail_server_server_type_server_type_fk_id` (`server_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `mail_server_server_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `mail_server_server_x_tenant` (
  `id_tenant` int(11) NOT NULL,
  `id_server` int(11) NOT NULL,
  `cname` varchar(34) NOT NULL,
  PRIMARY KEY (`id_tenant`,`id_server`),
  KEY `mail_server_server_x_tenant_server_fk_id` (`id_server`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

alter table mail_mailbox add column `is_teamlab_mailbox` int(11) NOT NULL DEFAULT 0;
alter table mail_mailbox add column `date_created` datetime DEFAULT NULL;