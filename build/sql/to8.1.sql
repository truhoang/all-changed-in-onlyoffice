drop table if exists audit_events_old;
drop table if exists blogs_posts_old;
drop table if exists crm_field_value_old;
drop table if exists feed_aggregate_old;
drop table if exists feed_last_old;
drop table if exists feed_users_old;
drop table if exists projects_comments_old;
drop table if exists wiki_pages_old;

-- ������� ������!!!
drop table if exists blogs_blogs;
drop table if exists files_photo_export_tmp;
drop table if exists photo_comment;
drop table if exists photo_album;
drop table if exists photo_event;
drop table if exists photo_image;
drop table if exists photo_imageview;
drop table if exists push_app_user;
drop table if exists push_device;
drop table if exists push_notification;
drop table if exists tenants_tariffcoupon;
drop table if exists webstudio_widgetcontainer;
drop table if exists webstudio_widgetstate;

ALTER TABLE `backup_schedule` ADD COLUMN `backup_mail` INT(11) NOT NULL DEFAULT '0' AFTER `tenant_id`;

ALTER TABLE `mail_alerts` CHANGE COLUMN `type` `type` INT(11) NOT NULL DEFAULT '0' AFTER `id_mailbox`, DROP INDEX `tenant_id_user`;

CREATE TABLE IF NOT EXISTS `notify_tip_watch` (
	`tip_id` VARCHAR(50) NOT NULL,
	`user_id` VARCHAR(38) NOT NULL,
	`tenant_id` INT(11) NOT NULL,
	PRIMARY KEY (`tip_id`, `user_id`),
	INDEX `tenant_id` (`tenant_id`)
) COLLATE='utf8_general_ci' ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `mobile_app_install` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_email` varchar(255) NOT NULL,
  `app_type` int(11) NOT NULL,
  `registered_on` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_email` (`user_email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `tenants_quota`
	DROP COLUMN `https_enable`,
	DROP COLUMN `security_enable`,
	DROP COLUMN `sms_auth`,
	DROP COLUMN `branding`;

ALTER TABLE `notify_promotion_watch`
	DROP PRIMARY KEY,
	ADD PRIMARY KEY (`user_id`, `id`);
    
delete from notify_queue where creation_date < '2014-12-04 00:00:00';
truncate table mail_log;

ALTER TABLE mail_mailbox
	ADD `is_teamlab_mailbox` INT(11) NOT NULL DEFAULT '0',
	ADD `date_created` DATETIME NULL DEFAULT NULL;

CREATE TABLE IF NOT EXISTS `mail_server_domain` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tenant` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_tenant_index` (`id`,`tenant`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `mail_server_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tenant` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `id_domain` int(11) DEFAULT NULL,
  `id_mailbox` int(11) DEFAULT NULL,
  `is_mail_group` tinyint(4) DEFAULT '0',
  `is_alias` tinyint(4) DEFAULT '0',
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_tenant_index` (`id`,`tenant`),
  KEY `id_domain_fk_index` (`id_domain`),
  KEY `id_mailbox_fk_index` (`id_mailbox`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `mail_server_server` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mx_record` varchar(128) DEFAULT NULL,
  `connection_string` varchar(256) NOT NULL,
  `server_type` int(11) NOT NULL,
  `smtp_settings_id` int(11) NOT NULL,
  `imap_settings_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `mail_mailbox_smtp_server_fk_id` (`smtp_settings_id`),
  KEY `mail_mailbox_imap_server_fk_id` (`imap_settings_id`),
  KEY `mail_server_server_type_server_type_fk_id` (`server_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `mail_server_server_x_tenant` (
  `id_tenant` int(11) NOT NULL,
  `id_server` int(11) NOT NULL,
  `cname` varchar(34) NOT NULL,
  PRIMARY KEY (`id_tenant`,`id_server`),
  KEY `mail_server_server_x_tenant_server_fk_id` (`id_server`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `mail_server_server_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `mail_server_mail_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_tenant` int(11) NOT NULL,
  `id_address` int(11) NOT NULL,
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `mail_server_address_fk_id` (`id_address`),
  KEY `mail_server_mail_group_tenant_seatch_indx` (`id`,`id_tenant`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `mail_server_mail_group_x_mail_server_address` (
  `id_address` int(11) NOT NULL,
  `id_mail_group` int(11) NOT NULL,
  PRIMARY KEY (`id_address`,`id_mail_group`),
  KEY `mail_server_mail_group_fk_id` (`id_mail_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `mail_server_domain_x_cname` (
  `id_domain` int(11) NOT NULL,
  `cname` varchar(32) NOT NULL,
  `reference_url` varchar(256) NOT NULL,
  `verified` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`cname`),
  KEY `domain_id_x_mail_server_domain_id_fk_index` (`id_domain`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `mail_server_dns` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`tenant` INT(11) NOT NULL,
	`id_user` VARCHAR(255) NOT NULL,
	`id_domain` INT(11) NOT NULL DEFAULT '-1',
	`dkim_selector` VARCHAR(63) NOT NULL DEFAULT 'dkim',
	`dkim_private_key` TEXT NULL,
	`dkim_public_key` TEXT NULL,
	`domain_check` TEXT NULL,
	`spf` TEXT NULL,
	`time_modified` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`),
	INDEX `id_domain_tenant_id_user` (`id_domain`, `tenant`, `id_user`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, REFERENCES, INDEX, ALTER, CREATE VIEW, SHOW VIEW ON `teamlab_v2_0`.`mail_server_domain` TO 'tm-mail-eu-com'@'%' WITH GRANT OPTION;
GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, REFERENCES, INDEX, ALTER, CREATE VIEW, SHOW VIEW ON `teamlab_v2_0`.`mail_server_address` TO 'tm-mail-eu-com'@'%' WITH GRANT OPTION;
GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, REFERENCES, INDEX, ALTER, CREATE VIEW, SHOW VIEW ON `teamlab_v2_0`.`mail_server_server` TO 'tm-mail-eu-com'@'%' WITH GRANT OPTION;
GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, REFERENCES, INDEX, ALTER, CREATE VIEW, SHOW VIEW ON `teamlab_v2_0`.`mail_server_server_x_tenant` TO 'tm-mail-eu-com'@'%' WITH GRANT OPTION;
GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, REFERENCES, INDEX, ALTER, CREATE VIEW, SHOW VIEW ON `teamlab_v2_0`.`mail_server_server_type` TO 'tm-mail-eu-com'@'%' WITH GRANT OPTION;
GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, REFERENCES, INDEX, ALTER, CREATE VIEW, SHOW VIEW ON `teamlab_v2_0`.`mail_server_mail_group` TO 'tm-mail-eu-com'@'%' WITH GRANT OPTION;
GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, REFERENCES, INDEX, ALTER, CREATE VIEW, SHOW VIEW ON `teamlab_v2_0`.`mail_server_mail_group_x_mail_server_address` TO 'tm-mail-eu-com'@'%' WITH GRANT OPTION;
GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, REFERENCES, INDEX, ALTER, CREATE VIEW, SHOW VIEW ON `teamlab_v2_0`.`mail_server_domain_x_cname` TO 'tm-mail-eu-com'@'%' WITH GRANT OPTION;
GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, REFERENCES, INDEX, ALTER, CREATE VIEW, SHOW VIEW ON `teamlab_v2_0`.`mail_server_dns` TO 'tm-mail-eu-com'@'%' WITH GRANT OPTION;

ALTER TABLE mail_server_mail_group ADD `address` varchar(320) NOT NULL;

-- tariffs
update tenants_quota set name = 'trial', features = 'trial,backup,domain,docs', active_users = 300 where tenant = -1;
update tenants_quota set visible = 0 where tenant in (-20, -31, -48);
update tenants_quota set visible = 1 where tenant = -53;

create temporary table _id (id int primary key);
insert ignore into _id select t.id from tenants_tenants t inner join tenants_tariff f on t.id = f.tenant where f.tariff = -1;
insert ignore into _id select t.id from tenants_tenants t left outer join tenants_tariff f on t.id = f.tenant where f.tariff is null;
update tenants_tenants set last_modified = utc_timestamp(), version_changed = date_add(utc_timestamp(), interval 45 day) where id in (select id from _id);
drop table if exists _id;


TRUNCATE `files_converts`;
INSERT IGNORE INTO `files_converts` (`input`, `output`) VALUES ('.ods', '.csv');
INSERT IGNORE INTO `files_converts` (`input`, `output`) VALUES ('.xls', '.csv');
INSERT IGNORE INTO `files_converts` (`input`, `output`) VALUES ('.xlst', '.csv');
INSERT IGNORE INTO `files_converts` (`input`, `output`) VALUES ('.xlsx', '.csv');
INSERT IGNORE INTO `files_converts` (`input`, `output`) VALUES ('.doc', '.docx');
INSERT IGNORE INTO `files_converts` (`input`, `output`) VALUES ('.doct', '.docx');
INSERT IGNORE INTO `files_converts` (`input`, `output`) VALUES ('.html', '.docx');
INSERT IGNORE INTO `files_converts` (`input`, `output`) VALUES ('.mht', '.docx');
INSERT IGNORE INTO `files_converts` (`input`, `output`) VALUES ('.odt', '.docx');
INSERT IGNORE INTO `files_converts` (`input`, `output`) VALUES ('.rtf', '.docx');
INSERT IGNORE INTO `files_converts` (`input`, `output`) VALUES ('.txt', '.docx');
INSERT IGNORE INTO `files_converts` (`input`, `output`) VALUES ('.csv', '.ods');
INSERT IGNORE INTO `files_converts` (`input`, `output`) VALUES ('.xls', '.ods');
INSERT IGNORE INTO `files_converts` (`input`, `output`) VALUES ('.xlst', '.ods');
INSERT IGNORE INTO `files_converts` (`input`, `output`) VALUES ('.xlsx', '.ods');
INSERT IGNORE INTO `files_converts` (`input`, `output`) VALUES ('.doc', '.odt');
INSERT IGNORE INTO `files_converts` (`input`, `output`) VALUES ('.doct', '.odt');
INSERT IGNORE INTO `files_converts` (`input`, `output`) VALUES ('.docx', '.odt');
INSERT IGNORE INTO `files_converts` (`input`, `output`) VALUES ('.html', '.odt');
INSERT IGNORE INTO `files_converts` (`input`, `output`) VALUES ('.mht', '.odt');
INSERT IGNORE INTO `files_converts` (`input`, `output`) VALUES ('.rtf', '.odt');
INSERT IGNORE INTO `files_converts` (`input`, `output`) VALUES ('.txt', '.odt');
INSERT IGNORE INTO `files_converts` (`input`, `output`) VALUES ('.pps', '.pptx');
INSERT IGNORE INTO `files_converts` (`input`, `output`) VALUES ('.ppsx', '.pptx');
INSERT IGNORE INTO `files_converts` (`input`, `output`) VALUES ('.ppt', '.pptx');
INSERT IGNORE INTO `files_converts` (`input`, `output`) VALUES ('.pptt', '.pptx');
INSERT IGNORE INTO `files_converts` (`input`, `output`) VALUES ('.svgt', '.svg');
INSERT IGNORE INTO `files_converts` (`input`, `output`) VALUES ('.doc', '.txt');
INSERT IGNORE INTO `files_converts` (`input`, `output`) VALUES ('.doct', '.txt');
INSERT IGNORE INTO `files_converts` (`input`, `output`) VALUES ('.docx', '.txt');
INSERT IGNORE INTO `files_converts` (`input`, `output`) VALUES ('.html', '.txt');
INSERT IGNORE INTO `files_converts` (`input`, `output`) VALUES ('.mht', '.txt');
INSERT IGNORE INTO `files_converts` (`input`, `output`) VALUES ('.odt', '.txt');
INSERT IGNORE INTO `files_converts` (`input`, `output`) VALUES ('.rtf', '.txt');
INSERT IGNORE INTO `files_converts` (`input`, `output`) VALUES ('.csv', '.xlsx');
INSERT IGNORE INTO `files_converts` (`input`, `output`) VALUES ('.ods', '.xlsx');
INSERT IGNORE INTO `files_converts` (`input`, `output`) VALUES ('.xls', '.xlsx');
INSERT IGNORE INTO `files_converts` (`input`, `output`) VALUES ('.xlst', '.xlsx');

INSERT INTO `tenants_quota` (`tenant`, `name`, `description`, `max_file_size`, `max_total_size`, `active_users`, `features`, `price`, `price2`, `avangate_id`, `visible`) VALUES (-54, '#1 year + docs (invisible)', NULL, 1024, 20480, 10, 'backup,domain,docs,year,sms', 200.00, 0.00, '131', 0);