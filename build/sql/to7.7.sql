ALTER TABLE `account_links`
	CHANGE COLUMN `provider` `provider` CHAR(60) NULL DEFAULT NULL AFTER `uid`;

ALTER TABLE `audit_events`
	ADD COLUMN `initiator` VARCHAR(200) NULL DEFAULT NULL AFTER `ip`,
	CHANGE COLUMN `user_id` `user_id` CHAR(38) NULL DEFAULT NULL AFTER `tenant_id`,
	CHANGE COLUMN `page` `page` VARCHAR(300) NULL DEFAULT NULL AFTER `user_id`;

ALTER TABLE `files_thirdparty_app`
	ADD COLUMN `app` VARCHAR(50) NOT NULL AFTER `user_id`,
	ADD COLUMN `modified_on` DATETIME NULL DEFAULT NULL AFTER `tenant_id`,
	DROP PRIMARY KEY,
	ADD PRIMARY KEY (`user_id`, `app`);

ALTER TABLE `projects_project_participant`
	DROP PRIMARY KEY,
	ADD PRIMARY KEY (`tenant`, `project_id`, `participant_id`);

CREATE TABLE IF NOT EXISTS `sso_links` (
  `id` varchar(200) NOT NULL,
  `uid` varchar(200) NOT NULL,
  `profile` varchar(200) NOT NULL,
  PRIMARY KEY (`id`,`uid`,`profile`)
);

-- Data exporting was unselected.


-- sso_tokens
CREATE TABLE IF NOT EXISTS `sso_tokens` (
  `tokenType` varchar(50) NOT NULL,
  `tenant` int(11) NOT NULL,
  `tokenId` varchar(100) NOT NULL,
  `expirationDate` datetime NOT NULL,
  UNIQUE KEY `tokenId` (`tokenId`)
);

ALTER TABLE `tenants_tenants`
	ADD COLUMN `industry` INT(10) NOT NULL DEFAULT '0' AFTER `payment_id`;
