REPLACE INTO `core_settings` (`tenant`, `id`, `value`) VALUES (-1, 'TrialPeriod', 0x3330);
create temporary table _id (id int primary key);
insert into _id select t.id from tenants_tenants t left outer join tenants_tariff f on t.id = f.tenant where f.tenant is null and t.version_changed >= date_add(utc_timestamp(),interval -45 day);
update tenants_tenants set last_modified = utc_timestamp(), version_changed = date_add(version_changed, interval 15 day) where id in (select id from _id);
drop table if exists _id;
