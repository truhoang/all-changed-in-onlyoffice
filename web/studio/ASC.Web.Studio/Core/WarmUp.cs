/*
 * 
 * (c) Copyright Ascensio System SIA 2010-2015
 * 
 * This program is a free software product.
 * You can redistribute it and/or modify it under the terms of the GNU Affero General Public License
 * (AGPL) version 3 as published by the Free Software Foundation. 
 * In accordance with Section 7(a) of the GNU AGPL its Section 15 shall be amended to the effect 
 * that Ascensio System SIA expressly excludes the warranty of non-infringement of any third-party rights.
 * 
 * This program is distributed WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * For details, see the GNU AGPL at: http://www.gnu.org/licenses/agpl-3.0.html
 * 
 * You can contact Ascensio System SIA at Lubanas st. 125a-25, Riga, Latvia, EU, LV-1021.
 * 
 * The interactive user interfaces in modified source and object code versions of the Program 
 * must display Appropriate Legal Notices, as required under Section 5 of the GNU AGPL version 3.
 * 
 * Pursuant to Section 7(b) of the License you must retain the original Product logo when distributing the program. 
 * Pursuant to Section 7(e) we decline to grant you any rights under trademark law for use of our trademarks.
 * 
 * All the Product's GUI elements, including illustrations and icon sets, as well as technical 
 * writing content are licensed under the terms of the Creative Commons Attribution-ShareAlike 4.0 International. 
 * See the License terms at http://creativecommons.org/licenses/by-sa/4.0/legalcode
 * 
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Web;
using System.Web.Optimization;
using ASC.Common.Threading.Workers;
using ASC.Core;
using ASC.Web.Core.Utility.Settings;
using ASC.Web.Studio.Utility;
using log4net;

namespace ASC.Web.Studio.Core
{
    public class WarmUp
    {
        private readonly object obj = new object();
        private WorkerQueue<string> requests;
        private readonly int tenantId;

        public StartupProgress Progress { get; private set; }

        private static WarmUp instance;
        public static WarmUp Instance
        {
            get { return instance ?? (instance = new WarmUp()); }
        }

        public WarmUp()
        {
            tenantId = CoreContext.TenantManager.GetCurrentTenant().TenantId;
            var listUrls = new List<string>
                {
                    "~/default.aspx",
                    "~/feed.aspx",
                    "~/products/files/default.aspx",
                    "~/products/files/doceditor.aspx",
                    "~/products/crm/cases.aspx",
                    "~/products/crm/deals.aspx",
                    "~/products/crm/default.aspx",
                    "~/products/crm/help.aspx",
                    "~/products/crm/invoices.aspx",
                    "~/products/crm/mailviewer.aspx",
                    "~/products/crm/sender.aspx",
                    "~/products/crm/settings.aspx",
                    "~/products/crm/tasks.aspx",
                    "~/products/projects/contacts.aspx",
                    "~/products/projects/default.aspx",
                    "~/products/projects/ganttchart.aspx",
                    "~/products/projects/GeneratedReport.aspx",
                    "~/products/projects/help.aspx",
                    "~/products/projects/import.aspx",
                    "~/products/projects/messages.aspx",
                    "~/products/projects/milestones.aspx",
                    "~/products/projects/projects.aspx",
                    //"~/products/projects/projectteam.aspx",
                    "~/products/projects/projecttemplates.aspx",
                    "~/products/projects/reports.aspx",
                    "~/products/projects/tasks.aspx",
                    "~/products/projects/timer.aspx",
                    "~/products/projects/timetracking.aspx",
                    "~/products/projects/tmdocs.aspx",
                    "~/products/people/default.aspx",
                    "~/products/people/help.aspx",
                    "~/products/people/profile.aspx",
                    "~/products/people/profileaction.aspx",
                    "~/addons/mail/default.aspx",
                };
            if (!WorkContext.IsMono)
            {
                listUrls.AddRange(new List<string> {
                    "~/auth.aspx",
                    "~/confirm.aspx",
                    "~/management.aspx?type=1",
                    "~/management.aspx?type=2",
                    "~/management.aspx?type=3",
                    "~/management.aspx?type=4",
                    "~/management.aspx?type=5",
                    "~/management.aspx?type=6",
                    "~/management.aspx?type=7",
                    "~/management.aspx?type=10",
                    "~/management.aspx?type=11",
                    "~/management.aspx?type=15",
                    "~/my.aspx",
                    "~/preparationportal.aspx",
                    "~/search.aspx",
                    "~/servererror.aspx",
                    "~/startscriptsstyles.aspx",
                    "~/tariffs.aspx",
                    "~/products/community/default.aspx",
                    "~/products/community/help.aspx",
                    "~/products/community/modules/birthdays/default.aspx",
                    "~/products/community/modules/blogs/addblog.aspx",
                    "~/products/community/modules/blogs/default.aspx",
                    "~/products/community/modules/blogs/editblog.aspx",
                    "~/products/community/modules/blogs/viewblog.aspx",
                    "~/products/community/modules/bookmarking/default.aspx",
                    "~/products/community/modules/bookmarking/createbookmark.aspx",
                    "~/products/community/modules/bookmarking/bookmarkinfo.aspx",
                    "~/products/community/modules/bookmarking/favouritebookmarks.aspx",
                    "~/products/community/modules/bookmarking/userbookmarks.aspx",
                    "~/products/community/modules/forum/default.aspx",
                    "~/products/community/modules/forum/edittopic.aspx",
                    "~/products/community/modules/forum/managementcenter.aspx",
                    "~/products/community/modules/forum/newforum.aspx",
                    "~/products/community/modules/forum/newpost.aspx",
                    "~/products/community/modules/forum/posts.aspx",
                    "~/products/community/modules/forum/search.aspx",
                    "~/products/community/modules/forum/topics.aspx",
                    "~/products/community/modules/forum/usertopics.aspx",
                    "~/products/community/modules/news/default.aspx",
                    "~/products/community/modules/news/editnews.aspx",
                    "~/products/community/modules/news/editpoll.aspx",
                    "~/products/community/modules/news/news.aspx",
                    //"~/products/community/modules/wiki/default.aspx",
                    "~/products/community/modules/wiki/diff.aspx",
                    "~/products/community/modules/wiki/listcategories.aspx",
                    "~/products/community/modules/wiki/listfiles.aspx",
                    "~/products/community/modules/wiki/listpages.aspx",
                    //"~/products/community/modules/wiki/pagehistorylist.aspx",
                    "~/addons/calendar/default.aspx"
                });
            }
            Progress = new StartupProgress(listUrls.Count);

            requests = new WorkerQueue<string>(4, TimeSpan.Zero, 10, true);
            requests.AddRange(listUrls);
        }

        private void LoaderPortalPages(string requestUrl)
        {
            try
            {
                CoreContext.TenantManager.SetCurrentTenant(tenantId);

                var authCookie = SecurityContext.AuthenticateMe(SecurityContext.CurrentAccount.ID);
                var tenant = CoreContext.Configuration.Standalone
                    ? "localhost"
                    : CoreContext.TenantManager.GetCurrentTenant().TenantDomain;

                var httpWebRequest = (HttpWebRequest)WebRequest.Create(CommonLinkUtility.GetFullAbsolutePath(requestUrl));
                httpWebRequest.Headers.Add("Authorization", authCookie);
                httpWebRequest.Method = "GET";
                httpWebRequest.CookieContainer = new CookieContainer();
                httpWebRequest.CookieContainer.Add(new Cookie("asc_auth_key", authCookie, "/", tenant));
                httpWebRequest.Timeout = 120000;

                httpWebRequest.GetResponse();
            }
            catch (Exception exception)
            {
                lock (obj)
                {
                    Progress.Error.Add(exception.StackTrace);
                }

                LogManager.GetLogger("ASC.Web")
                    .Error(string.Format("Page is not avaliable by url {0}", requestUrl), exception);
            }
            finally
            {
                lock (obj)
                {
                    Progress.Increment();
                    if (Progress.IsCompleted)
                    {
                        Terminate();
                        LogManager.GetLogger("ASC").Debug("Complete");
                    }
                }
            }
        }

        public void Start()
        {
            if (!requests.IsStarted)
            {
                requests.Start(LoaderPortalPages);
            }
        }

        public void Terminate()
        {
            if (requests != null)
            {
                requests.Terminate();
                requests = null;
            }
        }
    }

    [DataContract]
    public class StartupProgress
    {
        private int progress;
        private readonly int total;

        [DataMember]
        public bool IsCompleted { get { return progress == total; } }

        [DataMember]
        public int ProgressPercent { get { return (progress * 75) / total; } }

        [DataMember]
        public List<string> Error { get; set; }

        [DataMember]
        public string Link { get; set; }

        [DataMember]
        public List<string> Bundles { get; set; }

        public StartupProgress(int total)
        {
            this.total = total;
            Error = new List<string>();
        }

        public void Increment()
        {
            progress += 1;

            if (IsCompleted)
            {
                Link = VirtualPathUtility.ToAbsolute("~/default.aspx");
                Bundles = BundleTable.Bundles.Select(bundle => VirtualPathUtility.ToAbsolute(bundle.Path)).ToList();
            }
        }
    }

    [Serializable]
    [DataContract]
    public class StartupSettings : ISettings
    {
        [DataMember(Name = "Start")]
        public bool Start { get; set; }

        public Guid ID
        {
            get { return new Guid("{098F4CB6-A7EF-4D79-9A0E-73F5846F6B2F}"); }
        }


        public ISettings GetDefault()
        {
            return new StartupSettings();
        }
    }
}