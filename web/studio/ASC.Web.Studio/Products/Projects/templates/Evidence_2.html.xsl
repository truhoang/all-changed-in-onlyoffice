﻿<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:output method="html" />

  <xsl:param name="p0"/>
  <xsl:param name="p1"/>
  <xsl:param name="p2"/>
  <xsl:param name="p3"/>
  <xsl:param name="p4"/>
  <xsl:param name="p5"/>
  <xsl:param name="p6"/>
  <xsl:param name="p7"/>
  <xsl:param name="p8"/>
  <xsl:param name="p9"/>
  <xsl:param name="p10"/>
  <xsl:param name="p11"/>
  <xsl:param name="p12"/>
  <xsl:param name="p13"/>
  <xsl:param name="p14"/>
  <xsl:param name="p15"/>
  <xsl:param name="p16"/>

  <xsl:decimal-format NaN = "0.00" />

  <xsl:key name="users" match="r" use="@c0" />
  <xsl:key name="userColGroup" match="r" use="concat(@c0, '|', @c11)"/>
  <xsl:key name="userColGroupPrj" match="r" use="concat(@c0, '|', @c11, '|', @c1)"/>
  <xsl:key name="userColGroupPrjTask" match="r" use="concat(@c0, '|', @c11, '|', @c1, '|', @c5)"/>

  <xsl:template match="*">
    <div id="columnchartreportBody">
      <xsl:call-template name="reportInfo"/>
      <xsl:variable name="allUsers" select="r[generate-id(.)=generate-id(key('users', @c0))]"/>
      <xsl:apply-templates select="$allUsers"/>
    </div>
    <script type="text/javascript">
      <xsl:attribute name="src">
        https://www.google.com/jsapi?autoload={'modules':[{'name':'visualization','version':'1.1','packages':['corechart']}]}
      </xsl:attribute>
      &#160;
    </script>
  </xsl:template>

  <xsl:template name="reportInfo">
    <reportInfo>
      <xsl:attribute name="fromDate">
        <xsl:value-of select="$p14"/>
      </xsl:attribute>
      <xsl:attribute name="toDate">
        <xsl:value-of select="$p15"/>
      </xsl:attribute>
      <xsl:attribute name="timeIntervalType">
        <xsl:value-of select="$p16"/>
      </xsl:attribute>
      &#160;
    </reportInfo>
  </xsl:template>

  <xsl:template match="r">
    <xsl:variable name="user" select="@c0"/>
    <!--row representing user-->
    <user>
      <!--display name and link-->
      <xsl:attribute name="link">
        <xsl:value-of select="@c13"/>
        <!--<xsl:value-of select="concat($p13,@c14)"/>-->
      </xsl:attribute>
      <xsl:attribute name="DisplayName">
        <xsl:value-of select="@c12"/>
      </xsl:attribute>
      <!--time spent-->
      <xsl:variable name="timeSpent" select="sum(key('users', @c0)/@c9)"/>
      <xsl:attribute name="timeSpent">
        <xsl:value-of select="$timeSpent"/>
      </xsl:attribute>
      <!--user's project-->
      <xsl:apply-templates mode="colGroup" select="key('users', $user) [generate-id() = generate-id(key('userColGroup', concat($user, '|', @c11)))]">
        <!--<xsl:sort select="@c2" data-type="text"/>-->
      </xsl:apply-templates>
    </user>
  </xsl:template>

  <xsl:template match="r" mode="colGroup">
    <xsl:variable name="user" select="@c0"/>
    <xsl:variable name="colGroup" select="@c11"/>
    <columnGroup>
      <!--column group number-->
      <xsl:attribute name="colNumber">
        <xsl:value-of select="@c11"/>
      </xsl:attribute>

      <!--date for sorting-->
      <xsl:attribute name="dateOrder">
        <xsl:value-of select="@c10"/>
      </xsl:attribute>

      <!--time spent-->
      <xsl:variable name='colGroupTimeSpent' select="sum(key('userColGroup',  concat(@c0, '|', @c11))/@c9)"/>
      <xsl:attribute name="timeSpent">
        <xsl:value-of select="$colGroupTimeSpent"/>
      </xsl:attribute>
      <!--column group projects-->
      <xsl:apply-templates mode="proj" select="key('userColGroup', concat($user, '|', $colGroup))[generate-id()
        = generate-id(key('userColGroupPrj', concat($user, '|', $colGroup, '|', @c1)))]">
      </xsl:apply-templates>
    </columnGroup>
  </xsl:template>

  <xsl:template match="r" mode="proj">
    <xsl:variable name="user" select="@c0"/>
    <xsl:variable name="colGroup" select="@c11"/>
    <xsl:variable name="prj" select="@c1"/>
    <project>
      <!--projectId-->
      <xsl:attribute name="id">
        <xsl:value-of select="@c1"/>
      </xsl:attribute>
      <!--title and link-->
      <xsl:attribute name="link">
        <xsl:value-of select="concat($p13,'/products/projects/projects.aspx?prjID=',@c1)"/>
      </xsl:attribute>
      <xsl:attribute name="title">
        <xsl:value-of select="@c2"/>
      </xsl:attribute>
      <!--time spent-->
      <xsl:variable name='projectTimeSpent' select="sum(key('userColGroupPrj',  concat($user, '|',
        $colGroup, '|', $prj))/@c9)"/>
      <xsl:attribute name="timeSpent">
        <xsl:value-of select="$projectTimeSpent"/>
      </xsl:attribute>
      <!--project's tasks-->
      <xsl:apply-templates mode="task" select="key('userColGroupPrj', concat($user, '|',$colGroup, '|', $prj))[generate-id()
        = generate-id(key('userColGroupPrjTask', concat($user, '|',$colGroup, '|', $prj, '|', @c5)))]">
      </xsl:apply-templates>
    </project>
  </xsl:template>

  <xsl:template match="r" mode="task">
    <task>
      <!--title and link-->
      <xsl:attribute name="link">
        <xsl:value-of select="concat($p13, '/products/projects/tasks.aspx?prjID=' ,@c1, '&amp;ID=', @c5)"/>
      </xsl:attribute>
      <xsl:attribute name="title">
        <xsl:value-of select="@c6"/>
      </xsl:attribute>
      <!--time spent-->
      <xsl:variable name='taskTimeSpent' select="sum(key('userColGroupPrjTask',  concat(@c0, '|', @c11, '|', @c1, '|', @c5))/@c9)"/>
      <xsl:attribute name="timeSpent">
        <xsl:value-of select="$taskTimeSpent"/>
      </xsl:attribute>
      &#160;
    </task>
  </xsl:template>
</xsl:stylesheet>