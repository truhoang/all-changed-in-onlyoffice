﻿<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:output method="html" />

  <xsl:param name="p0"/>
  <xsl:param name="p1"/>
  <xsl:param name="p2"/>
  <xsl:param name="p3"/>
  <xsl:param name="p4"/>
  <xsl:param name="p5"/>
  <xsl:param name="p6"/>
  <xsl:param name="p7"/>
  <xsl:param name="p8"/>
  <xsl:param name="p9"/>
  <xsl:param name="p10"/>
  <xsl:param name="p11"/>
  <xsl:param name="p12"/>
  <xsl:param name="p13"/>

  <xsl:decimal-format NaN = "0.00" />

  <xsl:key name="users" match="r" use="@c0" />
  <xsl:key name="userPrj" match="r" use="concat(@c0, '|', @c1)"/>
  <xsl:key name="userPrjTask" match="r" use="concat(@c0, '|', @c1, '|', @c5)"/>
  <xsl:key name="projects" match="r" use="@c1" />
  <xsl:key name="tasks" match="r" use="@c5" />

  <xsl:template match="*">

    <div id="reportBody">
      <table class="pm-tablebase no-sorted" cellspacing="0" cellpadding="6" width="100%">
        <thead>
          <tr>
            <!--users-->
            <td style="width:15%;text-align:left;">
              <span class="report-tableColumnHeader" unselectable="on">
                <xsl:value-of select="$p0"/>
              </span>
              <span id="sorttable_sortfwdind"></span>
            </td>
            <!--Month Salary-->
            <td style="width:7%;text-align: left;white-space: nowrap;">
              <span class="report-tableColumnHeader" unselectable="on">
                <xsl:value-of select="$p1"/>
              </span>
            </td>

            <!--Hour salary-->
            <td style="width:6%;text-align: left;white-space: nowrap;">
              <span class="report-tableColumnHeader" unselectable="on">
                <xsl:value-of select="$p2"/>
              </span>
            </td>

            <!--Cost-->
            <td style="width:9%;text-align: left;white-space: nowrap;">
              <span class="report-tableColumnHeader" unselectable="on">
                <xsl:value-of select="$p3"/>
              </span>
            </td>

            <!--Price-->
            <td style="width:9%;text-align: left;white-space: nowrap;">
              <span class="report-tableColumnHeader" unselectable="on">
                <xsl:value-of select="$p4"/>
              </span>
            </td>

            <!--Time Signed-->
            <td style="width:8%;text-align: left;white-space: nowrap;">
              <span class="report-tableColumnHeader" unselectable="on">
                <xsl:value-of select="$p5"/>
              </span>
            </td>

            <!--Time Spent-->
            <td style="width:8%;text-align: left;white-space: nowrap;">
              <span class="report-tableColumnHeader" unselectable="on">
                <xsl:value-of select="$p6"/>
              </span>
            </td>

            <!--Difference-->
            <td style="width:8%;text-align: left;white-space: nowrap;">
              <span class="report-tableColumnHeader" unselectable="on">
                <xsl:value-of select="$p7"/>
              </span>
            </td>

            <!--paid-->
            <td style="width:9%;text-align: left;white-space: nowrap;">
              <span class="report-tableColumnHeader" unselectable="on">
                <xsl:value-of select="$p8"/>
              </span>
            </td>
          </tr>
        </thead>
        <tbody>
          <xsl:variable name="allUsers" select="r[generate-id(.)=generate-id(key('users', @c0))]"/>
          <xsl:variable name="allTasks" select="r[generate-id(.)=generate-id(key('tasks', @c5))]"/>
          <xsl:apply-templates select="$allUsers"/>
          <tr>
            <td class="borderBase" style='width:15%;padding-left:8px; padding-left:8px;font-weight:bold;font-size:16px;'>
              <div style='width:99%;overflow:hidden;'>
                All total
              </div>
            </td>
            <xsl:call-template name="allTotalUsers">
              <xsl:with-param name="allUsers" select="$allUsers"/>
            </xsl:call-template>
            <xsl:call-template name="allTotalProject">
              <xsl:with-param name="allProjects" select="r[generate-id(.)=generate-id(key('projects', @c1))]"/>
            </xsl:call-template>
            <xsl:call-template name="allTotalSignedTime">
              <xsl:with-param name="allUsers" select="$allUsers"/>
            </xsl:call-template>
            <xsl:call-template name="allTotalTask">
              <xsl:with-param name="allTasks" select="$allTasks"/>
              <xsl:with-param name="allTimeSpents" select="r"/>
            </xsl:call-template>
          </tr>
        </tbody>
      </table>
    </div>
  </xsl:template>

  <xsl:template match="r">
    <xsl:variable name="user" select="@c0"/>
    <!--row representing user-->
    <tr>
      <td style='width:15%;padding-left:8px; padding-left:8px;font-weight:bold;font-size:16px; white-space: nowrap;'>
        <div style='width:99%;overflow:hidden;'>
          <a href='{$p13}{@c14}' class='report-big-header'>
            <xsl:value-of select="@c13"/>
          </a>
        </div>
      </td>
      <!--month salary-->
      <td  style='width:8%;text-align: left;'>
        <div style='width:99%;overflow:hidden;font-weight:bold;font-size:16px;'>
          <xsl:choose>
            <xsl:when test="@c15 > 0">
              <xsl:value-of select="@c15"/>
            </xsl:when>
            <xsl:otherwise>
              -
            </xsl:otherwise>
          </xsl:choose>
        </div>
      </td>
      <!--hour salary-->
      <td  style='width:8%;text-align: left;'>
        <div style='width:99%;overflow:hidden;font-weight:bold;font-size:16px;'>
          <xsl:choose>
            <xsl:when test="@c16 > 0">
              <xsl:value-of select="./@c16"/>
            </xsl:when>
            <xsl:otherwise>
              -
            </xsl:otherwise>
          </xsl:choose>
        </div>
      </td>
      <!--Cost-->
      <td  style='width:8%;text-align: left;'>
        <div style='width:99%;overflow:hidden;font-weight:bold;font-size:16px;'>
          -
        </div>
      </td>
      <!--Price-->
      <td  style='width:8%;text-align: left;'>
        <div style='width:99%;overflow:hidden;font-weight:bold;font-size:16px;'>
          -
        </div>
      </td>

      <xsl:variable name='allUserTimeSpent' select="key('users', @c0)"/>
      <!--time signed-->
      <xsl:variable name="timeSigned">
        <xsl:call-template name="GetTimeFromMunites">
          <xsl:with-param name="totalMinutes" select="sum($allUserTimeSpent[generate-id(.)=generate-id(key('tasks', @c5))]/@c12)"></xsl:with-param>
        </xsl:call-template>
      </xsl:variable>
      <td  style='width:8%;text-align: left;'>
        <div style='width:99%;overflow:hidden;font-weight:bold;font-size:16px;'>
          <xsl:value-of select="concat(@c19,$p10)"/>
          <!--<xsl:call-template name='GetTime'>
            <xsl:with-param name='time' select="$timeSigned"/>
          </xsl:call-template>-->
        </div>
      </td>
      <!--time spent-->
      <xsl:variable name="timeSpent" select="sum(key('users', @c0)/@c9)"/>
      <td  style='width:8%;text-align: left;'>
        <div style='width:99%;overflow:hidden;font-weight:bold;font-size:16px;'>
          <xsl:call-template name="GetTime">
            <xsl:with-param name="time" select="$timeSpent"/>
          </xsl:call-template>
        </div>
      </td>
      <!--difference-->
      <td  style='width:8%;text-align: left;'>
        <div style='width:99%;overflow:hidden;font-weight:bold;font-size:16px;'>
          <xsl:call-template name="GetTime">
            <xsl:with-param name="time">
              <xsl:call-template name="timeDiff">
                <xsl:with-param name="signedTime" select="$timeSigned"/>
                <xsl:with-param name="spentTime" select="$timeSpent"/>
              </xsl:call-template>
            </xsl:with-param>
          </xsl:call-template>
        </div>
      </td>
      <!--paid-->
      <td  style='width:8%;text-align: left;'>
        <div style='width:99%;overflow:hidden;font-weight:bold;font-size:16px;'>
          <xsl:call-template name='moneyFormat'>
            <xsl:with-param name='value' select='$timeSpent * @c16'/>
          </xsl:call-template>
        </div>
      </td>

    </tr>

    <xsl:apply-templates mode="proj" select="key('users', $user) [generate-id() = generate-id(key('userPrj', concat($user, '|', @c1)))]">
      <xsl:sort select="@c2" data-type="text"/>
    </xsl:apply-templates>

  </xsl:template>

  <!--for "project" row-->
  <xsl:template match="r" mode="proj">
    <xsl:variable name="user" select="@c0"/>
    <xsl:variable name="prj" select="@c1"/>
    <tr style="font-weight:bold;">
      <td class='borderBase' style='width:15%;padding-left:13px;white-space: nowrap;'>
        <span class="menu-item-icon projects" style='color:transparent'>.</span>
        <div style='width:70%;overflow:hidden;'>
          <a class='report-middle-header' href='{$p13}/products/projects/projects.aspx?prjID={@c1}'>
            <xsl:value-of select="@c2"/>
          </a>
        </div>
      </td>
      <!--month salary-->
      <td class='borderBase' style='width:8%;text-align: left;'>
        <div style='width:99%;overflow:hidden;font-size:12px;'>
          -
        </div>
      </td>
      <!--hour salary-->
      <td class='borderBase' style='width:8%;text-align: left;'>
        <div style='width:99%;overflow:hidden;font-size:12px;'>
          -
        </div>
      </td>
      <!--Cost-->
      <td class='borderBase' style='width:8%;text-align: left;'>
        <div style='width:99%;overflow:hidden;font-size:12px;'>
          <xsl:value-of select='@c3'/>
        </div>
      </td>
      <!--Price-->
      <td class='borderBase' style='width:8%;text-align: left;'>
        <div style='width:99%;overflow:hidden;font-size:12px;'>
          <xsl:value-of select='@c4'/>
        </div>
      </td>
      <!--time signed-->
      <xsl:variable name="projectTimeSigned">
        <xsl:call-template name="GetTimeFromMunites">
          <xsl:with-param name="totalMinutes" select="@c11"></xsl:with-param>
        </xsl:call-template>
      </xsl:variable>
      <td class='borderBase' style='width:8%;text-align: left;'>
        <div style='width:99%;overflow:hidden;font-size:12px;'>
          <xsl:call-template name="GetTime">
            <xsl:with-param name="time" select="$projectTimeSigned"></xsl:with-param>
          </xsl:call-template>
        </div>
      </td>
      <!--time spent-->
      <xsl:variable name='projectTimeSpent' select="sum(key('userPrj',  concat(@c0, '|', @c1))/@c9)"/>
      <td class='borderBase' style='width:8%;text-align: left;'>
        <div style='width:99%;overflow:hidden;font-size:12px;'>
          <xsl:call-template name="GetTime">
            <xsl:with-param name="time" select="$projectTimeSpent"/>
          </xsl:call-template>
        </div>
      </td>
      <!--difference-->
      <td class='borderBase' style='width:8%;text-align: left;'>
        <div style='width:99%;overflow:hidden;font-size:12px;'>
          <xsl:call-template name="GetTime">
            <xsl:with-param name="time">
              <xsl:call-template name="timeDiff">
                <xsl:with-param name="signedTime" select="$projectTimeSigned"/>
                <xsl:with-param name="spentTime" select="$projectTimeSpent"/>
              </xsl:call-template>
            </xsl:with-param>
          </xsl:call-template>
        </div>
      </td>
      <!--paid-->
      <td class='borderBase' style='width:8%;text-align: left;'>
        <div style='width:99%;overflow:hidden;font-size:12px;'>
          <xsl:call-template name='moneyFormat'>
            <xsl:with-param name='value' select='$projectTimeSpent * @c16'/>
          </xsl:call-template>
        </div>
      </td>
    </tr>

    <xsl:apply-templates mode="task" select="key('userPrj', concat($user, '|', $prj))[generate-id()
        = generate-id(key('userPrjTask', concat($user, '|', $prj, '|', @c5)))]">
    </xsl:apply-templates>
    <!--all tasks within a project-->
    <xsl:variable name="tasks" select="key('userPrj', concat($user, '|', $prj))"/>

    <xsl:call-template name="taskTotalComputation">
      <xsl:with-param name="tasks" select="$tasks"/>
    </xsl:call-template>
    <tr>
      <td colspan='2'>
        <br/>
      </td>
    </tr>
  </xsl:template>

  <!--for "task" row-->
  <xsl:template match="r" mode="task">
    <tr>
      <td class='borderBase' style='width:15%;padding-left:32px;white-space: nowrap;'>
        <span class='menu-item-icon tasks' style='color:transparent'>
          <xsl:attribute name="title">
            <xsl:value-of select="@c6"/>
          </xsl:attribute>.
        </span>
        <div style='width:70%;overflow:hidden;font-size:12px;'>
          <a href='{$p13}/products/projects/tasks.aspx?prjID={@c1}&amp;ID={@c5}'>
            <xsl:value-of select="@c6"/>
          </a>
        </div>
      </td>
      <!--month salary-->
      <td class='borderBase' style='width:8%;text-align: left;'>
        <div style='width:99%;overflow:hidden;font-size:12px;'>
          -
        </div>
      </td>
      <!--hour salary-->
      <td class='borderBase' style='width:8%;text-align: left;'>
        <div style='width:99%;overflow:hidden;font-size:12px;'>
          -
        </div>
      </td>
      <!--Cost-->
      <td class='borderBase' style='width:8%;text-align: left;'>
        <div style='width:99%;overflow:hidden;font-size:12px;'>
          <xsl:value-of select='@c7'/>
        </div>
      </td>
      <!--Price-->
      <td class='borderBase' style='width:8%;text-align: left;'>
        <div style='width:99%;overflow:hidden;font-size:12px;'>
          <xsl:value-of select='@c8'/>
        </div>
      </td>
      <!--time signed-->
      <xsl:variable name="taskTimeSigned">
        <xsl:call-template name="GetTimeFromMunites">
          <xsl:with-param name="totalMinutes" select="@c12"></xsl:with-param>
        </xsl:call-template>
      </xsl:variable>
      <td class='borderBase' style='width:8%;text-align: left;'>
        <div style='width:99%;overflow:hidden;font-size:12px;'>
          <xsl:call-template name="GetTime">
            <xsl:with-param name="time" select="$taskTimeSigned"></xsl:with-param>
          </xsl:call-template>
        </div>
      </td>
      <!--time spent-->
      <xsl:variable name='taskTimeSpent' select="sum(key('userPrjTask',  concat(@c0, '|', @c1, '|', @c5))/@c9)"/>
      <td class='borderBase' style='width:8%;text-align: left;'>
        <div style='width:99%;overflow:hidden;font-size:12px;'>
          <xsl:call-template name="GetTime">
            <xsl:with-param name="time" select="$taskTimeSpent"/>
          </xsl:call-template>
        </div>
      </td>
      <!--difference-->
      <td class='borderBase' style='width:8%;text-align: left;'>
        <div style='width:99%;overflow:hidden;font-size:12px;'>
          <xsl:call-template name="GetTime">
            <xsl:with-param name="time">
              <xsl:call-template name="timeDiff">
                <xsl:with-param name="signedTime" select="$taskTimeSigned"/>
                <xsl:with-param name="spentTime" select="$taskTimeSpent"/>
              </xsl:call-template>
            </xsl:with-param>
          </xsl:call-template>
        </div>
      </td>
      <!--paid-->
      <td class='borderBase' style='width:8%;text-align: left;'>
        <div style='width:99%;overflow:hidden;font-size:12px;'>
          <xsl:call-template name='moneyFormat'>
            <xsl:with-param name='value' select='$taskTimeSpent * @c16'/>
          </xsl:call-template>
        </div>
      </td>
    </tr>
  </xsl:template>

  <!--for "total task" row-->
  <xsl:template name="taskTotalComputation">
    <xsl:param name="tasks"/>
    <xsl:variable name="style">
    </xsl:variable>
    <tr>
      <td class="borderBase" style="width:8%;text-align: left;padding-left:32px;white-space: nowrap;">
        <div style="width:99%;overflow:hidden;font-size:12px;font-weight: bold;">
          Total task
        </div>
      </td>
      <!--month salary-->
      <xsl:call-template name="taskTotalStyle">
        <xsl:with-param name="value">
          -
        </xsl:with-param>
      </xsl:call-template>
      <!--hour salary-->
      <xsl:call-template name="taskTotalStyle">
        <xsl:with-param name="value">
          -
        </xsl:with-param>
      </xsl:call-template>

      <xsl:variable name="taskGroup" select="$tasks[not(./@c18 =
            ./following-sibling::node()/@c18)]"></xsl:variable>
      <!--Cost-->
      <xsl:call-template name="taskTotalStyle">
        <xsl:with-param name="value">
          <xsl:value-of select="sum($taskGroup/@c7)"/>
        </xsl:with-param>
      </xsl:call-template>
      <!--Price-->
      <xsl:call-template name="taskTotalStyle">
        <xsl:with-param name="value">
          <xsl:value-of select='sum($taskGroup/@c8)'/>
        </xsl:with-param>
      </xsl:call-template>
      <!--time signed-->
      <xsl:variable name="taskTimeSigned">
        <xsl:call-template name="GetTimeFromMunites">
          <xsl:with-param name="totalMinutes" select="sum($taskGroup/@c12)"/>
        </xsl:call-template>
      </xsl:variable>
      <xsl:call-template name="taskTotalStyle">
        <xsl:with-param name="value">
          <xsl:call-template name="GetTime">
            <xsl:with-param name="time" select="$taskTimeSigned"></xsl:with-param>
          </xsl:call-template>
        </xsl:with-param>
      </xsl:call-template>
      <!--time spent-->
      <xsl:variable name='taskTimeSpent' select="sum($tasks/@c9)"/>
      <xsl:call-template name="taskTotalStyle">
        <xsl:with-param name="value">
          <xsl:call-template name="GetTime">
            <xsl:with-param name="time" select="$taskTimeSpent"/>
          </xsl:call-template>
        </xsl:with-param>
      </xsl:call-template>
      <!--difference-->
      <xsl:call-template name="taskTotalStyle">
        <xsl:with-param name="value">
          <xsl:call-template name="GetTime">
            <xsl:with-param name="time">
              <xsl:call-template name="timeDiff">
                <xsl:with-param name="signedTime" select="$taskTimeSigned"/>
                <xsl:with-param name="spentTime" select="$taskTimeSpent"/>
              </xsl:call-template>
            </xsl:with-param>
          </xsl:call-template>
        </xsl:with-param>
      </xsl:call-template>
      <!--paid-->
      <xsl:call-template name="taskTotalStyle">
        <xsl:with-param name="value">
          <xsl:call-template name="moneyFormat">
            <xsl:with-param name="value" select='$taskTimeSpent * @c16'/>
          </xsl:call-template>
        </xsl:with-param>
      </xsl:call-template>
    </tr>
  </xsl:template>

  <xsl:template name='taskTotalStyle'>
    <xsl:param name='value'/>
    <td class="borderBase" style="width:8%;text-align: left;white-space: nowrap;">
      <div style="width:99%;overflow:hidden;font-size:12px;font-weight: bold;">
        <xsl:value-of select='$value'/>
      </div>
    </td>
  </xsl:template>

  <xsl:template name="GetTime">
    <xsl:param name="time"/>
    <xsl:variable name="absoluteTime" select="$time*($time >=0) - $time*($time &lt; 0)"/>
    <xsl:if test='($absoluteTime=0.00)'>
      -
    </xsl:if>

    <xsl:if test='($absoluteTime!=0.00)'>
      <xsl:variable name="minutesTemp" select="round((($absoluteTime)-floor($absoluteTime))*60)"/>
      <xsl:variable name="hours" select="floor($absoluteTime) + floor(($minutesTemp div 60))"/>
      <xsl:variable name="minutes" select="$minutesTemp mod 60"/>
      <xsl:if test="0.00 &lt; $time">
        <xsl:value-of select="concat($hours,$p10,' ',format-number($minutes,'00'),$p11)"/>
      </xsl:if>
      <xsl:if test="0.00 &gt; $time">
        <xsl:value-of select="concat('-',$hours,$p10,' ',format-number($minutes,'00'),$p11)"/>
      </xsl:if>
    </xsl:if>
  </xsl:template>

  <xsl:template name="GetTimeFromMunites">
    <xsl:param name="totalMinutes"/>
    <xsl:value-of select="($totalMinutes div 60)"/>
  </xsl:template>

  <!--for all total row (last row)-->
  <xsl:template name="allTotalUsers">
    <xsl:param name="allUsers"/>
    <!--month salary-->
    <xsl:call-template name="allTotalStyle">
      <xsl:with-param name="value">
        <xsl:value-of select="sum($allUsers/@c15)"/>
      </xsl:with-param>
    </xsl:call-template>
    <!--hour salary-->
    <xsl:call-template name="allTotalStyle">
      <xsl:with-param name="value">
        <xsl:value-of select="sum($allUsers/@c16)"/>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <xsl:template name="allTotalProject">
    <xsl:param name="allProjects"/>
    <!--Cost-->
    <xsl:call-template name="allTotalStyle">
      <xsl:with-param name="value">
        <xsl:value-of select="sum($allProjects/@c3)"/>
      </xsl:with-param>
    </xsl:call-template>
    <!--Price-->
    <xsl:call-template name="allTotalStyle">
      <xsl:with-param name="value">
        <xsl:value-of select="sum($allProjects/@c4)"/>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <xsl:template name="allTotalSignedTime">
    <xsl:param name="allUsers"/>
    <!--month salary-->
    <xsl:call-template name="allTotalStyle">
      <xsl:with-param name="value">
        <xsl:value-of select="concat(sum($allUsers/@c19),$p10)"/>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <xsl:template name="allTotalTask">
    <xsl:param name="allTasks"/>
    <xsl:param name="allTimeSpents"/>
    <!--time signed-->
    <xsl:variable name="taskTimeSigned">
      <xsl:call-template name="GetTimeFromMunites">
        <xsl:with-param name="totalMinutes" select="sum($allTasks/@c12)"/>
      </xsl:call-template>
    </xsl:variable>
    <!--<xsl:call-template name="allTotalStyle">
      <xsl:with-param name="value">
        <xsl:call-template name="GetTime">
          <xsl:with-param name="time" select="$taskTimeSigned"/>
        </xsl:call-template>
      </xsl:with-param>
    </xsl:call-template>-->
    <!--time spent-->
    <xsl:variable name='taskTimeSpent' select="sum($allTimeSpents/@c9)"/>
    <xsl:call-template name="allTotalStyle">
      <xsl:with-param name="value">
        <xsl:call-template name="GetTime">
          <xsl:with-param name="time" select="$taskTimeSpent"/>
        </xsl:call-template>
      </xsl:with-param>
    </xsl:call-template>
    <!--difference-->
    <xsl:call-template name="allTotalStyle">
      <xsl:with-param name="value">
        <xsl:call-template name="GetTime">
          <xsl:with-param name="time">
            <xsl:call-template name="timeDiff">
              <xsl:with-param name="signedTime" select="$taskTimeSigned"/>
              <xsl:with-param name="spentTime" select="$taskTimeSpent"/>
            </xsl:call-template>
          </xsl:with-param>
        </xsl:call-template>
      </xsl:with-param>
    </xsl:call-template>
    <!--paid-->
    <xsl:call-template name="allTotalStyle">
      <xsl:with-param name="value">
        <xsl:call-template name="moneyFormat">
          <xsl:with-param name="value" select="sum($allTimeSpents/@c17)"/>
        </xsl:call-template>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <xsl:template name="allTotalStyle">
    <xsl:param name='value'/>
    <td class="borderBase" style="width:8%;text-align: left;white-space: nowrap;">
      <div style="width:99%;overflow:hidden;font-size:17px;font-weight: bold;">
        <xsl:value-of select='$value'/>
      </div>
    </td>
  </xsl:template>

  <xsl:template name='timeDiff'>
    <xsl:param name='signedTime'/>
    <xsl:param name='spentTime'/>
    <xsl:value-of select='$spentTime - $signedTime'/>
  </xsl:template>

  <xsl:template name='moneyFormat'>
    <xsl:param name='value'/>
    <xsl:value-of select='format-number($value, "#.00")'/>
  </xsl:template>

</xsl:stylesheet>