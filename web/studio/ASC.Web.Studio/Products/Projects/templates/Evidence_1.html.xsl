﻿<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:output method="html" />

  <xsl:param name="p0"/>
  <xsl:param name="p1"/>
  <xsl:param name="p2"/>
  <xsl:param name="p3"/>
  <xsl:param name="p4"/>
  <xsl:param name="p5"/>
  <xsl:param name="p6"/>
  <xsl:param name="p7"/>
  <xsl:param name="p8"/>
  <xsl:param name="p9"/>
  <xsl:param name="p10"/>
  <xsl:param name="p11"/>
  <xsl:param name="p12"/>
  <xsl:param name="p13"/>
  <xsl:param name="p14"/>
  <xsl:param name="p15"/>

  <xsl:decimal-format NaN = "0.00" />

  <xsl:key name="users" match="r" use="@c0" />
  <xsl:key name="userPrj" match="r" use="concat(@c0, '|', @c1)"/>
  <xsl:key name="userPrjTask" match="r" use="concat(@c0, '|', @c1, '|', @c5)"/>
  <xsl:key name="projects" match="r" use="@c1" />

  <xsl:template match="*">
    <div id="piechartreportBody">
      <xsl:variable name="allUsers" select="r[generate-id(.)=generate-id(key('users', @c0))]"/>
      <xsl:apply-templates select="$allUsers"/>
    </div>
    <script type="text/javascript">
      <xsl:attribute name="src">
        https://www.google.com/jsapi?autoload={'modules':[{'name':'visualization','version':'1.1','packages':['corechart']}]}
      </xsl:attribute>
      &#160;
    </script>
  </xsl:template>

  <xsl:template match="r">
    <xsl:variable name="user" select="@c0"/>
    <!--row representing user-->
    <user>
      <!--display name and link-->
      <xsl:attribute name="link">
        <xsl:value-of select="@c14"/>
        <!--<xsl:value-of select="concat($p13,@c14)"/>-->
      </xsl:attribute>
      <xsl:attribute name="DisplayName">
        <xsl:value-of select="@c13"/>
      </xsl:attribute>
      <!--time spent-->
      <xsl:variable name="timeSpent" select="sum(key('users', @c0)/@c9)"/>
      <xsl:attribute name="timeSpent">
        <xsl:value-of select="$timeSpent"/>
      </xsl:attribute>
      <!--user's project-->
      <xsl:apply-templates mode="proj" select="key('users', $user) [generate-id() = generate-id(key('userPrj', concat($user, '|', @c1)))]">
        <xsl:sort select="@c2" data-type="text"/>
      </xsl:apply-templates>
    </user>
  </xsl:template>

  <xsl:template match="r" mode="proj">
    <xsl:variable name="user" select="@c0"/>
    <xsl:variable name="prj" select="@c1"/>
    <project>
      <!--title and link-->
      <xsl:attribute name="link">
        <xsl:value-of select="concat($p13,'/products/projects/projects.aspx?prjID=',@c1)"/>
      </xsl:attribute>
      <xsl:attribute name="title">
        <xsl:value-of select="@c2"/>
      </xsl:attribute>
      <!--time spent-->
      <xsl:variable name='projectTimeSpent' select="sum(key('userPrj',  concat(@c0, '|', @c1))/@c9)"/>
      <xsl:attribute name="timeSpent">
        <xsl:value-of select="$projectTimeSpent"/>
      </xsl:attribute>
      <!--project's tasks-->
      <xsl:apply-templates mode="task" select="key('userPrj', concat($user, '|', $prj))[generate-id()
        = generate-id(key('userPrjTask', concat($user, '|', $prj, '|', @c5)))]">
      </xsl:apply-templates>
    </project>
  </xsl:template>

  <xsl:template match="r" mode="task">
    <task>
      <!--title and link-->
      <xsl:attribute name="link">
        <xsl:value-of select="concat($p13, '/products/projects/tasks.aspx?prjID=' ,@c1, '&amp;ID=', @c5)"/>
      </xsl:attribute>
      <xsl:attribute name="title">
        <xsl:value-of select="@c6"/>
      </xsl:attribute>
      <!--time spent-->
      <xsl:variable name='taskTimeSpent' select="sum(key('userPrjTask',  concat(@c0, '|', @c1, '|', @c5))/@c9)"/>
      <xsl:attribute name="timeSpent">
        <xsl:value-of select="$taskTimeSpent"/>
      </xsl:attribute>
      &#160;
    </task>
  </xsl:template>
</xsl:stylesheet>