﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ASC.Web.Projects.HoangEvidence.Classes;
using System.Diagnostics;

namespace ASC.Web.Projects.HoangEvidence.EvEntities
{
    public enum SignedTimeType
    {
        onProject,
        onTask
    }
    [DebuggerDisplay("Signed_Time: ID = {ID}, Title = {Title}")]
    public class SignedTime : EVAbstractClass
    {
        public Guid UserID { get; set; }

        public int? TaskID { get; set; }
        public String TaskTitle { get; set; }

        /// <summary>
        /// if TypeSignedTime is "Task" => we need "project", that this "task" belongs to
        /// </summary>
        public int? ProjectWithTaskID { get; set; }
        public String ProjectWithTaskTitle { get; set; }

        /// <summary>
        /// if TypeSignedTime is "Project"
        /// </summary>
        public int? ProjectID { get; set; }
        public String ProjectTitle { get; set; }
        public int TotalMinutes { get; set; }
        public String UserName { get; set; }
        public String CreatedByName { get; set; }

        /// <summary>
        /// if SignedTime is on a "Task" or on a "Project"
        /// </summary>
        public SignedTimeType TypeSignedTime { get; set; }

        public Guid LastModifiedBy { get; set; }
        public string LastModifiedByName { get; set; }
        public DateTime LastModifiedOn { get; set; }


        public override void Delete()
        {
            EvidenceSecurity.DemandDelete(this);
            EVGlobal.GetSignedTimeAction().Delete(this);
        }

        public override void Update()
        {
            EvidenceSecurity.DemandEdit(this);
            EVGlobal.GetSignedTimeAction().Update(this);
        }

        public override void Create()
        {
            EvidenceSecurity.DemandCreate();
            EVGlobal.GetSignedTimeAction().Create(this);
        }
    }
}