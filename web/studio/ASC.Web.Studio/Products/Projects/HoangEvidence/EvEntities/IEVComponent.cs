﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASC.Web.Projects.HoangEvidence.EvEntities
{
    public interface IEVComponent
    {
        void Delete();
        void Update();
    }
}
