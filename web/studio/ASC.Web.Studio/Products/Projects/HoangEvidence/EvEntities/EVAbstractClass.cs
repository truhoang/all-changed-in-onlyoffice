﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Reflection;

namespace ASC.Web.Projects.HoangEvidence.EvEntities
{
    public abstract class EVAbstractClass : IEVComponent
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime DateTimeCreated { get; set; }

        abstract public void Delete();
        abstract public void Update();
        abstract public void Create();

        public bool PropertiesEqual(object comparisonObject)
        {

            Type sourceType = this.GetType();
            Type destinationType = comparisonObject.GetType();

            if (sourceType == destinationType)
            {
                PropertyInfo[] sourceProperties = sourceType.GetProperties();
                foreach (PropertyInfo pi in sourceProperties)
                {
                    if ((sourceType.GetProperty(pi.Name).GetValue(this, null) == null && destinationType.GetProperty(pi.Name).GetValue(comparisonObject, null) == null))
                    {
                        // if both are null, don't try to compare  (throws exception)
                    }
                    else if (!(sourceType.GetProperty(pi.Name).GetValue(this, null).ToString() == destinationType.GetProperty(pi.Name).GetValue(comparisonObject, null).ToString()))
                    {
                        // only need one property to be different to fail Equals.
                        return false;
                    }
                }
            }
            else
            {
                throw new ArgumentException("Comparison object must be of the same type.", "comparisonObject");
            }

            return true;
        }
    }
}