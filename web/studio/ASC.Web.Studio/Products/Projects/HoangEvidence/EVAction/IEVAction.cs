﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ASC.Web.Projects.HoangEvidence.EvEntities;

namespace ASC.Web.Projects.HoangEvidence.EVAction
{
    public interface IEVAction
    {
        void Delete(IEVComponent component);
        IEVComponent Create(IEVComponent component);
        IEVComponent Update(IEVComponent component);
    }
}
