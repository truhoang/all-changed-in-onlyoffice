﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ASC.Common.Data.Sql;
using ASC.Core;
using ASC.Web.Projects.HoangEvidence.EVFilter;
using ASC.Web.Projects.HoangEvidence.EVFilter.OrderByFilter;


namespace ASC.Web.Projects.HoangEvidence.EVAction
{
    public class EVDataBaseThings
    {
        protected static readonly string CommentsTable = "projects_comments";
        protected static readonly string FollowingProjectTable = "projects_following_project_participant";
        protected static readonly string ProjectsTable = "projects_projects";
        protected static readonly string ParticipantTable = "projects_project_participant";
        protected static readonly string ProjectTagTable = "projects_project_tag";
        protected static readonly string ReportTable = "projects_report_template";
        protected static readonly string SubtasksTable = "projects_subtasks";
        protected static readonly string TasksTable = "projects_tasks";
        protected static readonly string TasksResponsibleTable = "projects_tasks_responsible";
        protected static readonly string TasksOrderTable = "projects_tasks_order";
        protected static readonly string EvidenceTable = "ev_evidence";
        protected static readonly string SignedTimeTable = "projects_signed_times";
        protected static readonly string FilterTable = "ev_filtr";
        protected static readonly string FilterUserTable = "ev_filtr_user";
        protected static readonly string DochazkaTable = "ev_dochazka";
        protected static readonly string UserTable = "core_user";

        protected int Tenant { get; private set; }

        protected Guid CurrentUserID { get; private set; }

        protected string DatabaseId { get; private set; }

        protected EVDataBaseThings(string dbId, int tenant)
        {
            Tenant = tenant;
            CurrentUserID = SecurityContext.CurrentAccount.ID;
            DatabaseId = dbId;
        }


        protected SqlQuery Query(string table)
        {
            return new SqlQuery(table).Where("tenant_id", Tenant);
        }

        protected SqlInsert Insert(string table)
        {
            return new SqlInsert(table, true).InColumnValue("tenant_id", Tenant);
        }

        protected SqlUpdate Update(string table)
        {
            return new SqlUpdate(table).Where("tenant_id", Tenant);
        }

        protected SqlDelete Delete(string table)
        {
            return new SqlDelete(table).Where("tenant_id", Tenant);
        }

        protected static Guid ToGuid(object guid)
        {
            try
            {
                var str = guid as string;
                return !string.IsNullOrEmpty(str) ? new Guid(str) : Guid.Empty;
            }
            catch (Exception)
            {
                return Guid.Empty;
            }

        }


        protected void getQueryWithFilter(BaseAbstractFilter filter, SqlQuery query)
        {
            if (filter == null)
            {
                return;
            }
            string condition = filter.SetCondition();
            if (condition != "")
            {
                query.Where(condition);
            }
        }

        protected void getQueryWithOrderBy(OrderByItemCollection orderByFilter, SqlQuery query)
        {
            if (orderByFilter == null)
            {
                return;
            }
            orderByFilter.SetOrderItemToQuery(query);
        }
    }
}