﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

using ASC.Common.Data;
using ASC.Common.Data.Sql;
using ASC.Common.Data.Sql.Expressions;
using ASC.Web.Projects.HoangEvidence.EvEntities;
using ASC.Web.Projects.HoangEvidence.EVFilter;
using ASC.Web.Projects.HoangEvidence.EVFilter.OrderByFilter;
using ASC.Projects.Core.Domain;
using ASC.Core.Tenants;
using ASC.Core;

namespace ASC.Web.Projects.HoangEvidence.EVAction
{
    public class SignedTimeAction : EVDataBaseThings, IEVAction
    {
        public SignedTimeAction(string dbID, int tenant)
            : base(dbID, tenant)
        {

        }

        #region IEVAction

        public void Delete(IEVComponent component)
        {
            SignedTime signedTime = (SignedTime)component;
            Delete(signedTime.ID);
        }

        public IEVComponent Create(IEVComponent component)
        {
            SignedTime signedTime = (SignedTime)component;
            using (var db = new DbManager(DatabaseId))
            {
                using (var tr = db.BeginTransaction(IsolationLevel.ReadUncommitted))
                {
                    var insert = Insert(SignedTimeTable)
                        .InColumnValue("id", signedTime.ID)
                        .InColumnValue("title", signedTime.Title)
                        .InColumnValue("user_id", signedTime.UserID.ToString())
                        .InColumnValue("task_id", signedTime.TaskID)
                        .InColumnValue("project_id", signedTime.ProjectID)
                        .InColumnValue("total_minutes", signedTime.TotalMinutes)
                        .InColumnValue("created_by", SecurityContext.CurrentAccount.ID.ToString())
                        .InColumnValue("datetime_created", DateTime.Now)
                        .InColumnValue("description", signedTime.Description)
                        .InColumnValue("type_signed_time", signedTime.TypeSignedTime.ToString())
                        .InColumnValue("last_modified_by", SecurityContext.CurrentAccount.ID.ToString())
                        .InColumnValue("last_modified_on", TenantUtil.DateTimeToUtc(DateTime.Now))
                        .Identity(1, 0, true);

                    signedTime.ID = db.ExecuteScalar<int>(insert);
                    tr.Commit();
                }
            }
            return GetByID(signedTime.ID);
        }

        public IEVComponent Update(IEVComponent component)
        {
            SignedTime signedTime = (SignedTime)component;
            using (var db = new DbManager(DatabaseId))
            {
                using (var tr = db.BeginTransaction(IsolationLevel.ReadUncommitted))
                {
                    db.ExecuteNonQuery(Update(SignedTimeTable)
                        .Set("title", signedTime.Title)
                        .Set("user_id", signedTime.UserID.ToString())
                        .Set("task_id", signedTime.TaskID)
                        .Set("project_id", signedTime.ProjectID)
                        .Set("total_minutes", signedTime.TotalMinutes)
                        .Set("tenant_id", Tenant)
                        .Set("description", signedTime.Description)
                        .Set("type_signed_time", signedTime.TypeSignedTime.ToString())
                        .Set("last_modified_by", SecurityContext.CurrentAccount.ID.ToString())
                        .Set("last_modified_on", TenantUtil.DateTimeToUtc(DateTime.Now))
                        .Where("id", signedTime.ID));
                    tr.Commit();
                }
            }
            return GetByID(signedTime.ID);
        }
        #endregion

        public void Delete(int id)
        {
            using (var db = new DbManager(DatabaseId))
            {
                using (var tx = db.BeginTransaction())
                {
                    db.ExecuteNonQuery(Delete(SignedTimeTable).Where("id", id));
                    tx.Commit();
                }
            }
        }

        public IEnumerable<SignedTime> GetByUserID(Guid userID)
        {
            string IDString = userID.ToString();
            using (var db = new DbManager(DatabaseId))
            {
                var query = CreateQuery().Where("uv.user_id", IDString);
                return db.ExecuteList(query).ConvertAll(ToSignedTime);
            }
        }

        public SignedTime GetByUserDomainObjectPair(Guid userID, string projectID = null, string taskID = null)
        {
            if (projectID == null && taskID == null) return null;
            string columnName = projectID != null ? "uv.project_id" : "uv.task_id";
            string queryInput = projectID ?? taskID;
            string IDString = userID.ToString();
            SignedTimeType typeSignedTime = projectID != null ? SignedTimeType.onProject : SignedTimeType.onTask;
            var typeSignedTimeCondition = Exp.EqColumns("uv.type_signed_time", "'" + typeSignedTime.ToString() + "'");
            using (var db = new DbManager(DatabaseId))
            {
                var query = CreateQuery().Where(Exp.And(Exp.And(Exp.EqColumns("uv.user_id", "'" + IDString + "'"), Exp.EqColumns(columnName, "'" + queryInput + "'")), typeSignedTimeCondition));
                return db.ExecuteList(query).ConvertAll(ToSignedTime).SingleOrDefault();
            }
        }

        public SignedTime GetByID(int id)
        {
            using (var db = new DbManager(DatabaseId))
            {
                var query = CreateQuery().Where("uv.id", id);
                return db.ExecuteList(query).ConvertAll(ToSignedTime).FirstOrDefault();
            }
        }

        public IEnumerable<SignedTime> GetAll()
        {
            using (var db = new DbManager(DatabaseId))
            {
                var query = CreateQuery();
                return db.ExecuteList(query).ConvertAll(ToSignedTime);
            }
        }

        public IEnumerable<SignedTime> GetSignedTimeWithFilter(BaseAbstractFilter filter = null, OrderByItemCollection orderByFilters = null)
        {
            using (var db = new DbManager(DatabaseId))
            {
                var query = CreateQuery();
                getQueryWithFilter(filter, query);
                getQueryWithOrderBy(orderByFilters, query);
                return db.ExecuteList(query).ConvertAll(ToSignedTime);
            }
        }

        private SqlQuery CreateQuery()
        {
            var query = new SqlQuery(SignedTimeTable + " uv")
                .InnerJoin(UserTable + " us", Exp.EqColumns("uv.user_id", "us.id"))
                .LeftOuterJoin(TasksTable + " t", Exp.EqColumns("uv.task_id", "t.id"))
                .LeftOuterJoin(ProjectsTable + " pt", Exp.EqColumns("pt.id", "t.project_id"))
                .LeftOuterJoin(ProjectsTable + " p", Exp.EqColumns("uv.project_id", "p.id"))
                .InnerJoin(UserTable + " m", Exp.EqColumns("m.id", "uv.created_by"))
                .InnerJoin(UserTable + " m2", Exp.EqColumns("m2.id", "uv.last_modified_by"))
                .Select("uv.id", "uv.title", "uv.user_id", "uv.created_by", "uv.task_id", "uv.project_id", "uv.datetime_created", "uv.description", "uv.type_signed_time")
                .Select("us.username")
                .Select("t.title")
                .Select("p.title")
                .Select("m.username")
            .Select("pt.id", "pt.title")
            .Select("uv.total_minutes")
            .Select("uv.last_modified_by")
            .Select("uv.last_modified_on")
            .Select("m2.username");
            return query;
        }

        private SignedTime ToSignedTime(object[] r)
        {
            var signedTime = new SignedTime()
            {
                ID = (int)r[0],
                Title = (string)r[1],
                UserID = ToGuid(r[2]),
                CreatedBy = ToGuid(r[3]),
                TaskID = (int?)r[4],
                ProjectID = (int?)r[5],
                DateTimeCreated = (DateTime)r[6],
                Description = (string)r[7],
                TypeSignedTime = (SignedTimeType)Enum.Parse(typeof(SignedTimeType), (string)r[8]),
                UserName = (string)r[9],
                TaskTitle = (string)r[10],
                ProjectTitle = (string)r[11],
                CreatedByName = (string)r[12],
                ProjectWithTaskID = (int?)r[13],
                ProjectWithTaskTitle = (string)r[14],
                TotalMinutes = (int)r[15],
                LastModifiedBy = ToGuid(r[16]),
                LastModifiedOn = r[17] != null ? TenantUtil.DateTimeFromUtc(Convert.ToDateTime(r[17])) : default(DateTime),
                LastModifiedByName = r[18].ToString()
            };
            return signedTime;
        }

    }
}