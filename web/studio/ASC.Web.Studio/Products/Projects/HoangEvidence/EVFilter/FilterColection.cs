﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ASC.Core.Users;

namespace ASC.Web.Projects.HoangEvidence.EVFilter
{
    public class FilterColection : BaseAbstractFilter
    {
        List<BaseAbstractFilter> filters = new List<BaseAbstractFilter>();

        public FilterColection(object pattern)
            : base(pattern)
        {

        }
        public FilterColection()
            : base()
        {

        }
        public override string SetCondition()
        {
            StringBuilder stb = new StringBuilder("");
            foreach (var item in filters)
            {
                stb.Append(item.SetCondition());
                if (item != filters.Last())
                {
                    stb.Append(" and ");
                }
            }
            return stb.ToString();
        }

        public void AddFilter(BaseAbstractFilter filter, Guid user)
        {
            if (canAddFilter(filter, user))
            {
                filters.Add(filter);
            }
        }
        private bool canAddFilter(BaseAbstractFilter filter, Guid user)
        {
            return true;
        }
    }
}