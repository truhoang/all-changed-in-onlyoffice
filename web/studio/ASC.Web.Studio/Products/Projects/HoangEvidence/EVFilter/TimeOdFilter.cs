﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASC.Web.Projects.HoangEvidence.EVFilter
{
    public class TimeOdFilter : BaseAbstractFilter
    {
        public TimeOdFilter(object pattern)
            : base(pattern) { }
        public override string SetCondition()
        {
            try
            {
                DateTime dateTime = Convert.ToDateTime(pattern);
                string time_started = dateTime.ToString("yyyy-MM-dd HH:mm:ss");

                string condition = tableAlias + ".time_started > " + quoted(time_started);
                return condition;
            }
            catch (Exception)
            {
                throw new Exception("can not convert to DateTime format");
            }
        }
    }
}