﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASC.Web.Projects.HoangEvidence.EVFilter
{
    public class ProjectFilter : BaseAbstractFilter
    {
        public ProjectFilter(object pattern)
            : base(pattern)
        {
            tableAlias = "p";
        }
        public override string SetCondition()
        {
            string projectID = pattern.ToString();
            string condition = tableAlias + ".id = " + quoted(projectID);
            return condition;
        }
    }
}