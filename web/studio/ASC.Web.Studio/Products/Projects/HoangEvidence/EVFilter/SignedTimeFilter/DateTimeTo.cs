﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASC.Web.Projects.HoangEvidence.EVFilter.SignedTimeFilter
{
    public class DateTimeTo : BaseAbstractFilter
    {

        public DateTimeTo(object pattern)
            : base(pattern)
        {
            tableAlias = "uv";
        }
        public override string SetCondition()
        {
            try
            {
                DateTime dateTime = Convert.ToDateTime(pattern).AddDays(1);
                string DateTimeCreated = dateTime.ToString("yyyy-MM-dd HH:mm:ss");
                string condition = tableAlias + ".datetime_created< " + quoted(DateTimeCreated);
                return condition;
            }
            catch (Exception)
            {
                throw new Exception("can not convert to DateTime format");
            }
        }
    }
}