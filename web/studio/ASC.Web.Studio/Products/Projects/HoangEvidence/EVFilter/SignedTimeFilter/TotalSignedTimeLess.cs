﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASC.Web.Projects.HoangEvidence.EVFilter.SignedTimeFilter
{
    public class TotalSignedTimeLess : BaseAbstractFilter
    {
        public TotalSignedTimeLess(object pattern)
            : base(pattern)
        {
            tableAlias = "uv";
        }
        public override string SetCondition()
        {
            try
            {
                int totalSignedTime = Convert.ToInt32(pattern);
                string condition = tableAlias + " .total_minutes<" + totalSignedTime;
                return condition;
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}