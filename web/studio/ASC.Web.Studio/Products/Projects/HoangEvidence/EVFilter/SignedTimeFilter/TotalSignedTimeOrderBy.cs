﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ASC.Web.Projects.HoangEvidence.EVFilter.OrderByFilter;

namespace ASC.Web.Projects.HoangEvidence.EVFilter.SignedTimeFilter
{
    public class TotalSignedTimeOrderBy : OrderByAbstractBase
    {
        public TotalSignedTimeOrderBy(bool isAsc)
            : base(isAsc)
        {
            alias = "uv";
            item.ColumnName = alias + ".total_minutes";
        }
    }
}