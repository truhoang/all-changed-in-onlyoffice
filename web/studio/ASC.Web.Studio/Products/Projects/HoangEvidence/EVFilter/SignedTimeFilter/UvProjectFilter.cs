﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASC.Web.Projects.HoangEvidence.EVFilter.SignedTimeFilter
{
    public class UvProjectFilter : BaseAbstractFilter
    {
        public UvProjectFilter(object pattern)
            : base(pattern)
        {
            tableAlias = "uv";
        }
        public override string SetCondition()
        {
            int projectID = Convert.ToInt32(pattern);
            string condition = "(" + tableAlias + " .project_id =" + projectID + " or pt.id=" + projectID + ")";
            return condition;
        }
    }
}