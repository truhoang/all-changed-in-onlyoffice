﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASC.Web.Projects.HoangEvidence.EVFilter
{
    public class UserFilter : BaseAbstractFilter
    {
        public UserFilter(object pattern)
            : base(pattern)
        {

        }
        public override string SetCondition()
        {
            string userID = pattern.ToString();
            string condition = tableAlias + ".user_id = " + quoted(userID);
            return condition;
        }
    }
}