﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASC.Web.Projects.HoangEvidence.EVFilter.OrderByFilter
{
    public class DurationOrderBy : OrderByAbstractBase
    {
        public DurationOrderBy(bool isAsc)
            : base(isAsc)
        {
            item.ColumnName = "TIMESTAMPDIFF(second, " + alias + ".time_started," + alias + " .time_finished)";
        }
    }
}