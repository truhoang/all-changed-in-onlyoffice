﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASC.Web.Projects.HoangEvidence.EVFilter.OrderByFilter
{
    public class IDOrderBy : OrderByAbstractBase
    {
        public IDOrderBy(bool isAsc)
            : base(isAsc)
        {
            item.ColumnName = alias + ".id";
        }
    }
}