﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASC.Web.Projects.HoangEvidence.EVFilter.OrderByFilter
{
    public class ProjectTitleOrderBy : OrderByAbstractBase
    {
        public ProjectTitleOrderBy(bool isAsc)
            : base(isAsc)
        {
            alias = "p";
            item.ColumnName = alias + ".title";
        }
    }
}