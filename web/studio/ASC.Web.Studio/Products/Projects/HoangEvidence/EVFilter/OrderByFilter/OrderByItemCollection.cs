﻿using System;
using System.Collections.Generic;
using ASC.Common.Data;
using ASC.Common.Data.Sql;
using ASC.Common.Data.Sql.Expressions;

namespace ASC.Web.Projects.HoangEvidence.EVFilter.OrderByFilter
{
    public class OrderByItemCollection : OrderByAbstractBase
    {
        List<OrderByAbstractBase> items = new List<OrderByAbstractBase>();
        public OrderByItemCollection(bool isAsc)
            : base(isAsc)
        {
        }
        public void SetOrderItemToQuery(SqlQuery query)
        {
            foreach (var item in items)
            {
                var OrderByColumnItem = item.GetOrderItem();
                query.OrderBy(OrderByColumnItem.ColumnName, OrderByColumnItem.IsAsc);
            }
        }

        public void AddOrderByItem(OrderByAbstractBase item)
        {
            //tady bude podminka pro pridani
            items.Add(item);
        }
    }
}