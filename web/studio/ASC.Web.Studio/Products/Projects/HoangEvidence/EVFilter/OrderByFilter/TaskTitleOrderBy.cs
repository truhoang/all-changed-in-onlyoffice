﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASC.Web.Projects.HoangEvidence.EVFilter.OrderByFilter
{
    public class TaskTitleOrderBy : OrderByAbstractBase
    {
        public TaskTitleOrderBy(bool isAsc)
            : base(isAsc)
        {
            alias = "t";
            item.ColumnName = alias + ".title";
        }
    }
}