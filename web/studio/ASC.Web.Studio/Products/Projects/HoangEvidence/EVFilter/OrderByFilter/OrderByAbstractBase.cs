﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASC.Web.Projects.HoangEvidence.EVFilter.OrderByFilter
{
    public class OrderByColumItem
    {
        public string ColumnName { get; set; }
        public bool IsAsc { get; set; }
    }
    public abstract class OrderByAbstractBase
    {
        protected string alias = "e";
        protected OrderByColumItem item = new OrderByColumItem();
        public OrderByAbstractBase(bool isAsc)
        {
            item.IsAsc = isAsc;
        }

        public OrderByAbstractBase(bool isAsc, string alias)
        {
            this.alias = alias;
            item.IsAsc = isAsc;
        }

        public OrderByAbstractBase(bool isAsc, string alias, string columnName)
        {

            this.alias = alias;
            item.ColumnName = alias + "." + columnName;
            item.IsAsc = isAsc;
        }

        public string SetAlias
        {
            set { alias = value; }
        }

        public OrderByColumItem GetOrderItem()
        {
            return item;
        }
    }
}