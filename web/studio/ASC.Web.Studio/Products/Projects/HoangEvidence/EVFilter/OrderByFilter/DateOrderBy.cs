﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASC.Web.Projects.HoangEvidence.EVFilter.OrderByFilter
{
    public class DateOrderBy : OrderByAbstractBase
    {
        public DateOrderBy(bool isAsc)
            : base(isAsc)
        {
            item.ColumnName = alias + ".time_started";
        }

        public DateOrderBy(bool isAsc, string alias, string columnName)
            : base(isAsc, alias, columnName)
        {

        }
    }
}