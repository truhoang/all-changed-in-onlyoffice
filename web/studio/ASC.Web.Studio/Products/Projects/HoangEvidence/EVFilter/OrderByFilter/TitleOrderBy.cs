﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASC.Web.Projects.HoangEvidence.EVFilter.OrderByFilter
{
    public class TitleOrderBy : OrderByAbstractBase
    {
        public TitleOrderBy(bool isAsc)
            : base(isAsc)
        {
            item.ColumnName = alias + ".title";
        }

        public TitleOrderBy(bool isAsc, string alias)
            : base(isAsc, alias)
        {
            item.ColumnName = alias + ".title";
        }
    }
}