﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASC.Web.Projects.HoangEvidence.EVFilter
{
    public class DurationMoreThanFilter : BaseAbstractFilter
    {
        public DurationMoreThanFilter(object pattern)
            : base(pattern)
        {

        }
        public override string SetCondition()
        {
            try
            {
                int seconds = Convert.ToInt32(pattern) * 60;
                string condition = seconds + " <= " + "(SELECT TIMESTAMPDIFF(second, " + tableAlias + ".time_started, " + tableAlias + ".time_finished))";
                return condition;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}