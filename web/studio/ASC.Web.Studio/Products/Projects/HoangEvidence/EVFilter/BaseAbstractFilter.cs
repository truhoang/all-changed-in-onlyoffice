﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASC.Web.Projects.HoangEvidence.EVFilter
{
    public abstract class BaseAbstractFilter
    {
        protected object pattern;
        protected string tableAlias = "e";
        public BaseAbstractFilter(object pattern)
        {
            this.pattern = pattern;
        }

        protected BaseAbstractFilter()
        {

        }
        public string TableAlias
        {
            set { tableAlias = value; }
        }

        protected string quoted(string st)
        {
            return "'" + st + "'";
        }

        public abstract string SetCondition();
    }
}