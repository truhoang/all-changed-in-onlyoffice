﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASC.Web.Projects.HoangEvidence.EVFilter
{
    public class SearchFilter : BaseAbstractFilter
    {
        public SearchFilter(object pattern)
            : base(pattern)
        {

        }
        public override string SetCondition()
        {
            string patternSt = (string)pattern;
            string condition = "(" + tableAlias + ".title like'%" + patternSt + "%' or " + tableAlias + ".description like '%" + patternSt + "%')";
            return condition;
        }
    }
}