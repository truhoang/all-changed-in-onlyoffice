﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASC.Web.Projects.HoangEvidence.EVFilter
{
    public class TimeDoFilter : BaseAbstractFilter
    {

        public TimeDoFilter(object pattern)
            : base(pattern)
        {

        }
        public override string SetCondition()
        {
            try
            {
                DateTime dateTime = Convert.ToDateTime(pattern).AddDays(1);
                string do_kdy = dateTime.ToString("yyyy-MM-dd HH:mm:ss");
                string condition = tableAlias + ".time_started < " + quoted(do_kdy);
                return condition;
            }
            catch (Exception)
            {
                throw new Exception("can not convert to DateTime format");
            }
        }
    }
}