﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ASC.Web.Projects.HoangEvidence.EVAction;

namespace ASC.Web.Projects.HoangEvidence.Factory
{
    public class SignedTimeActionSingletonCreator : IEVActionCreator
    {
        static SignedTimeAction signedTimeAction;
        public EVAction.IEVAction FactoryMethod(string dbId, int tenant)
        {
            if (signedTimeAction == null)
            {
                signedTimeAction = new SignedTimeAction(dbId, tenant);
            }
            return signedTimeAction;
        }
    }
}