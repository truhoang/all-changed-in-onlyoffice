﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ASC.Web.Projects.HoangEvidence.EvEntities;

namespace ASC.Web.Projects.HoangEvidence.Factory
{
    public class SignedTimeCreator : IEVCreator
    {
        public IEVComponent FactoryMethod(Guid user)
        {
            if (canCreate(user))
            {
                return new SignedTime();
            }
            return null;
        }
        private bool canCreate(Guid user)
        {
            return true;
        }
    }
}