﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ASC.Web.Projects.HoangEvidence.EVAction;

namespace ASC.Web.Projects.HoangEvidence.Factory
{
    public interface IEVActionCreator
    {
        IEVAction FactoryMethod(string dbId, int tenant);
    }
}
