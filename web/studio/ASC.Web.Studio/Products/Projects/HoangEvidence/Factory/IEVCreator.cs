﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ASC.Web.Projects.HoangEvidence.EvEntities;

namespace ASC.Web.Projects.HoangEvidence.Factory
{
    interface IEVCreator
    {
        IEVComponent FactoryMethod(Guid user);
    }
}
