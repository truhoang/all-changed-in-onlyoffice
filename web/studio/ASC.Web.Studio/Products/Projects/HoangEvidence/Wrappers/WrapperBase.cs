﻿using System;
using System.Runtime.Serialization;
using ASC.Api.Employee;

namespace ASC.Web.Projects.HoangEvidence.Wrappers
{
    [DataContract(Namespace = "")]
    public class WrapperBase
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string Description { get; set; }

        //ID of the creator
        [DataMember]
        public string CreatedByID { get; set; }


        //name of the creator
        [DataMember]
        public string CreatedByName { get; set; }

    }
}