﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ASC.Api.Employee;

using System.Runtime.Serialization;
using ASC.Web.Projects.HoangEvidence.EvEntities;
using ASC.Web.Projects.HoangEvidence.Classes;

namespace ASC.Web.Projects.HoangEvidence.Wrappers
{
    [DataContract(Name = "signed_time", Namespace = "")]
    public class SignedTimeWrapper : WrapperBase
    {
        [DataMember]
        public string UserID { get; set; }

        [DataMember]
        public string UserName { get; set; }

        [DataMember]
        public int? TaskID { get; set; }

        [DataMember]
        public string TaskTitle { get; set; }

        [DataMember]
        public int? ProjectWithTaskID { get; set; }

        [DataMember]
        public string ProjectWithTaskTitle { get; set; }

        [DataMember]
        public int TotalMinutes { get; set; }

        [DataMember]
        public int? ProjectID { get; set; }

        [DataMember]
        public string ProjectTitle { get; set; }

        [DataMember]
        public string TypeSignedTime { get; set; }

        [DataMember]
        public bool CanEdit { get; set; }

        [DataMember]
        public bool CanDelete { get; set; }

        [DataMember]
        public bool CanRead { get; set; }

        [DataMember]
        public DateTime DateTimeCreated { get; set; }

        public SignedTimeWrapper(SignedTime st)
        {
            ID = st.ID;
            UserID = st.UserID.ToString();
            UserName = st.UserName.ToString();
            Title = st.Title;
            Description = st.Description;
            CreatedByID = st.CreatedBy.ToString();
            CreatedByName = st.CreatedByName;
            ProjectID = st.ProjectID;
            ProjectTitle = st.ProjectTitle;
            TaskID = st.TaskID;
            TaskTitle = st.TaskTitle;
            DateTimeCreated = st.DateTimeCreated;
            ProjectWithTaskID = st.ProjectWithTaskID;
            ProjectWithTaskTitle = st.ProjectWithTaskTitle;
            TypeSignedTime = st.TypeSignedTime.ToString();
            TotalMinutes = st.TotalMinutes;
            CanEdit = EvidenceSecurity.CanEdit(st);
            CanDelete = EvidenceSecurity.CanDelete(st);
            CanRead = EvidenceSecurity.CanRead(st);
        }
    }
}