﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ASC.Web.Projects.HoangEvidence.Factory;
using ASC.Web.Projects.HoangEvidence.EVAction;
using ASC.Web.Projects.HoangEvidence.EvEntities;

using ASC.Projects.Engine;
using ASC.Web.Studio.Utility;
using ASC.Core;


namespace ASC.Web.Projects.HoangEvidence.Classes
{
    public class EVGlobal
    {
        public static readonly string DbID = "projects";
        public static readonly int MaxRecordOnAPage = 15;
        private static Guid CurrentUserID = SecurityContext.CurrentAccount.ID;
        static EngineFactory EngineFactory;

        public static IEVAction GetSignedTimeAction()
        {
            return new SignedTimeActionSingletonCreator().FactoryMethod(DbID, TenantProvider.CurrentTenantID);
        }

        public static EngineFactory GetEngineFactory()
        {
            if (EngineFactory == null)
            {
                EngineFactory = new EngineFactory(DbID, TenantProvider.CurrentTenantID);
            }
            return EngineFactory;
        }
    }
}