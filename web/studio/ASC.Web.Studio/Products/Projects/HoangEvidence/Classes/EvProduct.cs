﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ASC.Web.Core;
using ASC.Web.Core.Utility;

namespace Hoang.Web.EvidenceVykazu
{
    public class EvProduct : Product
    {
        internal const string ProductPath = "~/products/Hoang.Web.EvidenceVykazu/";
        private ProductContext _context;
        public override ProductContext Context
        {
            get { return this._context; }
        }

        public override string Description
        {
            get { return "evidence vykazu description"; }
        }

        public override void Init()
        {
            _context = new ProductContext
            {
                MasterPageFile = "~/products/Hoang.Web.EvidenceVykazu/Masters/NestedMasterPage1.master",
                DisabledIconFileName = "product_disabled_logo.png",
                IconFileName = "product_logo.png",
                LargeIconFileName = "product_logolarge.png",
                DefaultSortOrder = 50,
            };
        }

        public override string Name
        {
            get { return "evidence of work activities"; }
        }

        public override string ProductClassName
        {
            get { return "Evidence Vykazu Class Name"; }
        }

        public override Guid ProductID
        {
            get
            {
                return new Guid("{F4D98AFD-D336-4332-8778-3C6945C81EAE}");
            }
        }

        public override string StartURL
        {
            get { return ProductPath; }
        }
    }
}