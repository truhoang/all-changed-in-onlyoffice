﻿using System;
using System.Collections.Generic;
using System.Linq;
using ASC.Api.Interfaces;
using System.Web;
using ASC.Api.Attributes;
using ASC.Web.Projects.HoangEvidence.Wrappers;
using ASC.Web.Projects.HoangEvidence.Factory;
using ASC.Web.Projects.HoangEvidence.EVAction;
using ASC.Web.Projects.HoangEvidence.EvEntities;
using ASC.Api.Collections;
using ASC.MessagingSystem;
using ASC.Core;
using ASC.Api.Impl;
using ASC.Core.Users;
using ASC.Web.Projects.HoangEvidence.EVFilter;
using ASC.Web.Projects.HoangEvidence.EVFilter.OrderByFilter;
using ASC.Web.Projects.HoangEvidence.EVFilter.SignedTimeFilter;
using ASC.Api.Utils;
using ASC.Core.Users;
using ASC.Api.Exceptions;

namespace ASC.Web.Projects.HoangEvidence.Classes
{
    public partial class APIEvidence : IApiEntryPoint
    {
        private static Guid currentUserID = SecurityContext.CurrentAccount.ID;
        protected ApiContext _context;
        public string Name
        {
            get { return "evidence"; }
        }

        private static HttpRequest Request
        {
            get { return HttpContext.Current.Request; }
        }

        public APIEvidence(ApiContext context)
        {
            this._context = context;
        }

        #region additional Methods

        OrderByItemCollection getEVOrderBy()
        {

            OrderByItemCollection orderByCollection = new OrderByItemCollection(!_context.SortDescending);
            switch (_context.SortBy)
            {
                case "title": orderByCollection.AddOrderByItem(new TitleOrderBy(!_context.SortDescending)); break;
                case "taskTitle": orderByCollection.AddOrderByItem(new TaskTitleOrderBy(!_context.SortDescending)); break;
                case "projectTitle": orderByCollection.AddOrderByItem(new ProjectTitleOrderBy(!_context.SortDescending)); break;
                case "date": orderByCollection.AddOrderByItem(new DateOrderBy(!_context.SortDescending)); break;
                case "duration": orderByCollection.AddOrderByItem(new DurationOrderBy(!_context.SortDescending)); break;
                default:
                    break;
            }

            return orderByCollection;
        }
        bool isCurrentUserAdmin()
        {
            return CoreContext.UserManager.GetUsers(SecurityContext.CurrentAccount.ID).IsAdmin();

        }

        bool isCurrentUserOwner(string evidenceOwner)
        {
            return SecurityContext.CurrentAccount.ID.ToString() == evidenceOwner;
        }
        #endregion


        #region SignedTime

        #region read

        [Read(@"signed_time")]
        public IEnumerable<SignedTimeWrapper> GetSignedtimes()
        {
            var result = ((SignedTimeAction)EVGlobal.GetSignedTimeAction()).GetAll().Where(st => EvidenceSecurity.CanRead(st))
                .Select(st => new SignedTimeWrapper(st)).ToSmartList();
            return result;
        }

        [Read(@"signed_time/@self")]
        public IEnumerable<SignedTimeWrapper> GetMineSignedtimes()
        {
            var result = ((SignedTimeAction)EVGlobal.GetSignedTimeAction()).GetByUserID(currentUserID).Where(st => EvidenceSecurity.CanRead(st))
                .Select(st => new SignedTimeWrapper(st)).ToSmartList();
            return result;
        }

        [Read(@"signed_time/{id:[0-9]+}")]
        public SignedTimeWrapper GetSignedTimeByID(int id)
        {
            var signedTime = ((SignedTimeAction)EVGlobal.GetSignedTimeAction()).GetByID(id).NotFoundIfNull();
            var signedTimeWrapper = new SignedTimeWrapper(signedTime);
            return signedTimeWrapper;
        }

        [Read(@"signed_time/{username}")]
        public IEnumerable<SignedTimeWrapper> GetByUserName(string username)
        {
            if (CoreContext.Configuration.Personal) throw new MethodAccessException("Method not available on personal.onlyoffice.com");
            var user = CoreContext.UserManager.GetUserByUserName(username);
            if (user.ID == ASC.Core.Users.Constants.LostUser.ID)
            {
                user = CoreContext.UserManager.GetUsers(new Guid(username));
            }

            if (user.ID == ASC.Core.Users.Constants.LostUser.ID)
            {
                throw new ItemNotFoundException("User not found");
            }

            var result = ((SignedTimeAction)EVGlobal.GetSignedTimeAction()).GetByUserID(user.ID).Where(st => EvidenceSecurity.CanRead(st))
                   .Select(st => new SignedTimeWrapper(st)).ToSmartList();
            return result;
        }

        [Read(@"signed_time/project/{id:[0-9]+}")]
        public IEnumerable<SignedTimeWrapper> GetSignedTimeByProjID(int id)
        {
            FilterColection filters = new FilterColection();
            filters.AddFilter(new UvProjectFilter(id), currentUserID);
            var result = ((SignedTimeAction)EVGlobal.GetSignedTimeAction()).GetSignedTimeWithFilter(filter: filters).Where(x => EvidenceSecurity.CanRead(x)).Select(x => new SignedTimeWrapper(x)).ToSmartList();
            return result;
        }

        [Read(@"signed_time/@search/{query}")]
        public IEnumerable<SignedTimeWrapper> GetSignedTimeBySearching(string query)
        {
            FilterColection filters = new FilterColection();
            if (query != null)
            {
                var searchFilter = new SearchFilter(query);
                searchFilter.TableAlias = "uv";
                filters.AddFilter(searchFilter, currentUserID);
            }

            var result = ((SignedTimeAction)EVGlobal.GetSignedTimeAction()).GetSignedTimeWithFilter(filter: filters).Where(x => EvidenceSecurity.CanRead(x)).Select(x => new SignedTimeWrapper(x)).ToSmartList();
            return result;
        }

        [Read(@"signed_time/filter")]
        public IEnumerable<SignedTimeWrapper> GetSignedTimeByFilter(string signedTime_owner, string signedTime_from, string signedTime_to, string projectId, string signedTime_less_than, string signedTime_more_than, string FilterValue)
        {
            FilterColection filters = new FilterColection();
            if (signedTime_owner != null)
            {
                var userFilter = new UserFilter(signedTime_owner);
                userFilter.TableAlias = "uv";
                filters.AddFilter(userFilter, currentUserID);
            }
            if (signedTime_from != null)
            {
                filters.AddFilter(new DateTimeFrom(signedTime_from), currentUserID);
            }
            if (signedTime_to != null)
            {
                filters.AddFilter(new DateTimeTo(signedTime_to), currentUserID);
            }
            if (signedTime_less_than != null)
            {
                filters.AddFilter(new TotalSignedTimeLess(signedTime_less_than), currentUserID);
            }
            if (signedTime_more_than != null)
            {
                filters.AddFilter(new TotalSignedTimeMore(signedTime_more_than), currentUserID);
            }
            if (projectId != null)
            {
                filters.AddFilter(new UvProjectFilter(projectId), currentUserID);
            }
            if (FilterValue != null)
            {
                var searchFilter = new SearchFilter(FilterValue);
                searchFilter.TableAlias = "uv";
                filters.AddFilter(searchFilter, currentUserID);
            }
            OrderByItemCollection orderBy = getSignedTimeOrderBy();
            var result = ((SignedTimeAction)EVGlobal.GetSignedTimeAction()).GetSignedTimeWithFilter(filters, orderBy).Where(x => EvidenceSecurity.CanRead(x)).Select(x => new SignedTimeWrapper(x)).ToSmartList();
            return result;
        }

        #endregion

        #region update
        [Update(@"signed_time/{id:[0-9]+}")]
        public int UpdateSignedTime(int id, string title, string description, Guid userID, int totalSignedTime, int? taskID, int? projectID, int typeSignedTime)
        {
            SignedTime signedTime = ((SignedTimeAction)EVGlobal.GetSignedTimeAction()).GetByID(id);

            signedTime.Title = title;
            signedTime.Description = description;
            signedTime.UserID = userID;
            signedTime.TotalMinutes = totalSignedTime;
            signedTime.TypeSignedTime = typeSignedTime == 0 ? SignedTimeType.onProject : SignedTimeType.onTask;
            signedTime.TaskID = taskID;
            signedTime.ProjectID = projectID;
            signedTime.Update();
            return signedTime.ID;
        }
        #endregion

        #region create
        [Create(@"signed_time")]
        public int CreateNewSignedTime(string title, string description, Guid userID, int totalSignedTime, int? taskID, int? projectID, int typeSignedTime)
        {
            SignedTime existSignedTime = null;
            if (typeSignedTime == 0)
                existSignedTime = ((SignedTimeAction)EVGlobal.GetSignedTimeAction()).GetByUserDomainObjectPair(userID, projectID: projectID.ToString());

            else if (typeSignedTime == 1)
                existSignedTime = ((SignedTimeAction)EVGlobal.GetSignedTimeAction()).GetByUserDomainObjectPair(userID, taskID: taskID.ToString());

            if (existSignedTime != null) return -1;

            var signedTime = new SignedTime
            {
                Title = title,
                Description = description,
                UserID = userID,
                TotalMinutes = totalSignedTime,
                TypeSignedTime = typeSignedTime == 0 ? SignedTimeType.onProject : SignedTimeType.onTask,
                TaskID = taskID,
                ProjectID = projectID,
                CreatedBy = currentUserID,
                DateTimeCreated = DateTime.Now
            };
            signedTime.Create();
            return signedTime.ID;
        }

        #endregion

        #region delete
        [Delete(@"signed_time/{id:[0-9]+}")]
        public int? DeleteSignedTime(int? id)
        {
            if (id == null)
            {
                return id;
            }
            int signedTimeID = Convert.ToInt32(id);
            SignedTime signedTime = ((SignedTimeAction)EVGlobal.GetSignedTimeAction()).GetByID(signedTimeID);
            if (canDeleteSignedTime(signedTime))
            {
                signedTime.Delete();
                return id;
            }
            return null;
        }

        #endregion

        #region additional methods
        bool canDeleteSignedTime(SignedTime signedTime)
        {
            return isCurrentUserAdmin() || isCurrentUserOwner(signedTime.UserID.ToString());
        }

        OrderByItemCollection getSignedTimeOrderBy()
        {

            OrderByItemCollection orderByCollection = new OrderByItemCollection(!_context.SortDescending);
            switch (_context.SortBy)
            {
                case "title":
                    {
                        var titleOrderBy = new TitleOrderBy(!_context.SortDescending, "uv");
                        orderByCollection.AddOrderByItem(titleOrderBy);
                    } break;
                case "date":
                    {
                        var datetimeOrderBy = new DateOrderBy(!_context.SortDescending, "uv", "datetime_created");
                        orderByCollection.AddOrderByItem(datetimeOrderBy);
                    } break;
                case "signedTime": orderByCollection.AddOrderByItem(new TotalSignedTimeOrderBy(!_context.SortDescending)); break;
                default:
                    break;
            }

            return orderByCollection;
        }

        bool canUpdateSignedTime()
        {
            return isCurrentUserAdmin();
        }

        #endregion

        #endregion

    }
}