﻿using System;
using System.Linq;
using ASC.Core;
using ASC.Core.Users;
using ASC.Projects.Core.Domain;
using ASC.Projects.Data;
using ASC.Web.Core;
using System.Web;
using System.IO;
using ASC.Web.Core.Utility.Settings;
using ASC.Web.Studio.Utility;
using ASC.Projects.Engine;
using ASC.Web.Projects.HoangEvidence.EvEntities;
using ASC.Web.Projects.Classes;




namespace ASC.Web.Projects.HoangEvidence.Classes
{
    public class EvidenceSecurity
    {

        #region Properties

        private static Guid CurrentUserId
        {
            get { return SecurityContext.CurrentAccount.ID; }
        }

        public static bool CurrentUserAdministrator
        {
            get { return IsAdministrator(CurrentUserId); }
        }

        private static bool CurrentUserIsVisitor
        {
            get { return IsVisitor(CurrentUserId); }
        }

        private static bool CurrentUserIsOutsider
        {
            get { return IsOutsider(CurrentUserId); }
        }


        public static bool IsProjectsEnabled(Guid userID)
        {
            var projects = WebItemManager.Instance[WebItemManager.ProjectsProductID];

            if (projects != null)
            {
                return !projects.IsDisabled(userID);
            }

            return false;
        }

        public static bool IsCrmEnabled(Guid userID)
        {
            var projects = WebItemManager.Instance[WebItemManager.CRMProductID];

            if (projects != null)
            {
                return !projects.IsDisabled(userID);
            }

            return false;
        }

        #endregion


        #region Is.. block

        public static bool IsAdministrator(Guid userId)
        {
            return CoreContext.UserManager.IsUserInGroup(userId, Constants.GroupAdmin.ID) ||
                   WebItemSecurity.IsProductAdministrator(EngineFactory.ProductId, userId);
        }

        private static bool IsProjectManager(Project project)
        {
            return IsProjectManager(project, CurrentUserId);
        }

        private static bool IsProjectManager(Project project, Guid userId)
        {
            return (IsAdministrator(userId) || (project != null && project.Responsible == userId)) &&
                   !CurrentUserIsVisitor;
        }

        public static bool IsVisitor(Guid userId)
        {
            return CoreContext.UserManager.GetUsers(userId).IsVisitor();
        }

        public static bool IsOutsider(Guid userId)
        {
            return CoreContext.UserManager.GetUsers(userId).IsOutsider();
        }

        private static bool isEvidenceOwner(SignedTime signedTime)
        {
            return CurrentUserId == signedTime.UserID;
        }

        #endregion

        #region can Read

        public static bool CanRead(SignedTime signedTime)
        {
            if (signedTime == null) return false;

            int projectID = signedTime.ProjectID != null ? (int)signedTime.ProjectID : (int)signedTime.ProjectWithTaskID;
            Project project = Global.EngineFactory.GetProjectEngine().GetByID(projectID);
            bool canRead = IsProjectManager(project) || isEvidenceOwner(signedTime) || CurrentUserAdministrator;
            return canRead;
        }

        #endregion

        #region can Edit

        public static bool CanEdit(SignedTime signedTime)
        {
            if (signedTime == null) return false;
            int projectID = signedTime.ProjectID != null ? (int)signedTime.ProjectID : (int)signedTime.ProjectWithTaskID;
            Project project = Global.EngineFactory.GetProjectEngine().GetByID(projectID);
            bool canEdit = IsProjectManager(project) || CurrentUserAdministrator;
            return canEdit;
        }

        #endregion

        #region can Delete
        public static bool CanDelete(SignedTime signedTime)
        {
            if (signedTime == null) return false;
            int projectID = signedTime.ProjectID != null ? (int)signedTime.ProjectID : (int)signedTime.ProjectWithTaskID;
            Project project = Global.EngineFactory.GetProjectEngine().GetByID(projectID);
            return IsProjectManager(project) || CurrentUserAdministrator;
        }

        #endregion

        #region Can Create
        public static bool CanCreate()
        {
            bool canCreate = Global.EngineFactory.GetProjectEngine().GetAll().Any(p => IsProjectManager(p));
            return canCreate || CurrentUserAdministrator;
        }
        #endregion

        #region Demand
        public static void DemandRead(SignedTime signedTime)
        {
            if (!CanRead(signedTime)) throw CreateSecurityException();
        }
        public static void DemandEdit(SignedTime signedTime)
        {
            if (!CanEdit(signedTime)) throw CreateSecurityException();
        }
        public static void DemandCreate()
        {
            if (!CanCreate()) throw CreateSecurityException();
        }
        public static void DemandDelete(SignedTime signedTime)
        {
            if (!CanDelete(signedTime)) throw CreateSecurityException();
        }
        #endregion

        #region Exception

        public static Exception CreateSecurityException()
        {
            throw new System.Security.SecurityException("Access denied.");
        }

        #endregion
    }
}