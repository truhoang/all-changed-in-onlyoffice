﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ASC.Web.Studio.Utility;
using ASC.Web.Core.Files;
using ASC.Data.Storage;

namespace ASC.Web.Projects.HoangEvidence.Classes
{
    public class PathProvider
    {
        public static readonly String BaseVirtualPath = "~/Products/Hoang.Web.EvidenceVykazu/".ToLower();
        public static readonly String BaseAbsolutePath = CommonLinkUtility.ToAbsolute(BaseVirtualPath).ToLower();

        public static String GetFileStaticRelativePath(String fileName)
        {
            var ext = FileUtility.GetFileExtension(fileName);
            switch (ext)
            {
                case ".js":
                    return VirtualPathUtility.ToAbsolute("~/products/Hoang.Web.EvidenceVykazu/JS/" + fileName);
                case ".png":
                    return WebPath.GetPath("/products/Hoang.Web.EvidenceVykazu/App_Themes/Default/Images/" + fileName);
                case ".ascx":
                    return CommonLinkUtility.ToAbsolute("~/products/Hoang.Web.EvidenceVykazu/UserControls/" + fileName).ToLowerInvariant();
                case ".css":
                    return VirtualPathUtility.ToAbsolute("~/products/Hoang.Web.EvidenceVykazu/app_themes/default/css/" + fileName);
            }
            return fileName;
        }

        public static string GetVirtualPath(string physicalPath)
        {
            var rootpath = HttpContext.Current.Server.MapPath("~/");
            return "~/" + physicalPath.Replace(rootpath, string.Empty).Replace("\\", "/");
        }
    }
}