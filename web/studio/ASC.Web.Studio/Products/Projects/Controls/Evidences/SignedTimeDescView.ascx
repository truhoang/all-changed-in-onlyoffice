﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SignedTimeDescView.ascx.cs" Inherits="ASC.Web.Projects.Controls.Evidences.SignedTimeDescView" %>
<%@ Import Namespace="ASC.Web.Projects.Resources" %>
<%@ Import Namespace="ASC.Web.Projects.HoangEvidence.EvEntities" %>
<%@ Import Namespace="ASC.Web.Projects.HoangEvidence.Classes" %>


<div id="signedTimeActions" class="studio-action-panel">
    <ul class="dropdown-content">
        <li><a id="signedTimeActionButton" href="<%=getEditUrl()%>" class="dropdown-item"><%=EvidenceResource.Edit%></a></li>
        <li><a id="signedTimeDeleteButton" class="dropdown-item"><%=EvidenceResource.Delete %></a></li>
    </ul>
</div>

<%--<div id="evidenceActionsContainer" class="big-button-container">
    
    <span class="splitter-buttons"></span>
    
    <span class="splitter-buttons"></span>
    <a id="cancelEditEvidence" class="button gray big">
        <%= EvidenceResource.Cancel%>
    </a>
</div>--%>

<%
    if (!EvidenceSecurity.CanRead(SignedTime))
    { %>
<div id="toast-container" class="toast-top-right">
    <div class="toast toast-error">
        <div class="toast-message"><%=EvidenceResource.AccessDenied%></div>
    </div>
</div>
<% return;
    }%>

<div class="evidenceContent">
    <div>
        <span class="st-headerLeft">
            <%= EvidenceResource.TotalSignedTime%>:
        </span>
        <span class="st-fieldRight">
            <%= SignedTime.TotalMinutes%>
        </span>
    </div>

    <div>
        <span class="st-headerLeft">
            <%= EvidenceResource.Description%>:
        </span>
        <span class="st-fieldRight">
            <%= SignedTime.Description.HtmlEncode()%>
        </span>
    </div>

    <div>
        <span class="st-headerLeft">
            <%= EvidenceResource.UserName%>:
        </span>
        <span class="st-fieldRight">
            <a href="/products/people/profile.aspx?user=<%= SignedTime.UserName.HtmlEncode()%>"><%= SignedTime.UserName.HtmlEncode()%></a>
        </span>
    </div>

    <div>
        <span class="st-headerLeft">
            <%= EvidenceResource.TypeSignedTime%>:
        </span>
        <span class="st-fieldRight">
            <%=getTypeSignedTime()%>
        </span>
    </div>

    <%if (SignedTime.TypeSignedTime == SignedTimeType.onTask)
      {%>
    <div>
        <span class="st-headerLeft">
            <%= ProjectResource.Tasks%>:
        </span>
        <span class="st-fieldRight">
            <a href="tasks.aspx?prjID=<%=SignedTime.ProjectWithTaskID%>&id=<%=SignedTime.TaskID%>"><%=SignedTime.TaskTitle.HtmlEncode()%> </a>
        </span>
    </div>

    <div>
        <span class="st-headerLeft">
            <%= ProjectResource.Project%>:
        </span>
        <span class="st-fieldRight">
            <a href="Projects.aspx?prjID=<%=SignedTime.ProjectWithTaskID%>"><%= SignedTime.ProjectWithTaskTitle.HtmlEncode()%></a>
        </span>
    </div>
    <%}
      else
      {%>
    <div>
        <span class="st-headerLeft">
            <%= ProjectResource.Project%>:
        </span>
        <span class="st-fieldRight">
            <a href="Projects.aspx?prjID=<%=SignedTime.ProjectID%>"><%=SignedTime.ProjectTitle%></a>
        </span>
    </div>
    <%}%>

    <div>
        <span class="st-headerLeft">
            <%= EvidenceResource.DateCreated%>:
        </span>
        <span class="st-fieldRight">
            <%=SignedTime.DateTimeCreated%>
        </span>
    </div>

    <div>
        <span class="st-headerLeft">
            <%= EvidenceResource.CreateBy%>:
        </span>
        <span class="st-fieldRight">
            <a href="/products/people/profile.aspx?user=<%=SignedTime.CreatedByName%>"><%=SignedTime.CreatedByName%></a>
        </span>
    </div>

    <div>
        <span class="st-headerLeft">
            <%= EvidenceResource.LastModifiedBy%>:
        </span>
        <span class="st-fieldRight">
            <a href="/products/people/profile.aspx?user=<%=SignedTime.LastModifiedByName%>"><%=SignedTime.LastModifiedByName%></a>
        </span>
    </div>

    <div>
        <span class="st-headerLeft">
            <%= EvidenceResource.LastModifiedOn%>:
        </span>
        <span class="st-fieldRight">
            <%=SignedTime.LastModifiedOn%>
        </span>
    </div>
</div>
