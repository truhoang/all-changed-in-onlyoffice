﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ASC.Web.Projects.HoangEvidence.EvEntities;
using ASC.Web.Projects.Classes;
using ASC.Web.Studio.Utility.HtmlUtility;
using AjaxPro;
using ASC.Web.Studio.Utility;


namespace ASC.Web.Projects.Controls.Evidences
{
    public partial class SignedTimeDescView : System.Web.UI.UserControl
    {
        public SignedTime SignedTime { get; set; }
        public bool CanEditSignedTime { get; set; }
        public bool CanReadSignedTime { get; set; }
        public bool CanCreateSignedTime { get; set; }
        public bool CanDeleteSignedTime { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            Utility.RegisterTypeForAjax(typeof(SignedTimeDescView), Page);
        }

        protected string getEditUrl()
        {
            var url = String.Concat(PathProvider.BaseAbsolutePath, "SignedTimes.aspx?id=" + SignedTime.ID, "&action=edit");
            return url;
        }

        protected string getTypeSignedTime()
        {
            if (SignedTime.TypeSignedTime == SignedTimeType.onTask)
            {
                return "On Task";
            }

            if (SignedTime.TypeSignedTime == SignedTimeType.onProject)
            {
                return "On Project";
            }
            return null;
        }
    }
}