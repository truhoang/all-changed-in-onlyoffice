﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ASC.Web.Projects.HoangEvidence.EvEntities;
using ASC.Web.Projects.HoangEvidence.Classes;
using ASC.Core.Users;
using ASC.Core;


namespace ASC.Web.Projects.Controls.Evidences
{
    public partial class STAction : System.Web.UI.UserControl
    {
        public SignedTime SignedTime { get; set; }
        public string typeAction = "add";
        public bool CanDelete = false;
        protected UserInfo CurrentUser;

        protected void Page_Load(object sender, EventArgs e)
        {

            if (typeAction == "edit")
            {
                initForEdit();
            }
            else
            {
                if (typeAction == "add")
                {
                    initForAdd();
                }
            }
        }

        void setProjectSelection()
        {
            //EVGlobal.GetEngineFactory().GetProjectEngine().GetAll().Sort(;
        }
        void setTaskSelection()
        {

        }

        void initForEdit()
        {
            signedTimeTitle.Text = SignedTime.Title;
            signedTimeDescription.Text = SignedTime.Description;
            totalSignedTime.Value = SignedTime.TotalMinutes.ToString();
            userSelection.Items.Add(new ListItem(SignedTime.UserName, SignedTime.UserID.ToString()));

            initTypeSignedTime();

            typeSignedTimeSelection.Items[1].Selected = SignedTime.TypeSignedTime == SignedTimeType.onTask;

            var projecID = SignedTime.ProjectID;
            var projectTitle = SignedTime.ProjectTitle;

            if (SignedTime.TypeSignedTime == SignedTimeType.onTask)
            {
                projecID = SignedTime.ProjectWithTaskID;
                projectTitle = SignedTime.ProjectWithTaskTitle;
                signedTimeTaskSelection.Items.Add(new ListItem(SignedTime.TaskTitle, SignedTime.TaskID.ToString()));
            }
            signedTimeProjectSelection.Items.Add(new ListItem(projectTitle, projecID.ToString()));
        }

        void initForAdd()
        {
            initTypeSignedTime();
        }

        void initTypeSignedTime()
        {
            typeSignedTimeSelection.Items.Add(new ListItem("On Project", "0"));
            typeSignedTimeSelection.Items.Add(new ListItem("On Task", "1"));
        }


        protected void projectSelection_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}