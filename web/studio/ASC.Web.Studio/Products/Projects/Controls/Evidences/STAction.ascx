﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="STAction.ascx.cs" Inherits="ASC.Web.Projects.Controls.Evidences.STAction" %>
<%@ Import Namespace="ASC.Web.Projects.Resources" %>
<%@ Import Namespace="ASC.Web.Projects.HoangEvidence.Classes" %>

<%
    if (typeAction == "edit")
    {
        if (!EvidenceSecurity.CanEdit(SignedTime))
        { %>
<div id="toast-container" class="toast-top-right">
    <div class="toast toast-error">
        <div class="toast-message"><%=EvidenceResource.AccessDenied%></div>
    </div>
</div>
<% return;
        }
    }
    else
    {
        if (!EvidenceSecurity.CanCreate())
        { %>
<div id="toast-container" class="toast-top-right">
    <div class="toast toast-error">
        <div class="toast-message"><%=EvidenceResource.AccessDenied%></div>
    </div>
</div>
<%return;
        }
    }%>

<div id="signedTimeTitleContainer" class="block-cnt-splitter requiredField">
    <span class="requiredErrorText"><%= EvidenceResource.EmptySignedTimeTitle %></span>
    <div class="headerPanelSmall">
        <%= EvidenceResource.SignedTimeTitle%>
    </div>
    <div class="inputTitleContainer">
        <asp:TextBox autocomplete="off" CssClass="textEdit" ID="signedTimeTitle" MaxLength="250" runat="server" Width="100%">
        </asp:TextBox>
    </div>
    <div style="clear: both;"></div>
</div>

<div id="signedTimeDescriptionContainer" class="block-cnt-splitter">
    <div class="headerPanelSmall">
        <%= EvidenceResource.EvidenceDescription%>
    </div>
    <div class="dottedHeaderContent">
        <asp:TextBox ID="signedTimeDescription" Width="100%" runat="server" TextMode="MultiLine" Rows="6" autocomplete="off"></asp:TextBox>
    </div>
</div>

<%--<div id="signedTimeDateContainer" class="block-cnt-splitter requiredField">
    <span class="requiredErrorText"><%= EvidenceResource.DateError %></span>
    <div class="headerPanelSmall">
        <%=EvidenceResource.Date%>
    </div>
    <input type="text" id="signedTimeDate" class="textEditCalendar" runat="server" />
    <span class="splitter"></span><span class="dottedLink" id="todayTemplate">
        <%= EvidenceResource.Today%></span>
</div>--%>

<%--<div id="startTimeContainer" class="dottedHeaderContent block-cnt-splitter requiredField">
    <span class="requiredErrorText startDate-error"><%= EvidenceResource.signedTimeStartTimeError%></span>
    <div class="headerPanelSmall">
        <%=EvidenceResource.StartTime%>
    </div>
    <input type="text" id="signedTimeStarttime" class="textEdit" runat="server" />
    <span class="splitter" id="timeNow"></span><span class="dottedLink deadline_left" id="nowTemplate">
        <%= EvidenceResource.Now%></span>
</div>--%>


<div id="signedTimeDurationContainer" class="dottedHeaderContent block-cnt-splitter requiredField">
    <span class="requiredErrorText"><%=EvidenceResource.TotalTimeError%></span>
    <div class="headerPanelSmall">
        <%=EvidenceResource.TotalSignedTime%>
    </div>
    <input id="totalSignedTime" class="STComboBox" maxlength="9" runat="server" />
    <%-- <span class="splitter"></span><span class="dottedLink deadline_left" id="thirtyMinutes" data-value="30">
        <%= EvidenceResource.ThirtyMinutes%>
    </span><span class="splitter"></span><span class="dottedLink deadline_left" id="fortyFiveMinutes" data-value="45">
        <%= EvidenceResource.FortyFiveMinutes%>
    </span><span class="splitter"></span><span class="dottedLink deadline_left" id="sixtyMinutes" data-value="60">
        <%= EvidenceResource.SixtyMinutes %>
    </span>--%>
</div>

<div id="userContainer" class="dottedHeaderContent block-cnt-splitter requiredField">
    <span class="requiredErrorText"><%=EvidenceResource.UserSelectionError%></span>
    <div class="headerPanelSmall">
        <%=EvidenceResource.UserSelection%>
    </div>
    <div class="DropdownList" runat="server">
        <select id="userSelection" class="STComboBox" runat="server"></select>
    </div>
</div>

<div id="typeSignedTimeContainer" class="dottedHeaderContent block-cnt-splitter requiredField">
    <span class="requiredErrorText"><%=EvidenceResource.TypeSignedTimeError%></span>
    <div class="headerPanelSmall">
        <%=EvidenceResource.TypeSignedTime%>
    </div>
    <div class="typeSignedTimeDropdown" runat="server">
        <select id="typeSignedTimeSelection" class="STComboBox" runat="server"></select>
    </div>
</div>


<div id="signedTimeProjectSelectionContainer" class="dottedHeaderContent block-cnt-splitter requiredField">
    <span class="requiredErrorText"><%=  EvidenceResource.ProjectSelectionError%></span>
    <div class="headerPanelSmall">
        <%=EvidenceResource.ProjectSelection%>
    </div>
    <div class="signedTimeProjDropdown">
        <select id="signedTimeProjectSelection" class="STComboBox" runat="server">
        </select>
    </div>
</div>

<div id="signedTimeTaskSelectionContainer" class="dottedHeaderContent block-cnt-splitter requiredField" runat="server">
    <span class="requiredErrorText"><%=EvidenceResource.TaskSelectionError%></span>
    <div class="headerPanelSmall">
        <%=EvidenceResource.TaskSelection%>
    </div>
    <div class="signedTimeTaskDropdown">
        <select id="signedTimeTaskSelection" class="STComboBox" runat="server"></select>
    </div>
</div>


<%--Save or Add project--%>
<div id="signedTimeActionsContainer" class="big-button-container">

    <a id="signedTimeUpdateOrAddButton" class="button green big">
        <%
            if (typeAction == "edit")
            {%>
        <%=EvidenceResource.Save%>
        <%}
            else
            {%>
        <%=EvidenceResource.Add%>
        <%}%>
    </a>

    <%if (typeAction == "edit")
      {%>
    <span class="splitter-buttons"></span>
    <a id="signedTimeDeleteButton" class="button gray big">
        <%=EvidenceResource.Delete%>
    </a>
    <%}%>

    <span class="splitter-buttons"></span>
    <a id="cancelEditsignedTime" class="button gray big">
        <%= "Cancel"%>
    </a>
</div>
