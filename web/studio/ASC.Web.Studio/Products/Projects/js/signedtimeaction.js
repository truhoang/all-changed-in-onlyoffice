/*
 * 
 * (c) Copyright Ascensio System SIA 2010-2015
 * 
 * This program is a free software product.
 * You can redistribute it and/or modify it under the terms of the GNU Affero General Public License
 * (AGPL) version 3 as published by the Free Software Foundation. 
 * In accordance with Section 7(a) of the GNU AGPL its Section 15 shall be amended to the effect 
 * that Ascensio System SIA expressly excludes the warranty of non-infringement of any third-party rights.
 * 
 * This program is distributed WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * For details, see the GNU AGPL at: http://www.gnu.org/licenses/agpl-3.0.html
 * 
 * You can contact Ascensio System SIA at Lubanas st. 125a-25, Riga, Latvia, EU, LV-1021.
 * 
 * The interactive user interfaces in modified source and object code versions of the Program 
 * must display Appropriate Legal Notices, as required under Section 5 of the GNU AGPL version 3.
 * 
 * Pursuant to Section 7(b) of the License you must retain the original Product logo when distributing the program. 
 * Pursuant to Section 7(e) we decline to grant you any rights under trademark law for use of our trademarks.
 * 
 * All the Product's GUI elements, including illustrations and icon sets, as well as technical 
 * writing content are licensed under the terms of the Creative Commons Attribution-ShareAlike 4.0 International. 
 * See the License terms at http://creativecommons.org/licenses/by-sa/4.0/legalcode
 * 
*/
//added by hoang

ASC.Projects.SignedTimeAction = (function () {
    var $signedTimeActionsContainer = jq("#signedTimeActionsContainer");
    var $taskContainer = jq("#taskContainer");
    var currentUserId;
    var isInited = false;
    var $projectSelect = jq('[id$=signedTimeProjectSelection]');
    var actionType;
    var signedTimeID;
    var $signedTimeTitle = jq('[id$=signedTimeTitle]');
    var $signedTimeDescription = jq('[id$=signedTimeDescription]');
    var $totalSignedTime = jq('[id$=totalSignedTime]');
    var $userSelection = jq('[id$=userSelection]');
    var $typeSignedTime = jq('[id$=typeSignedTimeSelection]');
    var $signedTimeProjectSelection = jq('[id$=signedTimeProjectSelection]');
    var $signedTimeTaskSelection = jq('[id$=signedTimeTaskSelection]');
    var $signedTimeUpdateOrAddButton = jq("#signedTimeUpdateOrAddButton");
    var $signedTimeDeleteButton = jq("#signedTimeDeleteButton");
    var $cancelEditsignedTime = jq("#cancelEditsignedTime");
    var $signedTimeTaskSelectionContainer = jq("[id$=signedTimeTaskSelectionContainer]");
    var signedTimeForEdit;


    //region signedTime
    var onDeleteSignedTime = function (params, result) {
        if (result) {
            var pathname = window.location.pathname; // Returns path only
            window.location.replace(pathname);
            return;
        }
        window.alert("you do not have delete permission");
    }

    $signedTimeDeleteButton.click(function () {
        Teamlab.deleteSignedTime({}, signedTimeID, { filter: {}, success: onDeleteSignedTime });
    })
    //endregion delete

    //region cancel
    $cancelEditsignedTime.click(function () {
        var pathname = window.location.pathname; // Returns path only
        window.location.replace(pathname);
    })
    //endregion cancel

    //region template events
    $typeSignedTime.change(function () {
        if ($typeSignedTime.val() == 1) {
            $signedTimeTaskSelectionContainer.show();
        }
        else {
            $signedTimeTaskSelectionContainer.hide();
        }

    })
    //endregion template events

    //region task project selection

    var getTaskFilter = function () {
        var filter = {
            sortBy: "deadline",
            sortOrder: "ascending",
            StartIndex: 0
        };
        return filter;
    }
    var getProjectFilter = function () {
        var filter = {
            sortBy: "title",
            sortOrder: "ascending",
            StartIndex: 0,
            manager: currentUserId,
            allowAdmin: 1
        };
        return filter;
    }

    var initTaskSelection = function (projectId) {
        var filter = getTaskFilter();
        if (projectId) {
            filter.projectId = projectId;
        }
        Teamlab.getPrjTasks({}, { filter: filter, success: onGetTasks });
    };

    $projectSelect.change(function () {
        initTaskSelection($projectSelect.val());
    });


    var appendOptionToSelect = function ($optionElement, optionList, addedOption, user) {
        for (var i = 0; i < optionList.length; i++) {
            var text = optionList[i].title;
            if (user) {
                text = optionList[i].displayName;
            }
            if (addedOption.id != optionList[i].id) {
                $optionElement.append(jq('<option/>', {
                    value: optionList[i].id,
                    text: text
                }));
            }
        }
    }


    var onGetProjects = function (params, listProj) {
        var addedOption = { id: "" };
        if (isInited) {
            $projectSelect.empty();
        } else {
            addedOption.id = $projectSelect.val();
            if ($typeSignedTime.val() == 0 || !$signedTimeTaskSelection.val()) isInited = true;
        }
        appendOptionToSelect($projectSelect, listProj, addedOption);
        $projectSelect.trigger("change");
    }

    var onGetTasks = function (params, tasks) {
        $taskSelect = jq('[id$=signedTimeTaskSelection]');
        var addedOption = { id: "" };
        if (isInited) {
            $taskSelect.empty();
        } else {
            addedOption.id = $taskSelect.val();
            isInited = true;
        }
        appendOptionToSelect($taskSelect, tasks, addedOption);
    }

    var initProjSelection = function (participant) {
        var filter = getProjectFilter();
        if (participant) filter.participant = participant;
        Teamlab.getPrjProjects({}, { filter: filter, success: onGetProjects });
    }



    //end region task selection

    //region user selection

    $userSelection.change(function () {
        initProjSelection($userSelection.val());
    });

    var onGetUsers = function (params, users) {
        var addedOption = {
            id: $userSelection.val()
        }
        appendOptionToSelect($userSelection, users, addedOption, true);
        $userSelection.trigger("change");
    }
    var getUserFilter = function () {
        var filter = {
            sortBy: "username",
            sortOrder: "ascending",
            StartIndex: 0
        };
        return filter;
    }
    var iniUserSelection = function () {
        var filter = getUserFilter();
        Teamlab.getPeoleByResProjects({}, { filter: filter, success: onGetUsers });
    }
    //endregion user selection


    //region add evidence

    var getSignedTime = function () {

        var taskID = $typeSignedTime.val() == 1 ? $signedTimeTaskSelection.val() : null;
        var signedTime = {
            title: $signedTimeTitle.val(),
            description: $signedTimeDescription.val(),
            userID: $userSelection.val(),
            totalSignedTime: $totalSignedTime.val(),
            typeSignedTime: $typeSignedTime.val(),
            projectID: $signedTimeProjectSelection.val(),
            taskID: taskID
        };
        return signedTime;
    }

    var checkSignedTimeInput = function () {

        jq("#signedTimeTitleContainer, #signedTimeDurationContainer, #userContainer, #typeSignedTimeContainer, signedTimeProjectSelectionContainer, [id$=signedTimeTaskSelectionContainer]").removeClass("requiredFieldError");

        var scrollTo = '';
        var isError = false;

        if (!$signedTimeTitle.val() || $signedTimeTitle.val().length > 38) {
            jq('#signedTimeTitleContainer').addClass('requiredFieldError');
            isError = true;
            scrollTo = '#signedTimeTitleContainer';
        }


        if (!isPositiveInteger($totalSignedTime.val())) {
            jq('#signedTimeDurationContainer').addClass('requiredFieldError');
            isError = true;
            scrollTo = '#signedTimeDurationContainer';
        }

        if (!$userSelection.val()) {
            jq('#userContainer').addClass('requiredFieldError');
            isError = true;
            scrollTo = '#userContainer';
        }


        if (!($typeSignedTime.val() == 0 || $typeSignedTime.val() == 1)) {
            jq('#typeSignedTimeContainer').addClass('requiredFieldError');
            isError = true;
            scrollTo = '#typeSignedTimeContainer';
        }

        if (!isPositiveInteger($signedTimeProjectSelection.val())) {
            jq('#signedTimeProjectSelectionContainer').addClass('requiredFieldError');
            isError = true;
            scrollTo = '#signedTimeProjectSelectionContainer';
        }

        //type signedTime je na task
        if ($typeSignedTime.val() == 1) {
            if (!isPositiveInteger($signedTimeTaskSelection.val())) {
                jq('[id$=signedTimeTaskSelectionContainer]').addClass('requiredFieldError');
                isError = true;
                scrollTo = '[id$=signedTimeTaskSelectionContainer]';
            }
        }


        if (isError) {
            jq('body').scrollTo(scrollTo);
            return false;
        }
        return true;

    }

    var onUpdateOrAddSignedTime = function (params, id) {
        if (id == -1) {
            alert("this signed time for pair user project/task already exists, you can edit it from signed time page");
            return;
        }
        var pathname = window.location.pathname + "?action=view&id=" + id; // Returns path only
        window.location.replace(pathname);
    }

    var onUpdateOrAddSignedTimeError = function (params, id) {

    }


    //region add signed time
    $signedTimeActionsContainer.on('click', '#signedTimeUpdateOrAddButton', function () {

        if (!checkSignedTimeInput()) {
            return false;
        }
        var params = {};
        var signedTime = getSignedTime();
        if (actionType === "edit") {
            signedTime.id = signedTimeID;
            Teamlab.updateSignedTime(params, signedTimeID, signedTime, { success: onUpdateOrAddSignedTime, error: onUpdateOrAddSignedTimeError });
        }
        if (actionType === "add") {

            Teamlab.createSignedTime(params, signedTime, { success: onUpdateOrAddSignedTime, error: onUpdateOrAddSignedTimeError });
        }
    });
    //endregion add signed time

    var onGetInitSignedTime = function (params, signedTime) {

    }

    var initForEdit = function () {


    }

    var initForAdd = function () {
    }

    var initForView = function () {
    }


    var init = function (action, id) {
        actionType = action;
        signedTimeID = id;
        currentUserId = Teamlab.profile.id;
        if (actionType === "edit") {
            initForEdit();
        }
        if (actionType === "view") {
            initForEdit();
            return;
        }
        if (actionType === "add") {
            initForAdd();
        }
        if ($typeSignedTime.val() == 0) {
            $signedTimeTaskSelectionContainer.hide();
        }
        $signedTimeActionsContainer.show();
        iniUserSelection();

        //isInited = true;
    }

    return {
        init: init
    }
})(jQuery);
