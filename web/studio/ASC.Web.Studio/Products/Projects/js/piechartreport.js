﻿jq(document).ready(function () {
    var reportBody = jq("#piechartreportBody");
    if (!reportBody.length) {
        return;
    }
    var chartDivId = 0;
    var numberOfTaskPieChartInLine = 3;

    var getReportDataColums = function () {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Topping');
        data.addColumn('number', 'Slices');
        return data;
    };

    var drawPieChart = function (data, options, parentElement) {
        var childElement = newChartDiv(parentElement);
        var chart = new google.visualization.PieChart(childElement);
        chart.draw(data, options);
        return jq(childElement);
    };

    var userPieChartOptions = function () {
        var options = {
            'title': '',
            'width': 800,
            'height': 400,
            'chartArea': { 'left': '35%', 'top': 0, 'width': '100%', 'height': '100%' },
            'is3D': true
        }
        return options;
    };

    var userPieChartData = function (users) {
        var data = getReportDataColums();
        for (var i = 0; i < users.length; i++) {
            var user = users[i];
            var userName = jq(user).attr("DisplayName");
            var timeSpent = parseFloat(jq(user).attr("timeSpent"));
            data.addRow([userName, timeSpent]);
        }
        return data;
    };

    var newChartDiv = function (parentElement) {
        var childId = "chart_div" + chartDivId++;
        var chartDiv = jq("<div id='" + childId + "'/>");
        jq(parentElement).append(chartDiv);
        return document.getElementById(childId);
    };

    var drawUserPieChart = function (users, parentElement) {
        var data = userPieChartData(users);
        var options = userPieChartOptions();

        var divElement = drawPieChart(data, options, parentElement);
        var reportNameElement = jq("<h1>all selected users report</h1>");
        jq(reportNameElement).css("margin-left", "10px");
        jq(divElement).prepend(reportNameElement);
        setDivStyle(divElement);
    };

    var projectTaskPieChartOption = function (project) {
        var options = {
            'title': 'all tasks of "' + jq(project).attr("title") + '" report',
            'width': 300,
            'height': 200,
            pieHole: 0.3,
        }
        return options;
    };

    var projectTaskPieChartData = function (project) {
        var tasks = jq(project).children("task");
        var data = getReportDataColums();
        for (var i = 0; i < tasks.length; i++) {
            var task = tasks[i];
            var taskTitle = jq(task).attr("title");
            var timeSpent = parseFloat(jq(task).attr("timeSpent"));
            data.addRow([taskTitle, timeSpent]);
        }
        return data;
    };

    var userProjectPieChartOption = function (user) {
        var userName = jq(user).attr("DisplayName");
        var options = {
            'title': 'all projects of "' + userName + '" report',
            'width': 800,
            'height': 400,
            'chartArea': { 'left': '38%', 'top': 0, 'width': '100%', 'height': '100%' },
        }
        return options;
    };

    var userProjectPieChartData = function (user) {
        var projects = jq(user).children("project");
        var data = getReportDataColums();
        for (var i = 0; i < projects.length; i++) {
            var project = projects[i];
            var projectTitle = jq(project).attr("title");
            var timeSpent = parseFloat(jq(project).attr("timeSpent"));
            data.addRow([projectTitle, timeSpent]);
        }
        return data;
    };
    var setDivStyle = function (divElement) {
        jq(divElement).css("box-shadow", "0 0 3px 3px #888");
        jq(divElement).css("margin-bottom", "10px");
    }

    var drawProjectTaskPieChart = function (project, parentElement) {
        var data = projectTaskPieChartData(project);
        var options = projectTaskPieChartOption(project);
        drawPieChart(data, options, parentElement);
    };


    var drawUserProjectsPieChart = function (user, parentElement) {
        var data = userProjectPieChartData(user);
        var options = userProjectPieChartOption(user);
        var projectElement = drawPieChart(data, options, parentElement);
        var userNameElement = jq("<h2>" + jq(user).attr("DisplayName") + "</h2>");
        jq(userNameElement).css("margin", "0px");
        jq(userNameElement).css("margin-left", "5px");
        jq(projectElement).prepend(userNameElement);
        setDivStyle(projectElement);

        //draw task pieCharts of a project
        var projects = jq(user).children("project");
        var divBlock;
        for (var i = 0; i < projects.length; i++) {
            if (i % numberOfTaskPieChartInLine == 0) {
                divBlock = newChartDiv(projectElement);
                jq(divBlock).css("display", "flex");
            }
            drawProjectTaskPieChart(projects[i], divBlock);
        }
    };


    // Set a callback to run when the Google Visualization API is loaded.
    google.setOnLoadCallback(drawReport);

    // Callback that creates and populates a data table,
    // instantiates the pie chart, passes in the data and
    // draws it.
    function drawReport() {
        var users = jq(reportBody).children("user");
        drawUserPieChart(users, reportBody);

        var userChartDiv = newChartDiv(reportBody);
        for (var i = 0; i < users.length; i++) {
            drawUserProjectsPieChart(users[i], userChartDiv);
        }
        setDivStyle(userChartDiv);
        jq(userChartDiv).css("padding", "20px");
    }
});
