﻿jq(document).ready(function () {
    var reportBody = jq("#columnchartreportBody");
    if (!reportBody.length) return;

    var reportInfo = jq("reportinfo");
    var chartDivId = 0;
    var totalColumn = 12;


    var monthNames = {
        1: "January",
        2: "February",
        3: "March",
        4: "April",
        5: "May",
        6: "June",
        7: "July",
        8: "August",
        9: "September",
        10: "October",
        11: "November",
        12: "December",
    };

    var constElementNames = {
        reportinfo: "reportinfo",
        user: "user",
        columngroup: "columngroup",
        project: "project",
        task: "task"
    }

    var reportinfoAttributes = {
        fromdate: "fromdate",
        todate: "todate",
        timeintervaltype: "timeintervaltype"
    }

    var userAttributes = {
        link: "link",
        displayname: "displayname",
        timespent: "timespent"
    }

    var colGroupAttributes = {
        colnumber: "colnumber",
        dateorder: "dateorder",
        timespent: "timespent"
    }

    var projectAttributes = {
        id: "id",
        link: "link",
        title: "title",
        timespent: "timespent"
    }

    var newChartDiv = function (parentElement) {
        var childId = "chart_div" + chartDivId++;
        var chartDiv = jq("<div id='" + childId + "'/>");
        jq(parentElement).append(chartDiv);
        return document.getElementById(childId);
    };

    var removeDuplicate = function (allElements, uniqueAttribute) {
        var uniqueElements = [];
        var uniqueIds = [];
        jq.each(allElements, function (i, el) {
            if (jq.inArray(jq(el).attr(uniqueAttribute), uniqueIds) === -1) {
                uniqueIds.push(jq(el).attr(uniqueAttribute));
                uniqueElements.push(el);
            }
        });

        return uniqueElements
    };

    var getMaxColNumber = function (columnGroups) {
        var maximum = Number.MIN_VALUE;

        jq(columnGroups).each(function () {
            var value = parseInt(jq(this).attr(colGroupAttributes.colnumber));
            maximum = (value > maximum) ? value : maximum;
        });
        return maximum;
    }

    var getMinColNumber = function (columnGroups) {
        var minimum = Number.MAX_VALUE;

        jq(columnGroups).each(function () {
            var value = parseInt(jq(this).attr(colGroupAttributes.colnumber));
            minimum = (value < minimum) ? value : minimum;
        });
        return minimum;
    }

    var getNextColGroup = function (currentColGroup) {
        if (currentColGroup == 12) return 1;
        return currentColGroup + 1;
    }

    var getColumnGroups = function (parentElement) {
        var columnGroups = removeDuplicate(jq(parentElement).find(constElementNames.columngroup), colGroupAttributes.colnumber);
        var cols = jq(columnGroups).sort(function (a, b) {
            var n1 = jq(a).attr(colGroupAttributes.dateorder);
            var n2 = jq(b).attr(colGroupAttributes.dateorder);
            return (n1 > n2) ? (n1 > n2) ? 1 : 0 : -1;
        });
        return cols;
    }

    var getAllProjects = function (parentElement) {
        var projects = removeDuplicate(jq(parentElement).find(constElementNames.project), projectAttributes.id);
        return projects;
    }

    var getAllProjectTitles = function (projects) {
        var ProjectTitles = ['time'];
        for (var i = 0; i < projects.length; i++) {
            ProjectTitles.push(jq(projects[i]).attr(projectAttributes.title));
        }
        return ProjectTitles;
    }

    var getFromDateToDateString = function () {
        var fromDate = jq(constElementNames.reportinfo).attr(reportinfoAttributes.fromdate);
        var toDate = jq(constElementNames.reportinfo).attr(reportinfoAttributes.todate);
        return fromDate + " to " + toDate;
    }

    var allUserColumnChartOptions = function () {
        var options = {
            title: "All selected users report from " + getFromDateToDateString(),
            height: 400,
            legend: { position: 'top', maxLines: 3 },
            isStacked: true
        };
        return options;
    }

    var getColGroupTitle = function (columnNumber) {
        var timeIntervalType = jq(reportInfo).attr(reportinfoAttributes.timeintervaltype);
        if (timeIntervalType === "Month")
            return monthNames[columnNumber];

        if (timeIntervalType === "year") {
            var fromYear = jq(reportInfo).attr(reportinfoAttributes.fromdate);
            fromYear = fromYear.substring(fromYear.length - 4);
            return (parseInt(fromYear) + columnNumber - 1).toString();
        }

        return timeIntervalType + " " + columnNumber;
    }

    var getProjectTimeFromColGroups = function (colGroups, project) {
        var id = jq(project).attr(projectAttributes.id);
        var projects = jq(colGroups).find(constElementNames.project + "[" + projectAttributes.id + "=" + id + "]");
        var sum = 0;
        for (var i = 0; i < projects.length; i++) {
            sum += parseFloat(jq(projects[i]).attr(projectAttributes.timespent));
        }
        return sum;
    }

    var getColumnChartData = function (reportBodyData) {
        var data = [];
        var projects = getAllProjects(reportBodyData);
        var firstRow = getAllProjectTitles(projects);
        data.push(firstRow);
        var columnGroups = getColumnGroups(reportBodyData);
        var allColGroupWithDuplicite = jq(reportBodyData).find(constElementNames.columngroup);
        var startColumn = jq(columnGroups).first().attr(colGroupAttributes.colnumber);
        var endColumn = jq(columnGroups).last().attr(colGroupAttributes.colnumber);
        var numberOfDrawnColumns = 1;
        for (var i = parseInt(startColumn) ; true; i = getNextColGroup(i)) {
            var row = [];
            row.push(getColGroupTitle(i));
            var columnGroup = jq(columnGroups).filter("[" + colGroupAttributes.colnumber + "=" + i + "]");
            if (columnGroup.length === 0) {
                for (var j = 0; j < projects.length; j++) {
                    row.push(0);
                }
            }
            else {
                var colNumber = i;
                var sameColGroups = jq(allColGroupWithDuplicite).filter("[" + colGroupAttributes.colnumber + "=" + colNumber + "]");
                for (var j = 0; j < projects.length; j++) {
                    row.push(getProjectTimeFromColGroups(sameColGroups, projects[j]));
                }
            }
            data.push(row);
            if (i === parseInt(endColumn)) break;
            if (numberOfDrawnColumns === totalColumn) break;
            numberOfDrawnColumns++;
        }

        return new google.visualization.arrayToDataTable(data);
    }

    var userColumnChartOptions = function (user) {
        var options = {
            title: jq(user).attr(userAttributes.displayname) + " report from " + getFromDateToDateString(),
            width: 900,
            height: 400,
            legend: { position: 'top', maxLines: 3 },
            isStacked: true,
        };
        return options;
    }

    var drawColumnChart = function (data, options, parentElement) {
        var childElement = newChartDiv(parentElement);
        var chart = new google.visualization.ColumnChart(childElement);
        chart.draw(data, options);
        return jq(childElement);
    }

    var setDivStyle = function (divElement) {
        jq(divElement).css("box-shadow", "0 0 3px 3px #888");
        jq(divElement).css("margin-bottom", "10px");
    }

    var drawAllUserColumnChart = function () {
        var data = getColumnChartData(reportBody);
        var options = allUserColumnChartOptions();
        var divElement = drawColumnChart(data, options, reportBody);
        setDivStyle(divElement);
    }

    var drawUserColumnChart = function (user, parentDiv) {
        var data = getColumnChartData(user);
        var options = userColumnChartOptions(user);
        var divElement = drawColumnChart(data, options, parentDiv);
        setDivStyle(divElement);
    }

    google.setOnLoadCallback(drawBasic);

    function drawBasic() {
        drawAllUserColumnChart();

        var userChartDiv = newChartDiv(reportBody);
        var users = jq(reportBody).find(constElementNames.user);
        for (var i = 0; i < users.length; i++) {
            drawUserColumnChart(users[i], userChartDiv);
        }
        setDivStyle(userChartDiv);
        jq(userChartDiv).css("padding", "20px");
    }
})