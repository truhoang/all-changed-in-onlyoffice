
ASC.Projects.SignedTime = (function () {
    var isInit = false,
        currentUserId,
        filteredEvidences = [],
        signedTimeDescriptionTimeout = 0,
        overSignedTimeDescriptionPanel = false,
        filterEvidenceCount = 0,
        // cache DOM elements
        commonPopupContainer = jq("#commonPopupContainer"),
        commonListContainer = jq("#CommonListContainer"),
        $signedTimeListContainer = jq('.signedTimeList'),
        evidenceActionPanel = null,
        signedTimeDescribePanel = null,
        isFirstLoad = true,
        actionMenuItems = {
            listId: "evidenceActionPanel",
            menuItems: [
                { id: "st_add", text: ASC.Projects.Resources.EvidenceResource.Add },
                { id: "ev_edit", text: ASC.Projects.Resources.EvidenceResource.Edit },
                { id: "ev_remove", text: ASC.Projects.Resources.CommonResource.Delete }
            ]
        };
    var signedTimeLessThanOptions = [60, 90, 120, 150, 180, 210, 240, 270, 300];
    var signedTimeMoreThanOptions = [0, 30, 45, 60, 90, 120, 150, 180, 210, 240];

    var hideFirstLoader = function () {
        isFirstLoad = false;
        jq(".mainPageContent").children(".loader-page").hide();
        jq("#filterContainer, #CommonListContainer").show();
        jq('#ProjectsAdvansedFilter').advansedFilter("resize");
    };

    var self;
    var init = function () {
        if (isInit === false) {
            initActionPanels();
            isInit = true;
        }
        self = this;
        isFirstLoad = true;
        jq(".mainPageContent").children(".loader-page").show();

        self.setDocumentTitle(ASC.Projects.Resources.ProjectsJSResource.EvidencesModule);
        self.checkElementNotFound(ASC.Projects.Resources.ProjectsJSResource.TaskNotFound);

        currentUserId = Teamlab.profile.id;

        //page navigator
        self.initPageNavigator("tasksKeyForPagination");

        if (!isFirstLoad) {
            LoadingBanner.displayLoading();
            jq("#filterContainer, #CommonListContainer").show();
            jq('#ProjectsAdvansedFilter').advansedFilter("resize");
        } else {
            jq("#filterContainer, #CommonListContainer").hide();
        }

        // waiting data from api
        createAdvansedFilter();

        jq("#countOfRows").change(function (evt) {
            self.changeCountOfRows(this.value);
        });

        jq('body').on("click.tasksInit", function (event) {
            var elt = (event.target) ? event.target : event.srcElement;
            var isHide = true;
            var $elt = jq(elt);

            if (
              $elt.is('.studio-action-panel') ||
			  $elt.is('#taskName') ||
			  $elt.is('.subtask-name-input') ||
			  $elt.is('.choose') ||
			  $elt.is('.choose > *') ||
			  $elt.is('.combobox-title') ||
			  $elt.is('.combobox-title-inner-text') ||
			  $elt.is('.option-item')
			) {
                isHide = false;
            }
        });

        jq('#othersListPopup').on('click', '.user', function () {
            var userid = jq(this).attr('userId');
            if (userid != "4a515a15-d4d6-4b8e-828e-e0586f18f3a3") {
                var path = jq.changeParamValue(ASC.Controls.AnchorController.getAnchor(), 'tasks_responsible', userid);
                path = jq.removeParam('noresponsible', path);
                ASC.Controls.AnchorController.move(path);
            }
        });

        commonListContainer.on('click', '.project .value', function () {
            var path = jq.changeParamValue(ASC.Controls.AnchorController.getAnchor(), 'project', jq(this).attr('projectid'));
            path = jq.removeParam('milestone', path);
            path = jq.removeParam('myprojects', path);
            ASC.Controls.AnchorController.move(path);
        });

        commonListContainer.on('click', '.milestone .value', function () {
            var path = jq.changeParamValue(ASC.Controls.AnchorController.getAnchor(), 'milestone', jq(this).attr('milestone'));
            ASC.Controls.AnchorController.move(path);
        });

        jq("#emptyListSignedTime").on("click", ".addFirstElement", function () {
            var pathname = window.location.pathname + "?action=add"; // Returns path only
            window.location.replace(pathname);
            return false;
        });

        evidenceActionPanel.on('click', "#ev_edit", function () {
            var signedTimeId = jq('#evidenceActionPanel').attr('objid');
            var pathname = window.location.pathname + "?action=edit&id=" + signedTimeId; // Returns path only
            window.location.replace(pathname);

        });

        evidenceActionPanel.on('click', "#st_add", function () {
            var pathname = window.location.pathname + "?action=add"; // Returns path only
            window.location.replace(pathname);
            return false;
        });

        evidenceActionPanel.on('click', "#ev_remove", function () {
            var signedTimeId = jq('#evidenceActionPanel').attr('objid');
            jq('.studio-action-panel').hide();
            showQuestionWindowSignedTimeRemove(signedTimeId);
            return false;
        });

        $signedTimeListContainer.on('mouseenter', '.signedTime .signedTimeName a', function (event) {
            signedTimeDescriptionTimeout = setTimeout(function () {
                var targetObject = event.target,
                    signedTimeDescribePanel = jq('#signedTimeDescrPanel'),
                    panelContent = signedTimeDescribePanel.find(".panel-content");
                descriptionObj = {};

                panelContent.empty();

                if (jq(targetObject).attr('dateTimeCreated').length) {
                    descriptionObj.creationDate = Encoder.htmlEncode(jq(targetObject).attr('dateTimeCreated'));
                }
                if (jq(targetObject).attr('createdby').length) {
                    descriptionObj.createdBy = Encoder.htmlEncode(jq(targetObject).attr('createdby'));
                }
                if (jq(targetObject).attr('typeSignedTime').length) {
                    descriptionObj.typeSignedTime = Encoder.htmlEncode(jq(targetObject).attr('typeSignedTime'));
                }
                if (jq(targetObject).attr('projectTitle').length) {
                    descriptionObj.project = Encoder.htmlEncode(jq(targetObject).attr('projectTitle'));
                }
                if (jq(targetObject).attr('taskTitle').length) {
                    descriptionObj.task = Encoder.htmlEncode(jq(targetObject).attr('taskTitle'));
                }
                if (jq(targetObject).attr('userName').length) {
                    descriptionObj.userName = Encoder.htmlEncode(jq(targetObject).attr('userName'));
                }
                if (jq(targetObject).attr('totalMinutes').length) {
                    descriptionObj.totalMinutes = Encoder.htmlEncode(jq(targetObject).attr('totalMinutes'));
                }

                var description = jq(targetObject).attr('description');
                if (jq.trim(description) != '') {
                    descriptionObj.description = jq(targetObject).attr('description');
                    if (description.indexOf("\n") > 2 || description.length > 80) {
                        descriptionObj.readMore = "SignedTimes.aspx?action=view&id=" + jq(targetObject).attr('signedTimeid') + "&prjID=" + jq(targetObject).attr('projectID');
                    }
                }

                panelContent.append(jq.tmpl("projects_descriptionPanelContent", descriptionObj));

                showActionsPanel.call(targetObject, 'signedTimeDescrPanel');
                overSignedTimeDescriptionPanel = true;
            }, 400, this);
        });

        function showEntityMenu(event) {
            $signedTimeListContainer.find(".signedTime").removeClass('menuopen');
            jq(this).closest(".signedTime").addClass('menuopen');

            showActionsPanel.call(this, 'evidenceActionPanel', event ? { x: event.pageX | (event.clientX + event.scrollLeft), y: event.pageY | (event.clientY + event.scrollTop) } : undefined);

            // ga-track
            trackingGoogleAnalitics(ga_Categories.tasks, ga_Actions.actionClick, "task-menu");

            return false;
        }

        $signedTimeListContainer.on('click', '.signedTime .entity-menu', function () {
            return showEntityMenu.call(this);
        });

        $signedTimeListContainer.on('contextmenu', '.signedTime', function (event) {
            jq('.studio-action-panel, .filter-list').hide();
            return showEntityMenu.call(this, event);
        });

        signedTimeDescribePanel.on('mouseenter', function () {
            overSignedTimeDescriptionPanel = true;
        });

        signedTimeDescribePanel.on('mouseleave', function () {
            overSignedTimeDescriptionPanel = false;
            hideDescriptionPanel();
        });

        commonPopupContainer.on('click', "#popupRemoveSignedTimeButton", function () {
            var signedTimeID = commonPopupContainer.attr('evidenceID');
            removeSignedTime({ 'evidenceID': signedTimeID }, signedTimeID);
            return false;
        });

        // (google analitics)ga-track-events

        //responsible
        jq(".user span").trackEvent(ga_Categories.tasks, ga_Actions.userClick, "tasks-responsible");

        //actions in menu
        jq("#st_add").trackEvent(ga_Categories.tasks, ga_Actions.actionClick, "add");
        jq("#ev_remove").trackEvent(ga_Categories.tasks, ga_Actions.actionClick, "remove");
        jq("#ev_edit").trackEvent(ga_Categories.tasks, ga_Actions.actionClick, "edit");

        //end ga-track-events
    };

    var initActionPanels = function () {
        commonListContainer.append(jq.tmpl("projects_panelFrame", { panelId: "signedTimeDescrPanel" }));
        signedTimeDescribePanel = jq("#signedTimeDescrPanel");

        //action panel
        commonListContainer.append(jq.tmpl("projects_panelFrame", { panelId: "evidenceActionPanel" }));
        evidenceActionPanel = jq("#evidenceActionPanel");
        evidenceActionPanel.find(".panel-content").empty().append(jq.tmpl("projects_actionMenuContent", actionMenuItems));

        commonListContainer.append(jq.tmpl("projects_panelFrame", { panelId: "othersPanel" }));
        jq("#othersPanel .panel-content").empty().append(jq.tmpl("projects_actionMenuContent", { menuItems: [] }));
        jq("#othersPanel .dropdown-content").attr("id", "othersListPopup");
    };

    var getOptions = function (optionsArray) {
        var options = [];

        for (var i = 0; i < optionsArray.length; i++) {
            options.push({ value: optionsArray[i], title: optionsArray[i] });
        }
        return options;
    }


    var createAdvansedFilter = function () {
        var now = new Date(),
            today = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0),
            inWeek = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0);
        inWeek.setDate(inWeek.getDate() + 7);

        var filters = [];

        //signed time owner
        filters.push({
            type: "person",
            id: "me_signedTime_owner",
            title: ASC.Projects.Resources.ProjectsFilterResource.Me,
            filtertitle: "User :",
            group: "By User",
            hashmask: "person/{0}",
            groupby: "creatorid",
            bydefault: { id: currentUserId }
        });
        filters.push({
            type: "person",
            id: "signedTime_owner",
            title: ASC.Projects.Resources.ProjectsFilterResource.OtherUsers,
            filtertitle: "User :",
            group: "By User",
            hashmask: "person/{0}",
            groupby: "creatorid"
        });

        //Projects
        filters.push({
            type: "combobox",
            id: "project",
            title: ASC.Projects.Resources.ProjectsFilterResource.OtherProjects,
            filtertitle: ASC.Projects.Resources.ProjectsFilterResource.ByProject + ":",
            group: ASC.Projects.Resources.ProjectsFilterResource.ByProject,
            options: self.getProjectsForFilter(),
            hashmask: "project/{0}",
            groupby: "projects",
            defaulttitle: ASC.Projects.Resources.ProjectsFilterResource.Select
        });

        //date time created
        filters.push({
            type: "daterange",
            id: "signedTime_today",
            title: ASC.Projects.Resources.ProjectsFilterResource.Today,
            filtertitle: " ",
            group: ASC.Projects.Resources.ProjectsFilterResource.DueDate,
            //hashmask: "deadline/{0}/{1}",
            groupby: "date_time_created",
            bydefault: { from: today.getTime(), to: today.getTime() }
        });

        filters.push({
            type: "daterange",
            id: "signedTime_period",
            title: ASC.Projects.Resources.ProjectsFilterResource.CustomPeriod,
            filtertitle: " ",
            group: ASC.Projects.Resources.ProjectsFilterResource.DueDate,
            //hashmask: "deadline/{0}/{1}",
            groupby: "date_time_created"
        });

        //total signed time less than
        filters.push({
            type: "combobox",
            id: "signedTime_less_than",
            title: "less than",
            filtertitle: "Signed Time less than:",
            group: "signedTime",
            //hashmask: "deadline/{0}/{1}",
            groupby: "signedTime",
            options: getOptions(signedTimeLessThanOptions)

        });

        //total signed time more than
        filters.push({
            type: "combobox",
            id: "signedTime_more_than",
            title: "more than",
            filtertitle: "Signed Time more than:",
            group: "signedTime",
            options: getOptions(signedTimeMoreThanOptions)
        });


        self.filters = filters;
        self.colCount = 2;

        self.sorters =
        [
            { id: "title", title: "Title", sortOrder: "ascending", def: true },
            { id: "date", title: "Date", sortOrder: "descending" },
            { id: "signedTime", title: "Signed Time", sortOrder: "ascending" }
        ];

        ASC.Projects.ProjectsAdvansedFilter.init(self);

        //filter
        ASC.Projects.ProjectsAdvansedFilter.filter.one("adv-ready", function () {
            var projectAdvansedFilterContainer = jq("#ProjectsAdvansedFilter .advansed-filter-list");
            projectAdvansedFilterContainer.find("li[data-id='me_signedTime_owner'] .inner-text").trackEvent(ga_Categories.tasks, ga_Actions.filterClick, 'me-signedTime-owner');
            projectAdvansedFilterContainer.find("li[data-id='signedTime_owner'] .inner-text").trackEvent(ga_Categories.tasks, ga_Actions.filterClick, 'signedTime owner');

            projectAdvansedFilterContainer.find("li[data-id='myprojects'] .inner-text").trackEvent(ga_Categories.tasks, ga_Actions.filterClick, 'my-projects');
            projectAdvansedFilterContainer.find("li[data-id='project'] .inner-text").trackEvent(ga_Categories.tasks, ga_Actions.filterClick, 'project');

            projectAdvansedFilterContainer.find("li[data-id='signedTime_today'] .inner-text").trackEvent(ga_Categories.tasks, ga_Actions.filterClick, 'today');
            projectAdvansedFilterContainer.find("li[data-id='signedTime_period'] .inner-text").trackEvent(ga_Categories.tasks, ga_Actions.filterClick, 'signedTime-period');

            projectAdvansedFilterContainer.find("li[data-id='signedTime_less_than'] .inner-text").trackEvent(ga_Categories.tasks, ga_Actions.filterClick, 'signedTime-less-than');
            projectAdvansedFilterContainer.find("li[data-id='signedTime_more_than'] .inner-text").trackEvent(ga_Categories.tasks, ga_Actions.filterClick, 'signedTime-more-than');

            jq("#ProjectsAdvansedFilter .btn-toggle-sorter").trackEvent(ga_Categories.tasks, ga_Actions.filterClick, 'sort');
            jq("#ProjectsAdvansedFilter .advansed-filter-input").trackEvent(ga_Categories.tasks, ga_Actions.filterClick, "search_text", "enter");
        });
    };

    var getData = function () {
        self.currentFilter.Count = self.entryCountOnPage;
        self.currentFilter.StartIndex = self.entryCountOnPage * self.currentPage;

        Teamlab.getSignedTimes({}, { filter: self.currentFilter, success: onGetSignedTimes });
    };

    var getFilteredTaskById = function (taskId) {
        for (var i = 0, max = filteredEvidences.length; i < max; i++) {
            if (filteredEvidences[i].id == taskId) {
                return filteredEvidences[i];
            }
        }
    };

    var setFilteredTask = function (task) {
        for (var i = 0, max = filteredEvidences.length; i < max; i++) {
            if (filteredEvidences[i].id == task.id) {
                filteredEvidences[i] = task;
            }
        }
    };

    var removeSignedTime = function (params, signedTimeId) {
        Teamlab.deleteSignedTime(params, signedTimeId, { success: onRemoveSignedTime, error: onRemoveSignedTime });
    };

    var emptyScreenList = function (isItems) {
        var emptyScreen = ASC.Projects.ProjectsAdvansedFilter.baseFilter ? '#emptyListSignedTime' : '#signedTimesEmptyScreenForFilter';

        if (isItems === undefined) {
            var signedTimes = jq('.signedTimeList .signedTime');
            if (signedTimes.length != 0) {
                isItems = true;
            }
        }

        if (isItems) {
            jq('.noContentBlock').hide();
            self.showAdvansedFilter();
            jq('#tableForNavigation').show();
            jq(".signedTimeList").show();
        } else {
            if (filterEvidenceCount == undefined || filterEvidenceCount == 0) {
                jq(emptyScreen).show();
                jq('#tableForNavigation').hide();
                if (emptyScreen == '#emptyListSignedTime') {
                    self.hideAdvansedFilter();
                    jq('#tasksEmptyScreenForFilter').hide();
                }
            }
            else {
                if (self.currentPage > 0) {
                    self.currentPage--;
                    getData();
                }
            }
        }
    };

    // show popup methods

    var showQuestionWindowSignedTimeRemove = function (evidenceId) {
        self.showCommonPopup("projects_signedTimeRemoveWarning", 400, 200);
        PopupKeyUpActionProvider.EnterAction = "jq('.commonPopupContent .remove').click();";
        commonPopupContainer.attr('evidenceId', evidenceId);
    };

    var showActionsPanel = function (panelId, coord) {
        var self = jq(this);
        if (!self.is(".entity-menu") && panelId == "evidenceActionPanel") self = self.find(".entity-menu");
        var objid = '',
            objidAttr = '';
        var x, y;
        if (panelId == 'evidenceActionPanel') {
            objid = self.attr('signedTimeid');
        }
        if (objid.length) {
            objidAttr = '[objid=' + objid + ']';
        }
        if (jq('#' + panelId + ':visible' + objidAttr).length && panelId != 'signedTimeDescrPanel' && panelId != 'subTaskDescrPanel') {
            jq('body').off('click');
            jq('.studio-action-panel, .filter-list').hide();
            jq('.changeStatusCombobox').removeClass('selected');

        } else {
            jq('.studio-action-panel, .filter-list').hide();
            jq('.changeStatusCombobox').removeClass('selected');

            jq('#' + panelId).show();
            // remove magic numbers
            if (panelId == 'signedTimeDescrPanel') {
                x = self.offset().left + 10;
                y = self.offset().top + 20;
                jq('#' + panelId).attr('objid', jq(this).attr('taskid'));
            } else if (panelId == 'othersPanel') {
                x = self.offset().left - 133;
                y = self.offset().top + 26;
            } else {
                x = self.offset().left - 50;
                y = self.offset().top + 20;
                jq('#' + panelId).attr('objid', objid);
                evidenceActionPanel.find('.dropdown-item').show();

                var evidence = jq('.signedTime[evidenceid=' + objid + ']');

                var evidenceUser = jq(evidence).find(".user");
                if (evidence.length) { //if it`s tasks menu        
                    if (jq(evidence).hasClass('closed')) {
                        evidenceActionPanel.find('.dropdown-item').hide();
                        evidenceActionPanel.find('#ev_remove').show();
                    }
                }

                if (jq('.signedTime[evidenceID=' + objid + ']').length) {
                    if (self.attr('canDelete') != "true") {
                        evidenceActionPanel.find('#ev_remove').hide();

                        if (self.attr('canEdit') == "false" || Teamlab.profile.isVisitor) {
                            evidenceActionPanel.find('#ev_edit').hide();
                        }
                    }
                }
            }

            if (typeof y == 'undefined')
                y = self.offset().top + 29;

            if (coord) {
                x = coord.x - jq('#' + panelId).outerWidth();
                y = coord.y;
            }
            jq('#' + panelId).css({ left: x, top: y });

            jq('body').off("click.tasksShowActionsPanel");
            jq('body').on("click.tasksShowActionsPanel", function (event) {
                var elt = (event.target) ? event.target : event.srcElement;
                var isHide = true;
                if (jq(elt).is('[id="' + panelId + '"]') || (elt.id == this.id && this.id.length) || jq(elt).is('.entity-menu') || jq(elt).is('.other')) {
                    isHide = false;
                }

                if (isHide)
                    jq(elt).parents().each(function () {
                        if (self.is('[id="' + panelId + '"]')) {
                            isHide = false;
                            return false;
                        }
                    });

                if (isHide) {
                    hideevidenceActionPanel();
                }
            });
        }
    };

    var hideevidenceActionPanel = function () {
        jq('.studio-action-panel').hide();
        jq('.signedTimeList .signedTime').removeClass('menuopen');
    };

    var hideDescriptionPanel = function () {
        setTimeout(function () {
            if (!overSignedTimeDescriptionPanel) signedTimeDescribePanel.hide(100);
        }, 200);
    };

    var unbindListEvents = function () {
        if (!isInit) return;
        jq("#countOfRows").unbind();
        $signedTimeListContainer.unbind();
        evidenceActionPanel.unbind();
        commonListContainer.unbind();
        commonPopupContainer.unbind();
        signedTimeDescribePanel.unbind();
    };


    //api callback
    var onGetSignedTimes = function (params, evidences) {
        self.clearTables();

        filteredEvidences = evidences;
        jq('#CommonListContainer').height('auto');
        jq('#CommonListContainer .taskSaving').hide();
        clearTimeout(signedTimeDescriptionTimeout);
        overSignedTimeDescriptionPanel = false;
        hideDescriptionPanel();

        jq('#CommonListContainer .signedTimeList').height('auto');
        if (evidences.length) {
            jq.tmpl("evidence_signedTimeTmpl", evidences).appendTo('.signedTimeList');
            jq(".signedTimeList").show();
        }

        isFirstLoad ? hideFirstLoader() : LoadingBanner.hideLoading();

        filterEvidenceCount = params.__total != undefined ? params.__total : 0;
        if (parseInt(filterEvidenceCount) < evidences.length) {
            filterEvidenceCount = evidences.length;
        }
        self.updatePageNavigator(filterEvidenceCount);
        emptyScreenList(evidences.length);
    };

    var onRemoveSignedTime = function (params, signedTimeID) {
        if (signedTimeID === -1) {
            alert("you do not have permission to delete this evidence");
        }

        jq('.signedTimeList .signedTime[signedTimeid=' + signedTimeID + ']').remove();

        filterEvidenceCount--;
        self.updatePageNavigator(filterEvidenceCount);
        emptyScreenList(0);

        commonPopupContainer.removeAttr('evidenceID');
        jq.unblockUI();
        self.displayInfoPanel(ASC.Projects.Resources.ProjectsJSResource.SignedTimeRemoved);

        if (filterEvidenceCount == 0) {
            jq("#emptyListTimers .addFirstElement").addClass("display-none");
        }
    };

    var onErrorRemoveEvidence = function (param, error) {
        var removePopupErrorBox = commonPopupContainer.find(".errorBox");
        removePopupErrorBox.text(error[0]);
        removePopupErrorBox.removeClass("display-none");
        commonPopupContainer.find(".middle-button-container").css('marginTop', '8px');
        setTimeout(function () {
            removePopupErrorBox.addClass("display-none");
            commonPopupContainer.find(".middle-button-container").css('marginTop', '32px');
        }, 3000);
    };

    return jq.extend({
        init: init,
        getData: getData,
        showActionsPanel: showActionsPanel,
        onRemoveSignedTime: onRemoveSignedTime,
        unbindListEvents: unbindListEvents,
        basePath: 'sortBy=date&sortOrder=descending'
    }, ASC.Projects.Common);
})(jQuery);