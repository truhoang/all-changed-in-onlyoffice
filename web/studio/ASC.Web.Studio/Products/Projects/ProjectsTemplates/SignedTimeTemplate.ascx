﻿<%@ Control Language="C#" AutoEventWireup="false" EnableViewState="false" %>
<%@ Assembly Name="ASC.Web.Studio" %>
<%@ Assembly Name="ASC.Web.Projects" %>
<%@ Import Namespace="ASC.Web.Projects.Resources" %>

<script id="evidence_signedTimeTmpl" type="text/x-jquery-tmpl">
    <div class="signedTime with-entity-menu" signedTimeid="${id}">
            {{if canEdit}}
                <div class="entity-menu" signedTimeid="${id}" taskid="${taskID}" projectwithtaskid="${projectWithTaskID}" projectwithtasktitle="${projectWithTaskTitle}" projectid="${projectID}" projecttitle="${projectTitle}" candelete="${canDelete}" canedit="${canEdit}" userid="${userID}"></div>
            {{else}}
                <div class="entity-menu" style="background: inherit; cursor: inherit;"></div>
            {{/if}}

        <div class="evidencePlace">
            <div class="signedTimeName" signedTimeid="${id}">
                <a signedTimeid="${id}" createdby="${createdByName}" projectTitle="${projectTitle}" typeSignedTime="${typeSignedTime}"
                    userName="${userName}" totalMinutes="${totalMinutes}" taskTitle="${taskTitle}" dateTimeCreated="${dateTimeCreated}"
                    signedTimeid="${id}" description="${description}" projectID="${projectID}" taskID="${taskID}" href="SignedTimes.aspx?action=view&id=${id}&prjID=${projectID}">${title}                    
                </a>
            </div>
        </div>

        <div class="otherMargin">
            {{if taskID}}
            <a class="linkElement" taskid="${id}" href="tasks.aspx?prjID=${projectWithTaskID}&id=${taskID}">${taskTitle}</a>
            {{else}}
            <a class="linkElement" projectid="${projectID}" href="Projects.aspx?prjID=${projectID}">${projectTitle}</a>
            {{/if}} 
            <span></span>
        </div>

        <div class="user otherMargin" userid="${userID}">
            <a class="linkElement" href="/products/people/profile.aspx?user=${userName}">${userName}</a>
        </div>

        <div class="user deadline">
            <span>${totalMinutes}</span>
        </div>

    </div>
</script>
