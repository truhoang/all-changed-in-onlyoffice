﻿<%@ Control Language="C#" AutoEventWireup="false" EnableViewState="false" %>
<%@ Assembly Name="ASC.Web.Studio" %>
<%@ Assembly Name="ASC.Web.Projects" %>
<%@ Import Namespace="ASC.Web.Projects.Resources" %>

<%
    var n = 10;
     %>

<script id="projects_evidenceListItemTmpl" type="text/x-jquery-tmpl">
            <div class="evidence with-entity-menu" evidenceID="${id}">    
                
                 <%--{{if $canEdit || $canCreateEvidence || $canDelete}}--%>
                    <div class="entity-menu" evidenceID="${id}" taskID="${taskID}" projectid="${projectID}" canDelete="${canDelete}" canEdit="${canEdit}" userid="${userID}"></div>
               <%-- {{else}}
                    <div class="nomenupoint"></div>
                {{/if}}    --%>
                
                <%--<div class="check">
                  <div taskid="${id}" class="changeStatusCombobox{{if $item.data.canEdit}} canEdit{{/if}}">
                      {{if $item.data.status == 2}}
                          <span title="<%= TaskResource.Closed%>" class="closed"></span>
                      {{else}}
                          <span title="<%= TaskResource.Open%>" class="open"></span>
                      {{/if}}   
                      {{if $item.data.canEdit}}<span class="arrow"></span> {{/if}}              
                  </div>
                </div>--%>
                
                <div class="evidencePlace">
                  <div class="evidenceName" evidenceID="${id}">
                        <a evidenceID="${id}" href="Evidences.aspx?action=view&id=${id}" 
                            projectid = ${projectID}
                            description = "${description}"
                            {{if typeof $createdByID != 'undefined'}}createdBy="${createdByName}"{{/if}}>
                            ${title}                    
                        </a>                    
					</div>
				    <%--<div class="subtasksCount" data="<%=TaskResource.Subtask %>"> 
                        {{if ASC.Projects.TasksManager.openedCount($item.data.subtasks)}}
                            <span class="expand" taskid="${id}"><span class="dottedNumSubtask" title="<%=ProjectResource.TitleTaskOpenSubtasks %>">+{{html ASC.Projects.TasksManager.openedCount($item.data.subtasks)}}</span></span>
                        {{else}}
                            {{if $item.data.canCreateSubtask}}
                            <span class="add" taskid="${id}">+ <%=TaskResource.Subtask %></span>
                            {{/if}}                          
                        {{/if}} 
                    </div>   --%>
                </div>  
                                                                                                                                                                                   
              <%--  {{if $item.data.responsibles.length > 1}}
                    <div taskid="${id}"  userId="${responsibles[0].id}">
                        <div class="otherMargin"><span taskid="${id}" class="other" title="<%=ProjectResource.TitleTaskResponsibleUsers %>">${responsibles.length} <%= TaskResource.Responsibles %></span></div>
                        <ul class="others" taskid="${id}">
                            {{each responsibles}}
                               {{if $index >= 0}}
                                    <li userId="${id}" class="user dropdown-item{{if id=='4a515a15-d4d6-4b8e-828e-e0586f18f3a3'}} not-action{{/if}}"><span  title="${displayName}">${displayName}</span></li>
                               {{/if}}                          
                            {{/each}}
                        </ul>                
                    </div>                        
                {{else}}
                
                    {{if $item.data.responsible == null}}
                        <div class="not user" taskid="${id}">
                            <span><%= TaskResource.NoResponsible%></span>
                        </div>
                    {{else}}
                        <div class="user" taskid="${id}" userId="${responsible.id}">
                                <span {{if responsible.id=='4a515a15-d4d6-4b8e-828e-e0586f18f3a3'}}class="not-action"{{else}} title="<%=ProjectResource.TitleTaskResponsibleUser %>"{{/if}}>${responsible.displayName}</span>                                                  
                        </div>            
                    {{/if}}                                
                {{/if}}--%>
                    <div class="dateAndTime">
                          <span id = "${id}" class="timeCreated" timeCreated = ${}>${date}</span>
                    </div>

                <div class="dateAndTime">
                          <span id = "${id}" class="duration">${duration}</span>
                    </div>
                <div class="dateAndTime">
                          <span taskid = "${taskID}" class="taskTitle" >${taskTitle}</span>
                </div>

                <div class="user" userId="${userID}">
                                <span>${userName}</span>                   
                </div>            
            </div>
            <div taskid="${id}" class="subtasks" projectid="${projectID}">    
                {{tmpl 'projects_subtaskTemplate'}}
                {{if canEdit || $canCreateEvidence}}
                        <div class="quickAddSubTaskLink icon-link plus" evidenceID="${id}" projectid="${projectID}" visible="true"> 
                          <span class="dottedLink" taskid="${id}"><%= TaskResource.AddNewSubtask%></span>
                        </div>                           
                {{/if}}  
                    <div class="st_separater" evidenceID="${id}"></div>       
                 </div> 
             </div> 
    </script>

    <%--<script id="projects_milestoneForMoveTaskPanelTmpl" type="text/x-jquery-tmpl">               
            <div class="ms">
                <input id="ms_${id}" value="${id}" type="radio" name="milestones"/>
                <%--<label for="ms_${id}">[${displayDateDeadline}] ${title}</label>--%>
<%--            </div>             
    </script>--%>--%>