﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ASC.Web.Projects.Controls.ExamplePage;
using ASC.Web.Projects.Classes;
using ASC.Web.Projects.Controls.Common;
using ASC.Web.Studio.UserControls.Common.LoaderPage;

namespace ASC.Web.Projects
{
    public partial class ExamplePlugin : BasePage
    {

        protected override void PageLoad()
        {
            Page.RegisterBodyScripts(PathProvider.GetFileStaticRelativePath("exampleplugin.js"));

            _content.Controls.Add(LoadControl(CommonList.Location));
            //loaderHolder.Controls.Add(LoadControl(LoaderPage.Location));
            //var examplePageControl = (ExamplePageControl)LoadControl(PathProvider.GetFileStaticRelativePath("ExamplePlugin/ExamplePluginControl.ascx"));
            //_content.Controls.Add(examplePageControl);
        }
    }
}