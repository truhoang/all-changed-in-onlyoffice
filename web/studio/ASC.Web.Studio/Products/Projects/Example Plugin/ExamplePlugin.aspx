﻿<%@ Assembly Name="ASC.Web.Projects" %>

<%@ Page Title="Example Page" Language="C#" MasterPageFile="~/Products/Projects/Masters/BasicTemplate.Master" AutoEventWireup="true" CodeBehind="ExamplePlugin.aspx.cs" Inherits="ASC.Web.Projects.ExamplePlugin" %>
<%@ MasterType TypeName="ASC.Web.Projects.Masters.BasicTemplate" %>
<asp:Content ID="PageContent" ContentPlaceHolderID="BTPageContent" runat="server">
    <asp:PlaceHolder ID="loaderHolder" runat="server"></asp:PlaceHolder>
    <asp:PlaceHolder runat="server" ID="_filter"></asp:PlaceHolder>
    <asp:PlaceHolder runat="server" ID="_content"></asp:PlaceHolder>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="projectsClientTemplatesResourcesPlaceHolder" runat="server">
</asp:Content>
