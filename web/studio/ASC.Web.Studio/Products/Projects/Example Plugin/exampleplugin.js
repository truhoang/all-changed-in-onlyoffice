﻿

if (typeof ASC === "undefined")
    ASC = {};
if (typeof ASC.Projects === "undefined")
    ASC.Projects = {};

ASC.Projects.ExamplePLugin = (function () {
    var commonListContainer = jq("#CommonListContainer"),
    projectsAdvansedFilter,
    projectsTable = jq("#tableListProjects"),
    filterContainer = jq('#filterContainer');
    var self;
    var init = function () {
        self = this;
        projectsAdvansedFilter = createAdvansedFilter();
        filterContainer.show();
        commonListContainer.show();
        projectsAdvansedFilter.advansedFilter("resize");
        this.initPageNavigator("ExampleKeyForPagination");
        filterCount = 10;
        self.updatePageNavigator(filterCount);
    };

    var createAdvansedFilter = function () {
        var filters =
                    [{ type: "person", id: "me_team_member", title: "Example Title", filtertitle: "Example Filter Title", group: "TeamMember", hashmask: "person/{0}", groupby: "userid" }],
                sorters =
                [
                    { id: "title", title: "ByTitle", sortOrder: "ascending", def: true },
                    { id: "create_on", title: "ByCreateDate", sortOrder: "descending" }
                ];
        self.filters = filters;
        self.sorters = sorters;
        self.colCount = 2;
        var filter = ASC.Projects.ProjectsAdvansedFilter.init(self);
        return filter;
    };

    var getData = function () {
        self.currentFilter.Count = self.entryCountOnPage;
        self.currentFilter.StartIndex = self.entryCountOnPage * self.currentPage;
        Teamlab.getPrjProjects({}, { filter: self.currentFilter, success: onGetListProject });
    };

    var getProjTmpl = function (proj) {
        return {
            title: proj.title,
            id: proj.id,
            created: proj.displayDateCrtdate,
            createdBy: proj.createdBy ? proj.createdBy.displayName : "",
            projLink: proj.id,
            description: proj.description,
            milestones: proj.milestoneCount,
            linkMilest: proj.id + '#sortBy=deadline&sortOrder=ascending&status=open',
            tasks: proj.taskCount,
            linkTasks: proj.id + '#sortBy=deadline&sortOrder=ascending&status=open',
            responsible: proj.responsible.displayName,
            responsibleId: proj.responsible.id,
            participants: proj.participantCount ? proj.participantCount - 1 : "",
            linkParticip: proj.id,
            privateProj: proj.isPrivate,
            canEdit: proj.canEdit,
            isSimpleView: false,
            canLinkContact: proj.canLinkContact,
            status: proj.status == 0 ? 'open' : (proj.status == 2 ? 'paused' : 'closed')
        };
    };

    var onGetListProject = function (params, listProj) {
        $projectsTableBody = projectsTable.find('tbody');
        $projectsTableBody.empty();
        $projectsTableBody.append(jq.tmpl("projects_projectTmpl", listProj.map(getProjTmpl)));
        projectsTable.show();
    };

    return jq.extend({
        init: init,
        getData: getData,
    }, ASC.Projects.Common);
})(jQuery);