/*
 * 
 * (c) Copyright Ascensio System SIA 2010-2015
 * 
 * This program is a free software product.
 * You can redistribute it and/or modify it under the terms of the GNU Affero General Public License
 * (AGPL) version 3 as published by the Free Software Foundation. 
 * In accordance with Section 7(a) of the GNU AGPL its Section 15 shall be amended to the effect 
 * that Ascensio System SIA expressly excludes the warranty of non-infringement of any third-party rights.
 * 
 * This program is distributed WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * For details, see the GNU AGPL at: http://www.gnu.org/licenses/agpl-3.0.html
 * 
 * You can contact Ascensio System SIA at Lubanas st. 125a-25, Riga, Latvia, EU, LV-1021.
 * 
 * The interactive user interfaces in modified source and object code versions of the Program 
 * must display Appropriate Legal Notices, as required under Section 5 of the GNU AGPL version 3.
 * 
 * Pursuant to Section 7(b) of the License you must retain the original Product logo when distributing the program. 
 * Pursuant to Section 7(e) we decline to grant you any rights under trademark law for use of our trademarks.
 * 
 * All the Product's GUI elements, including illustrations and icon sets, as well as technical 
 * writing content are licensed under the terms of the Creative Commons Attribution-ShareAlike 4.0 International. 
 * See the License terms at http://creativecommons.org/licenses/by-sa/4.0/legalcode
 * 
*/

using System;
using System.Collections.Generic;
using ASC.Web.Projects.Classes;
using ASC.Web.Projects.Controls.Common;
using ASC.Web.Projects.Controls.Evidences;
using ASC.Web.Projects.HoangEvidence.EvEntities;
using ASC.Web.Projects.HoangEvidence.Classes;
using ASC.Web.Projects.HoangEvidence.EVAction;
using ASC.Web.Projects.Resources;
using ASC.Web.Studio.Utility;

using ASC.Projects.Core.Domain;
using ASC.Web.Studio.UserControls.Common.LoaderPage;

namespace ASC.Web.Projects
{
    public partial class SignedTimes : BasePage
    {
        Dictionary<string, string> queries = new Dictionary<string, string>();
        protected override void PageLoad()
        {

            getQueries();
            if (queries.ContainsKey("action"))
            {
                if (queries["action"] == "add")
                {
                    InitSTPage(null);
                    return;
                }
            }

            int signedTimeID;

            if (Int32.TryParse(UrlParameters.EntityID, out signedTimeID))
            {

                var signedTime = ((SignedTimeAction)EVGlobal.GetSignedTimeAction()).GetByID(signedTimeID);

                if (signedTime == null || !queries.ContainsKey("action"))
                {
                    RedirectNotFound(string.Format("SignedTimes.aspx"));
                }
                else
                {
                    if (!queries.ContainsKey("prjID") && queries["action"] == "view")
                    {
                        Response.Redirect(Request.RawUrl + "&prjID=" + signedTime.ProjectID);
                    }
                    if (queries.ContainsKey("prjID") && queries["action"] != "view")
                    {
                        var id = queries.ContainsKey("id") ? "id=" + queries["id"] : "";
                        var action = queries.ContainsKey("action") ? "action=" + queries["action"] : "";
                        Response.Redirect(Request.Url.AbsolutePath + "?" + id + "&" + action);
                    }
                    InitSTPage(signedTime);
                }
            }
            else
            {
                _content.Controls.Add(LoadControl(CommonList.Location));

                Title = EvidenceResource.EvidenceTitle;
                //Title = HeaderStringHelper.GetPageTitle(EvidenceResource.EvidenceTitle);
                loaderHolder.Controls.Add(LoadControl(LoaderPage.Location));
            }
        }

        private void InitSTPage(SignedTime signedTime)
        {
            if (queries["action"] == "view")
            {
                var signedTimeDescriptionView = (SignedTimeDescView)LoadControl(PathProvider.GetFileStaticRelativePath("Evidences/SignedTimeDescView.ascx"));
                signedTimeDescriptionView.SignedTime = signedTime;

                EssenceTitle = signedTime.Title;
                _content.Controls.Add(signedTimeDescriptionView);
                return;
            }
            if (queries["action"] == "edit" || queries["action"] == "add")
            {
                var stactionActionPage = (STAction)LoadControl(PathProvider.GetFileStaticRelativePath("Evidences/STAction.ascx"));
                stactionActionPage.SignedTime = signedTime;
                stactionActionPage.typeAction = queries["action"];
                _content.Controls.Add(stactionActionPage);
                return;
            }

        }

        void getQueries()
        {
            queries = new Dictionary<string, string>();
            foreach (var item in Request.QueryString.AllKeys)
            {
                queries.Add(item, Request.QueryString[item]);
            }
        }
    }
}